# -*- coding: utf-8 -*-
"""
Created on Mon Oct 14 15:33:56 2019

@author: Bradley Barth
"""

from netpyne import specs, sim
import numpy as np

# Network parameters
netParams = specs.NetParams()
num_neurons = 1000
### References & Assumptions
    ### 28% of MEN are AH neurons | doi: 10.1371/journal.pone.0039887
    ### 15% of neurons are MEN | doi: 10.1371/journal.pone.0039887
    ### 7% of MEN are SAMEN | doi: 10.1371/journal.pone.0039887
    
    ### All AH cells that are MEN are RAMEN
    ### All MEN are either SAMEN or RAMEN (there are no RAMEN)
        ### Therefore 93% of MEN are RAMEN
    
    ### Assume 50:50 split of interneuron MEN between ascending and descending

    ### Proportion of myenteric neurons from p. 32, Furness 2006 [guinea-pig small intestine]
        ### C-EMN       12%
        ### C-IMN       16%
        ### L-EMN       25%
        ### L-IMN       2%
        ### A-INT       5%
        ### D-INT       11%
            ### ChAT/NOS    5%      local reflex
            ### ChAT/5HT    2%      secretomotor/motility reflex
            ### ChAT/SOM    4%      migrating motor complex
        ### IPAN        26%

    ### Of all S-type cells (and MEN):
        ### 16% are C-EMN
        ### 21% are C-IMN
        ### 38% are L-EMN
        ### 3% are L-IMN
        ### 7% are A-INT
        ### 7% are D-INT/NOS
        ### 3% are D-INT/5HT
        ### 5% are D-INT/SOM


num_ramen = np.int(0.15*num_neurons)
num_men = np.int(num_ramen/0.93)
num_samen = num_men - num_ramen

num_ah_ramen = np.int(0.28*num_men)
num_ah_samen = np.int(0)

num_s_ramen = num_ramen - num_ah_ramen
num_s_samen = num_samen

num_sens_men = num_ah_ramen
num_asc_ramen = np.int(0.07*num_s_ramen)
num_asc_samen = np.int(0.07*num_s_samen)
num_desc_ramen = np.int(0.15*num_s_ramen)
num_desc_samen = np.int(0.15*num_s_samen)

num_cemn_samen = np.int(0.16*num_s_samen)
num_cemn_ramen = np.int(0.16*num_s_ramen)
num_cimn_samen = np.int(0.21*num_s_samen)
num_cimn_ramen = np.int(0.21*num_s_ramen)

num_lemn_samen = np.int(0.38*num_s_samen)
num_lemn_ramen = np.int(0.38*num_s_ramen)
num_limn_samen = np.int(0.03*num_s_samen)
num_limn_ramen = np.int(0.03*num_s_ramen)

num_sens_cell = np.int(0.26*num_neurons) - num_sens_men
num_asc_cell = np.int(0.05*num_neurons) - (num_asc_ramen + num_asc_samen)

num_desc_local = np.int(0.05*num_neurons - 0.45*(num_desc_ramen + num_desc_samen))
num_desc_5HT = np.int(0.02*num_neurons - 0.18*(num_desc_ramen + num_desc_samen))
num_desc_MMC = np.int(0.04*num_neurons - 0.37*(num_desc_ramen + num_desc_samen))

num_cemn_cell = np.int(0.12*num_neurons) - (num_cemn_ramen + num_cemn_samen)
num_cimn_cell = np.int(0.16*num_neurons) - (num_cimn_ramen + num_cimn_samen)

num_lemn_cell = np.int(0.25*num_neurons) - (num_lemn_ramen + num_lemn_samen)
num_limn_cell = np.int(0.02*num_neurons) - (num_limn_ramen + num_limn_samen)

print('Group\tRAMEN\tSAMEN\tnon-MEN')
print('Sens\t'+str(num_ah_ramen)+'\t'+str(num_ah_samen)+'\t'+str(num_sens_cell))
print('Asc\t'+str(num_asc_ramen)+'\t'+str(num_asc_samen)+'\t'+str(num_asc_cell))
print('Desc\t'+str(num_desc_ramen)+'\t'+str(num_desc_samen)+'\t'+str(num_desc_local+num_desc_5HT+num_desc_MMC))
print('EMN\t'+str(num_cemn_ramen+num_lemn_ramen)+'\t'+str(num_cemn_samen+num_lemn_samen)+'\t'+str(num_cemn_cell+num_lemn_cell))
print('IMN\t'+str(num_cimn_ramen+num_limn_ramen)+'\t'+str(num_cimn_samen+num_limn_samen)+'\t'+str(num_cimn_cell+num_limn_cell))


num_cemn_samen = 0
num_cemn_ramen = 0
num_lemn_samen = 0
num_lemn_ramen = 0
num_cemn_cell = 0
num_lemn_cell = 0


netParams.sizeX = 150*1e3
netParams.sizeY = 100
netParams.sizeZ = 0

### Population parameters
# Pseudo-ECC intiators
ECCdel = 0.5e3
ECCdur = 3e3
spkTimes = list(np.linspace(ECCdel,ECCdel+ECCdur,int(ECCdur*0.01+1)))
netParams.popParams['ECC'] = {'cellModel': 'VecStim','numCells':10,'spkTimes':spkTimes, 'xnormRange': [0.3, 0.325]}
# Interneurons
netParams.popParams['asc_cell'] = {'cellType': 'S_cell', 'numCells': num_asc_cell, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}
netParams.popParams['asc_samen'] = {'cellType': 'S_samen', 'numCells': num_asc_samen, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}
netParams.popParams['asc_ramen'] = {'cellType': 'S_ramen', 'numCells': num_asc_ramen, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}
netParams.popParams['desc_local'] = {'cellType': 'S_cell', 'numCells': num_desc_local, 'xnormRange': [0, 1], 'ynormRange': [0.95, 1]}
netParams.popParams['desc_5HT'] = {'cellType': 'S_cell', 'numCells': num_desc_5HT, 'xnormRange': [0, 1], 'ynormRange': [0.95, 1]}
netParams.popParams['desc_MMC'] = {'cellType': 'S_cell', 'numCells': num_desc_MMC, 'xnormRange': [0, 1], 'ynormRange': [0.95, 1]}
netParams.popParams['desc_samen'] = {'cellType': 'S_samen', 'numCells': num_desc_samen, 'xnormRange': [0, 1], 'ynormRange': [0.95, 1]}
netParams.popParams['desc_ramen'] = {'cellType': 'S_ramen', 'numCells': num_desc_ramen, 'xnormRange': [0, 1], 'ynormRange': [0.95, 1]}
# Sensory neurons
netParams.popParams['sens_cell'] = {'cellType': 'AH_cell', 'numCells': num_sens_cell, 'xnormRange': [0, 1], 'ynormRange': [0, 0.05]}
netParams.popParams['sens_men'] = {'cellType': 'AH_men', 'numCells': num_sens_men, 'xnormRange': [0, 1], 'ynormRange': [0, 0.05]}
# Motor neurons
netParams.popParams['cemn_cell'] = {'cellType': 'S_cell', 'numCells': num_cemn_cell, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}
netParams.popParams['cemn_samen'] = {'cellType': 'S_samen', 'numCells': num_cemn_ramen, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}
netParams.popParams['cemn_ramen'] = {'cellType': 'S_ramen', 'numCells': num_cemn_ramen, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}
netParams.popParams['cimn_cell'] = {'cellType': 'S_cell', 'numCells': num_cimn_cell, 'xnormRange': [0, 1], 'ynormRange': [0.95, 1]}
netParams.popParams['cimn_samen'] = {'cellType': 'S_samen', 'numCells': num_cimn_ramen, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}
netParams.popParams['cimn_ramen'] = {'cellType': 'S_ramen', 'numCells': num_cimn_ramen, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}
netParams.popParams['lemn_cell'] = {'cellType': 'S_cell', 'numCells': num_lemn_cell, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}
netParams.popParams['lemn_samen'] = {'cellType': 'S_samen', 'numCells': num_lemn_ramen, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}
netParams.popParams['lemn_ramen'] = {'cellType': 'S_ramen', 'numCells': num_lemn_ramen, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}
netParams.popParams['limn_cell'] = {'cellType': 'S_cell', 'numCells': num_limn_cell, 'xnormRange': [0, 1], 'ynormRange': [0.95, 1]}
netParams.popParams['limn_samen'] = {'cellType': 'S_samen', 'numCells': num_limn_ramen, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}
netParams.popParams['limn_ramen'] = {'cellType': 'S_ramen', 'numCells': num_limn_ramen, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}


netParams.importCellParams(label='S_cell_rule', conds= {'cellType': ['S_cell']},
        fileName='s_template.py', cellName='s_cell')
netParams.importCellParams(label='S_samen_rule', conds= {'cellType': ['S_samen']},
        fileName='s_template.py', cellName='s_samen')
netParams.importCellParams(label='S_ramen_rule', conds= {'cellType': ['S_ramen']},
        fileName='s_template.py', cellName='s_ramen')

netParams.importCellParams(label='AH_cell_rule', conds= {'cellType': ['AH_cell']},
        fileName='ah_template.py', cellName='ah_cell')
netParams.importCellParams(label='AH_men_rule', conds= {'cellType': ['AH_men']},
        fileName='ah_template.py', cellName='ah_ramen')

pops = list(netParams.popParams.keys())
total_cells = 0
for i in range(len(pops)):
    total_cells += netParams.popParams[pops[i]]['numCells']

### Apply tension to all populations
amp = 0
rate = 0.015

### Compression driver
#comp_delay = 2e3
#comp_dur = 4e3
#comp_amp = amp
#comp_rate = rate #(elongation per ms)

### Tension driver
tens_delay = 1e3
tens_dur = 30e3
tens_amp = amp
tens_rate = rate #(elongation per ms)
tens = []

def updateStrains(t):
    # Update compression
#    compFlat = comp_amp/comp_rate + comp_delay
#    compOff = comp_delay + comp_dur
#    if t < comp_delay:
#        thisCompression = 0
#    if t >= comp_delay and t < compFlat:
#        thisCompression = (t-comp_delay)*comp_rate
#    if t >= compFlat and t < comp_dur + comp_delay:
#        thisCompression = comp_amp
#    if t >= comp_dur + comp_delay and t < compOff:
#        thisCompression = comp_amp - (t - (comp_dur+comp_delay))*comp_rate
#    if t >= compOff:
#        thisCompression = 0
    
    # Update tension
    tensFlat = tens_amp/tens_rate + tens_delay
    tensOff = tens_delay + tens_dur
    if t < tens_delay:
        thisTension = 0
    if t >= tens_delay and t < tensFlat:
        thisTension = (t-tens_delay)*tens_rate
    if t >= tensFlat and t < tens_dur + tens_delay:
        thisTension = tens_amp
    if t >= tens_dur + tens_delay and t < tensOff:
        thisTension = tens_amp - (t - (tens_dur+tens_delay))*tens_rate
    if t >= tensOff:
        thisTension = 0
        
    # Set values
    for j in range(len(pops)):
        cellsInPop = len(sim.net.pops[pops[j]].cellGids)
        for i in range(cellsInPop):
            thisCell = sim.net.pops[pops[j]].cellGids[i]
#            comp[i].append(((i+1)/(num_neurons))*thisCompression)
#            sim.net.cells[thisCell].secs['soma']['mechs']['cMSC']['compression'] = (i+1)/(num_neurons)*thisCompression
#            sim.net.cells[thisCell].secs['soma']['hObj'].compression_cMSC = (i+1)/(num_neurons)*thisCompression
            tens.append(thisTension)
            sim.net.cells[thisCell].secs['soma']['mechs']['tMSC']['tension'] = thisTension
            sim.net.cells[thisCell].secs['soma']['hObj'].tension_tMSC = thisTension
    return sim


### Synaptic mechanisms
netParams.synMechParams['fEPSP'] = {'mod': 'Exp2Syn', 'tau1': 0.7, 'tau2': 20.0, 'e': 0}
netParams.fEPSPweight = 0.0006 #0.0009125

netParams.synMechParams['sEPSP'] = {'mod': 'Exp2Syn', 'tau1': 25e3, 'tau2': 55e3, 'e': 0}
netParams.sEPSPweight = 0.00008

netParams.synMechParams['IPSP'] = {'mod': 'Exp2Syn', 'tau1': 6e3, 'tau2': 8e3, 'e': -75}
netParams.IPSPweight = 0.002



propVelocity = 100 #(um/ms)
## Ascending synaptic distribution
netParams.p_max_asc = 4000
netParams.sigma_a = 0.3
A_proj_mean = 6000
netParams.mu_a = np.log(A_proj_mean)
## Descending synaptic distribution
netParams.p_max_desc = 4000
netParams.sigma_d = 0.3
D_proj_mean = 15000
netParams.mu_d = np.log(D_proj_mean)
## Local synaptic distribution
netParams.p_max_local = 800
netParams.sigma_l = 0.3
L_proj_mean = 1000
netParams.mu_l = np.log(L_proj_mean)
## IPAN to IPAN synaptic distribution
netParams.p_max_sens = 400
netParams.sigma_s = 0.3
S_proj_mean = 1000
netParams.mu_s = np.log(S_proj_mean)


### Cell connectivity rules
netParams.connParams['ECC->sens'] = {          # ECC to sensory (stim to ipan)
        'preConds': {'pop': ['ECC']},             # conditions of presyn cells
        'postConds': {'pop': ['sens_cell','sens_men']},            # conditions of postsyn cells
        'probability': 'p_max_sens / (dist_x * sigma_s * sqrt(2*np.pi)) * exp(-((np.log(dist_x)-mu_s)**2)/(2*sigma_s**2))',             # probability of connection
        'weight': 'fEPSPweight',                # synaptic weight
        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
        'synMech': 'fEPSP'}    
netParams.connParams['asc->asc'] = {            # ascending to ascending
        'preConds': {'pop': ['asc_cell','asc_samen','asc_ramen']},             # conditions of presyn cells
        'postConds': {'pop': ['asc_cell','asc_samen','asc_ramen']},            # conditions of postsyn cells
        'probability': '100*p_max_asc / ((pre_x - post_x) * sigma_a * sqrt(2*np.pi)) * exp(-((np.log(pre_x - post_x)-mu_a)**2)/(2*sigma_a**2))',             # probability of connection
        'weight': 'fEPSPweight',                # synaptic weight
        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
        'synMech': 'fEPSP'}                     # synaptic mechanism
netParams.connParams['descending_5HT_projection'] = {                          # descending 5HT distal projection
        'preConds': {'pop': ['desc_5HT']},      # conditions of presyn cells
        'postConds': {'pop': ['desc_5HT','cimn_cell','cimn_samen','cimn_ramen','limn_cell','limn_samen','limn_ramen','asc_cell','asc_samen','asc_ramen']},           # conditions of postsyn cells
        'probability': 'p_max_desc / ((post_x - pre_x) * sigma_d * sqrt(2*np.pi)) * exp(-((np.log(post_x - pre_x)-mu_d)**2)/(2*sigma_d**2))',             # probability of connection
        'weight': 'fEPSPweight',                # synaptic weight
        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
        'synMech': 'fEPSP'}                     # synaptic mechanism
netParams.connParams['descending_5HT_local'] = {                               # descending 5HT distal projection
        'preConds': {'pop': ['desc_5HT']},      # conditions of presyn cells
        'postConds': {'pop': ['desc_5HT','sens_cell','sens_men','cimn_cell','cimn_samen','cimn_ramen','limn_cell','limn_samen','limn_ramen']},           # conditions of postsyn cells
        'probability': 'p_max_local / ((post_x - pre_x) * sigma_l * sqrt(2*np.pi)) * exp(-((np.log(post_x - pre_x)-mu_l)**2)/(2*sigma_l**2))',             # probability of connection
        'weight': 'fEPSPweight',                # synaptic weight
        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
        'synMech': 'fEPSP'}                     # synaptic mechanism
netParams.connParams['descending_5HT_local'] = {                               # descending 5HT distal projection
        'preConds': {'pop': ['desc_local']},    # conditions of presyn cells
        'postConds': {'pop': ['desc_local','cimn_cell','cimn_samen','cimn_ramen','limn_cell','limn_samen','limn_ramen']},           # conditions of postsyn cells
        'probability': 'p_max_local / ((post_x - pre_x) * sigma_l * sqrt(2*np.pi)) * exp(-((np.log(post_x - pre_x)-mu_l)**2)/(2*sigma_l**2))',             # probability of connection
        'weight': 'fEPSPweight',                # synaptic weight
        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
        'synMech': 'fEPSP'}                     # synaptic mechanism
netParams.connParams['desc->desc'] = {          # descending to descending
        'preConds': {'pop': ['desc_MMC','desc_samen','desc_ramen']},           # conditions of presyn cells
        'postConds': {'pop': ['desc_MMC','desc_samen','desc_ramen']},          # conditions of postsyn cells
        'probability': 'p_max_desc / ((post_x - pre_x) * sigma_d * sqrt(2*np.pi)) * exp(-((np.log(post_x - pre_x)-mu_d)**2)/(2*sigma_d**2))',             # probability of connection
        'weight': 'fEPSPweight',                # synaptic weight
        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
        'synMech': 'fEPSP'}                     # synaptic mechanism
netParams.connParams['desc_MMC->asc'] = {           # local ascending to descending and vice versa
        'preConds': {'pop': ['desc_MMC']},           # conditions of presyn cells
        'postConds': {'pop': ['asc_cell','asc_samen','asc_ramen']},            # conditions of postsyn cells
        'probability': '50*p_max_local / (dist_x * sigma_l * sqrt(2*np.pi)) * exp(-((np.log(dist_x)-mu_l)**2)/(2*sigma_l**2))',             # probability of connection
        'weight': 'fEPSPweight',                # synaptic weight
        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
        'synMech': 'fEPSP'}                     # synaptic mechanism
netParams.connParams['asc->desc'] = {           # local ascending to descending and vice versa
        'preConds': {'pop': ['asc_cell','asc_samen','asc_ramen']},             # conditions of presyn cells
        'postConds': {'pop': ['desc_MMC','desc_samen','desc_ramen']},          # conditions of postsyn cells
        'probability': 'p_max_local / (dist_x * sigma_l * sqrt(2*np.pi)) * exp(-((np.log(dist_x)-mu_l)**2)/(2*sigma_l**2))',             # probability of connection
        'weight': 'fEPSPweight',                # synaptic weight
        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
        'synMech': 'fEPSP'}                     # synaptic mechanism
netParams.connParams['sens->sens'] = {          # sensory to sensory (ipan to ipan)
        'preConds': {'pop': ['sens_cell','sens_men']},             # conditions of presyn cells
        'postConds': {'pop': ['sens_cell','sens_men']},            # conditions of postsyn cells
        'probability': 'p_max_sens / (dist_x * sigma_s * sqrt(2*np.pi)) * exp(-((np.log(dist_x)-mu_s)**2)/(2*sigma_s**2))',             # probability of connection
        'weight': 'sEPSPweight',                # synaptic weight
        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
        'synMech': 'sEPSP'}                     # synaptic mechanism
netParams.connParams['sens->int'] = {           # sensory to interneuron
        'preConds': {'pop': ['sens_cell','sens_men']},             # conditions of presyn cells
        'postConds': {'pop': ['asc_cell','asc_samen','asc_ramen','desc_local','desc_5HT','desc_MMC','desc_samen','desc_ramen']},            # conditions of postsyn cells
        'probability': 'p_max_local / (dist_x * sigma_l * sqrt(2*np.pi)) * exp(-((np.log(dist_x)-mu_l)**2)/(2*sigma_l**2))',             # probability of connection
        'weight': 'fEPSPweight',                # synaptic weight
        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
        'synMech': 'fEPSP'}                     # synaptic mechanism
netParams.connParams['asc->EMN'] = {            # ascending to EMN
        'preConds': {'pop': ['asc_cell','asc_samen','asc_ramen']},            # conditions of presyn cells
        'postConds': {'pop': ['cemn_cell','cemn_samen','cemn_ramen','lemn_cell','lemn_samen','lemn_ramen']},           # conditions of postsyn cells
        'probability': 'p_max_desc / ((post_x - pre_x) * sigma_d * sqrt(2*np.pi)) * exp(-((np.log(post_x - pre_x)-mu_d)**2)/(2*sigma_d**2))',             # probability of connection
        'weight': 'fEPSPweight',                # synaptic weight
        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
        'synMech': 'fEPSP'}                     # synaptic mechanism
netParams.connParams['desc->IMN'] = {           # descending to IMN
        'preConds': {'pop': ['desc_local','desc_MMC','desc_samen','desc_ramen']},            # conditions of presyn cells
        'postConds': {'pop': ['cimn_cell','cimn_samen','cimn_ramen','limn_cell','limn_samen','limn_ramen']},           # conditions of postsyn cells
        'probability': 'p_max_desc / ((post_x - pre_x) * sigma_d * sqrt(2*np.pi)) * exp(-((np.log(post_x - pre_x)-mu_d)**2)/(2*sigma_d**2))',             # probability of connection
        'weight': 'fEPSPweight',                # synaptic weight
        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
        'synMech': 'fEPSP'}                     # synaptic mechanism
netParams.connParams['IMN->IPAN,EMN,INT'] = {          # IMN inhibition of IPAN, EMN, some INT
        'preConds': {'pop': ['cimn_cell','cimn_samen','cimn_ramen','limn_cell','limn_samen','limn_ramen']},            # conditions of presyn cells
        'postConds': {'pop': ['sens_cell','sens_men','cemn_cell','cemn_samen','cemn_ramen','lemn_cell','lemn_samen','lemn_ramen']},           # conditions of postsyn cells
        'probability': 'p_max_local / ((post_x - pre_x) * sigma_l * sqrt(2*np.pi)) * exp(-((np.log(post_x - pre_x)-mu_l)**2)/(2*sigma_l**2))',             # probability of connection
        'weight': 'IPSPweight',                 # synaptic weight
        'delay': '450+dist_x/propVelocity',         # transmission delay (ms)
        'synMech': 'IPSP'}                      # synaptic mechanism

### Simulation options
simConfig = specs.SimConfig()                   # object of class SimConfig to store simulation configuration

simConfig.hParams['v_init'] = -52
simConfig.duration = 5*1e3                      # Duration of the simulation, in ms
simConfig.dt = 0.05                             # Internal integration timestep to use
simConfig.verbose = False                       # Show detailed messages
simConfig.recordStep = 0.1                      # Step size in ms to save data (e.g. V traces, LFP, etc)

simConfig.saveDataInclude = ['netParams', 'netCells', 'netPops', 'simConfig', 'simData']
simConfig.filename = 'model_output_nn_' + str(num_neurons) + '_dur10_' + str(np.round(np.log10(simConfig.duration),2)) # Set file output name
simConfig.saveJson = True                        # Save params, network and sim output to json file
sim.updateInterval = 2*simConfig.dt
simConfig.recordLFP = [[0, 10e3, 0]]

simConfig.analysis['plotRaster'] = {'spikeHist': None, 'orderBy': 'x', 'orderInverse': 'True'} # Plot a raster
simConfig.analysis['plotLFP'] = {'electrodes': [0], 'plots': ['timeSeries'], 'figSize': (12,9)}

sim.create(netParams, simConfig)

#for i in range(len(sim.net.cells)):
#    modifier = np.random.normal(loc=0.0,scale=0.02)
#    cOn = sim.net.cells[i].secs['soma']['mechs']['cMSC']['onHalf']
#    sim.net.cells[i].secs['soma']['mechs']['cMSC']['onHalf'] = (1.0+modifier)*cOn
#    sim.net.cells[i].secs['soma']['hObj'].onHalf_cMSC = (1.0+modifier)*cOn
#    cOff = sim.net.cells[i].secs['soma']['mechs']['cMSC']['offHalf']
#    sim.net.cells[i].secs['soma']['mechs']['cMSC']['offHalf'] = (0.01*modifier)+cOff
#    sim.net.cells[i].secs['soma']['hObj'].offHalf_cMSC = (0.01*modifier)+cOff
#    tOn = sim.net.cells[i].secs['soma']['mechs']['tMSC']['onHalf']
#    sim.net.cells[i].secs['soma']['mechs']['tMSC']['onHalf'] = (1.0+modifier)*tOn
#    sim.net.cells[i].secs['soma']['hObj'].onHalf_tMSC = (1.0+modifier)*tOn
#    tOff = sim.net.cells[i].secs['soma']['mechs']['tMSC']['offHalf']
#    sim.net.cells[i].secs['soma']['mechs']['tMSC']['offHalf'] = (1.0+modifier)*tOff
#    sim.net.cells[i].secs['soma']['hObj'].offHalf_tMSC = (1.0+modifier)*tOff

#sim.runSimWithIntervalFunc(sim.updateInterval, updateStrains)
sim.simulate()
sim.analyze()