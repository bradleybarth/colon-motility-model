# -*- coding: utf-8 -*-
"""
Created on Mon Oct 14 15:33:56 2019

@author: Bradley Barth
"""

from netpyne import specs, sim
import numpy as np

# Network parameters
netParams = specs.NetParams()
num_neurons = 1

### Population parameters
netParams.popParams['interneuron'] = {'cellType': 'INT', 'numCells': num_neurons}
netParams.popParams['sensory'] = {'cellType': 'IPAN', 'numCells': num_neurons}

netParams.importCellParams(label='IPAN_rule', conds= {'cellType': 'IPAN'},
        fileName='ah_template.py', cellName='ah_cell')
netParams.importCellParams(label='INT_rule', conds= {'cellType': ['INT', 'pre']},
        fileName='s_template.py', cellName='s_cell')


### Stimulation parameters
spkTimes = [5000]#, 200, 300, 400, 500]#, 150, 250, 350, 450]
netParams.popParams['pre'] = {'cellModel': 'VecStim', 'numCells': 1, 'spkTimes': spkTimes}


### Synaptic mechanisms
netParams.synMechParams['fEPSP'] = {'mod': 'Exp2Syn', 'tau1': 0.7, 'tau2': 20.0, 'e': 0}
netParams.fEPSPweight = 0.0006 #0.0009125

netParams.synMechParams['sEPSP'] = {'mod': 'Exp2Syn', 'tau1': 25e3, 'tau2': 55e3, 'e': 0}
netParams.sEPSPweight = 0.00008

netParams.synMechParams['sIPSP'] = {'mod': 'Exp2Syn', 'tau1': 6e3, 'tau2': 8e3, 'e': -80}
netParams.sIPSPweight = 0.002

netParams.synMechParams['fIPSP'] = {'mod': 'Exp2Syn', 'tau1': 200, 'tau2': 400, 'e': -80}
netParams.fIPSPweight = 0.004

### Cell connectivity rules
netParams.connParams['pre->IPAN'] = {           # spikes -> IPAN label
        'preConds': {'pop': 'pre'},             # conditions of presyn cells
        'postConds': {'pop': 'sensory'},        # conditions of postsyn cells
        'probability': 1,                       # probability of connection
        'weight': 'sIPSPweight',                # synaptic weight
        'delay': 0,                             # transmission delay (ms)
        'synMech': 'sIPSP'}                     # synaptic mechanism

netParams.connParams['pre->INT'] = {            # spikes -> INT label
        'preConds': {'pop': 'pre'},             # conditions of presyn cells
        'postConds': {'pop': 'interneuron'},    # conditions of postsyn cells
        'probability': 1,                       # probability of connection
        'weight': 'sIPSPweight',                # synaptic weight
        'delay': 0,                             # transmission delay (ms)
        'synMech': 'sIPSP'}                     # synaptic mechanism

### Simulation options
simConfig = specs.SimConfig()                   # object of class SimConfig to store simulation configuration

simConfig.hParams['v_init'] = -52
simConfig.duration = 90e3                       # Duration of the simulation, in ms
simConfig.dt = 0.025                            # Internal integration timestep to use
simConfig.verbose = True                        # Show detailed messages
simConfig.recordTraces = {'V_soma':{'sec':'soma','loc':0.5,'var':'v'}}  # Dict with traces to record
simConfig.recordStep = 0.1                      # Step size in ms to save data (e.g. V traces, LFP, etc)
simConfig.filename = 'model_output'             # Set file output name
simConfig.savePickle = False                    # Save params, network and sim output to pickle file

simConfig.analysis['plotTraces'] = {'include': ['interneuron','sensory']}

sim.create(netParams, simConfig)
sim.simulate()
sim.analyze()