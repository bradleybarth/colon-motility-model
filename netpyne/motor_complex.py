# -*- coding: utf-8 -*-
"""
Created on Wed Dec  4 15:21:07 2019

@author: Bradley Barth
"""

from netpyne import specs, sim
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import netpyneFunctions as nF

### Network parameters
netParams = specs.NetParams()
num_smfibers = 30
num_motorneurons = 6                                            # number of motor neurons


### Population parameters
    ### Motor neurons
IMNList = []
EMNList = []
for i in range(num_motorneurons):
    IMNList.append({'cellLabel': 'IMN'+str(i), 'x': 9.5, 'y': (i-int(num_motorneurons/2))*5*(num_smfibers/num_motorneurons)})
    EMNList.append({'cellLabel': 'EMN'+str(i), 'x': 10.5, 'y': (i-int(num_motorneurons/2))*5*(num_smfibers/num_motorneurons)+2.5*(num_smfibers/num_motorneurons)})
    
netParams.popParams['IMN'] = {'cellType': 'MN', 'cellsList': IMNList}
netParams.popParams['EMN'] = {'cellType': 'MN', 'cellsList': EMNList}
netParams.importCellParams(label='MN_rule', conds= {'cellType': ['MN']},
        fileName='s_template.py', cellName='s_cell')

    ### Smooth muscle fibers
cellsList = []
for i in range(num_smfibers):
    cellsList.append({'cellLabel': 'smc'+str(i), 'x': 20, 'y': (i-int(num_smfibers/2))*5})
netParams.popParams['smc'] = {'cellType': 'SMC', 'cellsList': cellsList}# 'numCells': num_smfibers, 'ynormRange': [0, 0]}
netParams.importCellParams(label='SMC_rule', conds={'cellType': 'SMC'},
        fileName='smc_template.hoc', cellName='sm_cell')

    ### Stimulation or driver groups
NOdel1 = 2e3
NOdur1 = 38e3
NOspkTimes = list(np.linspace(NOdel1,NOdel1+NOdur1,int(NOdur1*0.002+1)))
EXdel1 = 5e3
EXdur1 = 35e3
EXspkTimes = list(np.linspace(EXdel1,EXdel1+EXdur1,int(EXdur1*0.002+1)))
GspkTimes = [10e3]
netParams.popParams['IMNdriver'] = {'cellType': 'MNDriver','cellModel': 'VecStim', 'numCells': 1, 'spkTimes': NOspkTimes, 'xRange': [0,0], 'yRange': [0,0]}
netParams.popParams['EMNdriver'] = {'cellType': 'MNDriver','cellModel': 'VecStim', 'numCells': 1, 'spkTimes': EXspkTimes, 'xRange': [1,1], 'yRange': [15,15]}
netParams.popParams['glia'] = {'cellType': 'Glia', 'cellModel': 'VecStim', 'numCells': 1, 'spkTimes': GspkTimes, 'xRange': [5,5], 'yRange': [40,40]}



### Synaptic mechanisms
netParams.synMechParams['fEPSP'] = {'mod': 'Exp2Syn', 'tau1': 0.7, 'tau2': 20.0, 'e': 0}
netParams.fEPSPweight = 0.0009125
netParams.synMechParams['IPSP'] = {'mod': 'Exp2Syn', 'tau1': 6e3, 'tau2': 8e3, 'e': -75}
netParams.IPSPweight = 0.002

netParams.synMechParams['EJP'] = {'mod': 'Exp2Syn', 'tau1': 45, 'tau2': 90, 'e': 0}
netParams.EJPweight = 0.0048 #originally tuned to 0.0012 for single EJP in single, non-gap-junctioned smc;
# but with 5 fibers and 0.0048 S/cm2 gap junctions, need twice the conductance

netParams.synMechParams['fIJP'] = {'mod': 'Exp2Syn', 'tau1': 200, 'tau2': 100, 'e': -70}
netParams.synMechParams['sIJP'] = {'mod': 'Exp2Syn', 'tau1': 900, 'tau2': 850, 'e': -70}
netParams.IJPweight = 0.002

netParams.synMechParams['esyn'] = {'mod': 'ElectSyn', 'g': 0.025} #200nS -> 0.0387 S/cm2 or 306nS -> 0.025 S/cm2 approx. from Edwards and Hirst, 2005
gapList = []
for i in range(num_smfibers-1): gapList.append([i,i+1])


#innervation = int(num_smfibers/2)-2
#innervList = []
#for i in range(num_motorneurons): innervList.append([i,innervation+np.mod(i,4)])
innervList = []
for i in range(num_smfibers): 
    innervList.append([int(i/np.ceil(num_smfibers/num_motorneurons)),i])


### Cell connectivity rules
netParams.connParams['EXdriver->EMN'] = {       # excitatory drivers into excitatory motor neurons      
        'preConds': {'pop': 'EMNdriver'},           # conditions of presyn cells
        'postConds': {'pop': 'EMN'},                # conditions of postsyn cells
        'probability ': 1,                          # 100% connection probability
        'weight': 'fEPSPweight',                    # synaptic weight
        'delay': 0,                                 # transmission delay (ms)
        'synMech': 'fEPSP'}                         # synaptic mechanism
netParams.connParams['INdriver->IMN'] = {       # inhibitory drviers into inhibitory motor neurons      
        'preConds': {'pop': 'IMNdriver'},      
        'postConds': {'pop': 'IMN'},           
        'probability ': 1,
        'weight': 'fEPSPweight',                
        'delay': 0,                             
        'synMech': 'fEPSP'}                     
netParams.connParams['motor_gap'] = {           # gap junctions
        'preConds': {'pop': 'smc'},             
        'postConds': {'pop': 'smc'},            
        'connList': gapList,                        # explicit connection from neighboring IDs
        'weight': 1,                            
        'delay': 0,                              
        'synMech': 'esyn',                          # electrical synapse
        'gapJunction': True,                        # gap junction
        'loc': 0.1,                                 # post synaptic at "left" of cell
        'preLoc': 0.9}                              # pre synaptic at "right" of cell NOTE: bidirectional anyway
netParams.connParams['in->SMC'] = {             # inhibitory motor neurons fIJP and sIJP -> SMC
        'preConds': {'pop': 'IMN'},             
        'postConds': {'pop': 'smc'},            
        'connList': innervList,
        'weight': 'IJPweight',                  
        'delay': 0,                             
        'synMech': ['fIJP','sIJP']}               
netParams.connParams['ex->SMC'] = {             # excitatory motor neurons EJP -> SMC
        'preConds': {'pop': 'EMN'},             
        'postConds': {'pop': 'smc'},            
        'connList': innervList,
        'weight': 'EJPweight',                  
        'delay': 0,                             
        'synMech': 'EJP'}              
netParams.connParams['glia->IMN'] = {           # glia spikes inhibit IMN
        'preConds': {'pop': 'glia'},            
        'postConds': {'pop': 'IMN'},           
        'connList': [[0,int(num_motorneurons/2)],[0,int(num_motorneurons/2)-1]],                 
        'weight': 'IPSPweight',                  
        'delay': 0,                             
        'synMech': 'IPSP'}                      


### Simulation options
simConfig = specs.SimConfig()                   # object of class SimConfig to store simulation configuration

simConfig.hParams['v_init'] = -40
simConfig.duration = 40e3                       # Duration of the simulation, in ms
simConfig.dt = 0.025                            # Internal integration timestep to use
simConfig.verbose = False                       
simConfig.recordTraces = {'Muscle Fibers':{'sec':'soma','loc':0.5,'var':'v'}}
simConfig.recordStep = 1
simConfig.recordLFP = [[0, 10e3, 0]]

simConfig.filename = 'simDataOutput'
simConfig.saveDataInclude = 'simData'
simConfig.saveMat = False

simConfig.analysis['plotTraces'] = {'include': ['smc'], 'figSize': (12,9),'overlay': 'True', 'oneFigPer':'trace'}
#simConfig.analysis['plotRaster'] = {'include':['IMN','EMN'], 'figSize': (12,9), 'popRates': 'overlay'}
simConfig.analysis['plotLFP'] = {'electrodes': [0], 'plots': ['timeSeries'], 'figSize': (12,9)}
simConfig.analysis['plot2Dnet'] = {'figSize': (12,9)}

sim.create(netParams, simConfig)
sim.simulate()
sim.analyze()

columns = sim.simData['Muscle Fibers'].keys()
columns = list(columns)
columns = columns[0:num_smfibers]
df = pd.DataFrame(sim.simData['Muscle Fibers'], columns = np.array(columns))
df = np.array(df)
plt.figure(figsize = [12,9])
smoothDF = nF.movAvg(df,n=40)
plt.imshow(np.transpose(smoothDF), aspect = 'auto', vmin = -65, vmax = 0, cmap='gray')
plt.xticks(list(np.linspace(0,simConfig.duration,5)),list(np.linspace(0,simConfig.duration/1e3,5)))
plt.xlabel('Time (sec)')
plt.ylabel('Cell ID')
cb = plt.colorbar()
cb.set_label('Membrane Potential (mV)')
plt.title('Smooth Muscle')

nF.plotConns(sim)