from matplotlib import pyplot
import random
from datetime import datetime
import pickle
from neuron import h


# template for intrinsic primary afferent neurons (ipan) by Chamers et al. 2013
# https://senselab.med.yale.edu/modeldb/ShowModel.cshtml?model=155796&file=/ChambersEtAl2013/mod/nav17.mod#tabs-1
# doi: 10.1152/ajpgi.00228.2013
# adapted by Bradley Barth on October 24th, 2019


class Cell(object):
    def __init__(self):
        self.synlist = []
        self.createSections()
        self.defineGeometry()
        self.defineBiophysics()
        self.createSynapses()
        self.nclist = []

    def createSections(self):
        pass

    def buildTopology(self):
        pass

    def defineGeometry(self):
        pass

    def defineBiophysics(self):
        pass

    def createSynapses(self):
        """Add an exponentially decaying synapse """
        synsoma = h.ExpSyn(self.soma(0.5))
        synsoma.tau = 2
        synsoma.e = 0
        self.synlist.append(synsoma) # synlist is defined in Cell


    def createNetcon(self, thresh=10):
        """ created netcon to record spikes """
        nc = h.NetCon(self.soma(0.5)._ref_v, None, sec = self.soma)
        nc.threshold = thresh
        return nc
        
        
class ah_ramen(Cell): 
    """AH-MEN cell: A soma with active channels""" 
    def createSections(self):
        """Create the sections of the cell."""
        self.soma = h.Section(name='soma', cell=self)

    def defineGeometry(self):
        """Set the 3D geometry of the cell."""
        self.soma.L = 49.0
        self.soma.diam = 25.0
        self.soma.Ra = 30
        self.soma.cm = 1
    
    def defineBiophysics(self):
        """Assign the membrane properties across the cell."""
        # Insert active channels in the soma        
        self.soma.insert('pas')
        self.soma.g_pas = 4e-5  # Passive conductance in S/cm2
        self.soma.e_pas = -55    # Leak reversal potential mV
        
        self.soma.insert('nav13')
        self.soma.gbar_nav13 = 3e-1
        self.soma.sh_nav13 = 8
        self.soma.tha_nav13 = -30.5
        self.soma.qa_nav13 = 4.5
        self.soma.Ra_nav13 = 0.4
        self.soma.Rb_nav13 = 0.135
        self.soma.thi1_nav13 = -30
        self.soma.thi2_nav13 = -30
        self.soma.qd_nav13 = 1.5
        self.soma.qg_nav13 = 1.5
        self.soma.mmin_nav13 = 0.02	
        self.soma.hmin_nav13 = 0.5			
        self.soma.q10_nav13 = 2
        self.soma.Rg_nav13 = 0.01
        self.soma.Rd_nav13 = 0.03
        self.soma.qq_nav13 = 10
        self.soma.tq_nav13 = -50
        self.soma.thinf_nav13 = -55
        self.soma.qinf_nav13 = 4
        self.soma.vhalfs_nav13 = -60
        self.soma.a0s_nav13 = 0.0003
        self.soma.zetas_nav13 = 12
        self.soma.gms_nav13 = 0.2
        self.soma.smax_nav13 = 10
        self.soma.vvh_nav13 = -58
        self.soma.vvs_nav13 = 2
        self.soma.ar2_nav13 = 1

        self.soma.insert('nav17')
        self.soma.gnabar_nav17 = 1e-1
        self.soma.mam_nav17 = 5
        self.soma.sam_nav17 = -12.08
        self.soma.mbm_nav17 = 72.7
        self.soma.sbm_nav17 = 16.7
        self.soma.mah_nav17 = 122.35
        self.soma.sah_nav17 = 15.29
        self.soma.mas_nav17 = 93.9
        self.soma.sas_nav17 = 16.6
        self.soma.alphaD_nav17 = 0.05
        self.soma.betaD_nav17 = 0.02

        self.soma.insert('nap')
        self.soma.gbar_nap = 1e-5

        self.soma.insert('jcal')
        self.soma.TotalBuffer_jcal = 5e-3
        self.soma.k1buf_jcal = 3
        self.soma.k2buf_jcal = 0.005
        self.soma.depth_jcal = 1
        self.soma.taur_jcal = 1e1
        self.soma.cainf_jcal = 5e-5
        self.soma.cain_jcal = 0.5e0
        self.soma.gcaca_jcal = 0.2e-4
        self.soma.cacamid_jcal = 1e-4
        self.soma.cacas_jcal = 2e-5

        self.soma.insert('can')
        self.soma.gbar_can = 4e-2
        self.soma.mafac_can = 0.1
        self.soma.mamid_can = -15
        self.soma.maslope_can = 5
        self.soma.mbfac_can = 0.5
        self.soma.mbmid_can = -5
        self.soma.mbslope_can = 10
        self.soma.hafac_can = 0.01
        self.soma.hamid_can = 50
        self.soma.haslope_can = 15
        self.soma.hbfac_can = 0.2
        self.soma.hbmid_can = 40
        self.soma.hbslope_can = 17
        
        self.soma.insert('kdr')
        self.soma.gbar_kdr = 1e-2
        self.soma.nimid_kdr = 0
        self.soma.nislope_kdr = 25
        self.soma.ntmid_kdr = 27
        self.soma.ntslope_kdr = 15
        
        self.soma.insert('ka')
        self.soma.gbar_ka = 1e-2
        self.soma.amid_ka = 30
        self.soma.aslope_ka = 20
        self.soma.bmid_ka = 80
        self.soma.bslope_ka = 6
        self.soma.btau_ka = 15
        self.soma.atau_ka = 0.5
        
        self.soma.insert('kca_fast')
        self.soma.gbar_kca_fast = 3e-2
        self.soma.cac_kca_fast = 7e-4
        self.soma.cas_kca_fast = 7.5e-5
        self.soma.v1_kca_fast = 50
        self.soma.v2_kca_fast = 53.5
        self.soma.s1_kca_fast = 11
        self.soma.s2_kca_fast = 27
        self.soma.aspeed_kca_fast = 1
        self.soma.bspeed_kca_fast = 1
        
        self.soma.insert('kca_slow')
        self.soma.gbar_kca_slow = 0.5e0
        self.soma.Ra_kca_slow = 0.3e-3
        self.soma.Rb_kca_slow = 6e-5
        self.soma.cac_kca_slow = 2.5e-4
        self.soma.cas_kca_slow = 2.5e-5
        
        self.soma.insert('cansc')
        self.soma.gbar_cansc = 0.5e-4
        self.soma.beta_cansc = 1e-4
        self.soma.cac_cansc = 6e-4
        self.soma.cas_cansc = 2e-5
        self.soma.ratc_cansc = 1e-1
        self.soma.x_cansc = 1
        self.soma.erev_cansc = -38
        self.soma.taumin_cansc = -0.1e0
        
        self.soma.insert('ih')
        self.soma.gbar_ih = 0.5e-4
        self.soma.vhalf_ih = -72
        self.soma.vslope1_ih = 8.2
        self.soma.vslope2_ih = 11.9
        self.soma.tmc1_ih = 537
        self.soma.tmc2_ih = 56
        
        self.soma.insert('cMSC')
        self.soma.gbar_cMSC = 0.0005
        self.soma.onHalf_cMSC = 0.07
        self.soma.onTauOffset_cMSC = 0.01
        self.soma.onTauSlope_cMSC = 0.005
        self.soma.onTauHalf_cMSC = 0.01
        self.soma.offTauOffset_cMSC = 0.1667
        self.soma.onTauMax_cMSC = 500
        self.soma.offTauMax_cMSC = 300
        self.soma.k1_cMSC = 1
        self.soma.B_cMSC = 100
        
        self.soma.insert('tMSC')
        self.soma.gbar_tMSC = 0.0
        
class ah_cell(Cell): 
    """AH cell: A soma with active channels""" 
    def createSections(self):
        """Create the sections of the cell."""
        self.soma = h.Section(name='soma', cell=self)

    def defineGeometry(self):
        """Set the 3D geometry of the cell."""
        self.soma.L = 49.0
        self.soma.diam = 25.0
        self.soma.Ra = 30
        self.soma.cm = 1
    
    def defineBiophysics(self):
        """Assign the membrane properties across the cell."""
        # Insert active channels in the soma        
        self.soma.insert('pas')
        self.soma.g_pas = 4e-5  # Passive conductance in S/cm2
        self.soma.e_pas = -55    # Leak reversal potential mV
        
        self.soma.insert('nav13')
        self.soma.gbar_nav13 = 3e-1
        self.soma.sh_nav13 = 8
        self.soma.tha_nav13 = -30.5
        self.soma.qa_nav13 = 4.5
        self.soma.Ra_nav13 = 0.4
        self.soma.Rb_nav13 = 0.135
        self.soma.thi1_nav13 = -30
        self.soma.thi2_nav13 = -30
        self.soma.qd_nav13 = 1.5
        self.soma.qg_nav13 = 1.5
        self.soma.mmin_nav13 = 0.02	
        self.soma.hmin_nav13 = 0.5			
        self.soma.q10_nav13 = 2
        self.soma.Rg_nav13 = 0.01
        self.soma.Rd_nav13 = 0.03
        self.soma.qq_nav13 = 10
        self.soma.tq_nav13 = -50
        self.soma.thinf_nav13 = -55
        self.soma.qinf_nav13 = 4
        self.soma.vhalfs_nav13 = -60
        self.soma.a0s_nav13 = 0.0003
        self.soma.zetas_nav13 = 12
        self.soma.gms_nav13 = 0.2
        self.soma.smax_nav13 = 10
        self.soma.vvh_nav13 = -58
        self.soma.vvs_nav13 = 2
        self.soma.ar2_nav13 = 1

        self.soma.insert('nav17')
        self.soma.gnabar_nav17 = 1e-1
        self.soma.mam_nav17 = 5
        self.soma.sam_nav17 = -12.08
        self.soma.mbm_nav17 = 72.7
        self.soma.sbm_nav17 = 16.7
        self.soma.mah_nav17 = 122.35
        self.soma.sah_nav17 = 15.29
        self.soma.mas_nav17 = 93.9
        self.soma.sas_nav17 = 16.6
        self.soma.alphaD_nav17 = 0.05
        self.soma.betaD_nav17 = 0.02

        self.soma.insert('nap')
        self.soma.gbar_nap = 1e-5

        self.soma.insert('jcal')
        self.soma.TotalBuffer_jcal = 5e-3
        self.soma.k1buf_jcal = 3
        self.soma.k2buf_jcal = 0.005
        self.soma.depth_jcal = 1
        self.soma.taur_jcal = 1e1
        self.soma.cainf_jcal = 5e-5
        self.soma.cain_jcal = 0.5e0
        self.soma.gcaca_jcal = 0.2e-4
        self.soma.cacamid_jcal = 1e-4
        self.soma.cacas_jcal = 2e-5

        self.soma.insert('can')
        self.soma.gbar_can = 4e-2
        self.soma.mafac_can = 0.1
        self.soma.mamid_can = -15
        self.soma.maslope_can = 5
        self.soma.mbfac_can = 0.5
        self.soma.mbmid_can = -5
        self.soma.mbslope_can = 10
        self.soma.hafac_can = 0.01
        self.soma.hamid_can = 50
        self.soma.haslope_can = 15
        self.soma.hbfac_can = 0.2
        self.soma.hbmid_can = 40
        self.soma.hbslope_can = 17
        
        self.soma.insert('kdr')
        self.soma.gbar_kdr = 1e-2
        self.soma.nimid_kdr = 0
        self.soma.nislope_kdr = 25
        self.soma.ntmid_kdr = 27
        self.soma.ntslope_kdr = 15
        
        self.soma.insert('ka')
        self.soma.gbar_ka = 1e-2
        self.soma.amid_ka = 30
        self.soma.aslope_ka = 20
        self.soma.bmid_ka = 80
        self.soma.bslope_ka = 6
        self.soma.btau_ka = 15
        self.soma.atau_ka = 0.5
        
        self.soma.insert('kca_fast')
        self.soma.gbar_kca_fast = 3e-2
        self.soma.cac_kca_fast = 7e-4
        self.soma.cas_kca_fast = 7.5e-5
        self.soma.v1_kca_fast = 50
        self.soma.v2_kca_fast = 53.5
        self.soma.s1_kca_fast = 11
        self.soma.s2_kca_fast = 27
        self.soma.aspeed_kca_fast = 1
        self.soma.bspeed_kca_fast = 1
        
        self.soma.insert('kca_slow')
        self.soma.gbar_kca_slow = 0.5e0
        self.soma.Ra_kca_slow = 0.3e-3
        self.soma.Rb_kca_slow = 6e-5
        self.soma.cac_kca_slow = 2.5e-4
        self.soma.cas_kca_slow = 2.5e-5
        
        self.soma.insert('cansc')
        self.soma.gbar_cansc = 0.5e-4
        self.soma.beta_cansc = 1e-4
        self.soma.cac_cansc = 6e-4
        self.soma.cas_cansc = 2e-5
        self.soma.ratc_cansc = 1e-1
        self.soma.x_cansc = 1
        self.soma.erev_cansc = -38
        self.soma.taumin_cansc = -0.1e0
        
        self.soma.insert('ih')
        self.soma.gbar_ih = 0.5e-4
        self.soma.vhalf_ih = -72
        self.soma.vslope1_ih = 8.2
        self.soma.vslope2_ih = 11.9
        self.soma.tmc1_ih = 537
        self.soma.tmc2_ih = 56
    
        self.soma.insert('cMSC')
        self.soma.gbar_cMSC = 0.0
    
        self.soma.insert('tMSC')
        self.soma.gbar_tMSC = 0.0