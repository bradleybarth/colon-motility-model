# -*- coding: utf-8 -*-
"""
Created on Mon Dec 16 15:16:49 2019

@author: Bradley Barth
"""

from netpyne import specs, sim
import numpy as np
import matplotlib.pyplot as plt

# Network parameters
netParams = specs.NetParams()
num_neurons = 8

### Population parameters
#netParams.popParams['RAMEN'] = {'cellType': 'SENS', 'numCells': num_neurons}
#netParams.importCellParams(label='SENS_rule', conds= {'cellType': ['SENS']},
#        fileName='ah_template.py', cellName='ah_ramen')


netParams.popParams['AH_Cell'] = {'cellType': 'AH', 'numCells': num_neurons}
netParams.importCellParams(label='AH_rule', conds= {'cellType': ['AH']},
        fileName='ah_template.py', cellName='ah_ramen')


netParams.popParams['S_Cell'] = {'cellType': 'S', 'numCells': num_neurons}
netParams.importCellParams(label='S_rule', conds= {'cellType': ['S']},
        fileName='s_template.py', cellName='s_ramen')

pops = list(netParams.popParams.keys())
total_cells = 0
for i in range(len(pops)):
    total_cells += netParams.popParams[pops[i]]['numCells']

amp = 0.2
rate1 = 0.001
rate2 = 0.0005

### Compression driver
comp_delay = 2e3
comp_dur1 = 4e3
comp_dur2 = 0
comp_amp1 = amp
comp_amp2 = 0.5*amp
comp_rate1 = rate1 #(elongation per ms)
comp_rate2 = rate2 #(elongation per ms)

### Tension driver
tens_delay = 9e3
tens_dur = 4e3
tens_amp = amp
tens_rate1 = rate1 #(elongation per ms)
tens_rate2 = rate2 #(elongation per ms)

tens = list()
comp = list()
for i in range(num_neurons):
    tens.append(list())
    comp.append(list())
    
def updateStrains(t):
    if (t==sim.updateInterval):
        sim.lastCompression = 0
        sim.lastTension = 0
    # Update compression
    if t < comp_delay:
        thisCompression = 0
    if t >= comp_delay and t < comp_amp1/comp_rate1 + comp_delay:
        thisCompression = (t-comp_delay)*comp_rate1
    if t >= comp_amp1/comp_rate1 + comp_delay and t < comp_dur1 + comp_delay:
        thisCompression = comp_amp1
    if t >= comp_delay + comp_dur1 and t < comp_dur1 + comp_delay + abs(comp_amp2-comp_amp1)/comp_rate1:
        thisCompression = comp_amp1 + (comp_amp2-comp_amp1)/abs(comp_amp2-comp_amp1)*(t - (comp_dur1+comp_delay))*comp_rate1
    if t >= comp_dur1 + comp_delay + abs(comp_amp2-comp_amp1)/comp_rate1 and t < comp_dur1 + comp_delay + comp_dur1 + comp_delay + comp_dur2:
        thisCompression = comp_amp2
    if t >= comp_dur1 + comp_delay + comp_dur2 and t < comp_dur2 + comp_dur1 + comp_delay +  comp_amp2/comp_rate2:
        thisCompression = comp_amp2 - (t - (comp_dur1+comp_dur2+comp_delay))*comp_rate2
    if t >= comp_dur2 + comp_dur1 + comp_delay + comp_amp2/comp_rate2:
        thisCompression = 0
    dCompression = abs(thisCompression-sim.lastCompression)/sim.updateInterval
    
    # Update tension        
    if t < tens_delay:
        thisTension = 0
    if t >= tens_delay and t < tens_amp/tens_rate1 + tens_delay:
        thisTension = (t-tens_delay)*tens_rate1
    if t >= tens_amp/tens_rate1 + tens_delay and t < tens_dur + tens_delay:
        thisTension = tens_amp
    if t >= tens_dur + tens_delay and t < tens_dur + tens_delay + tens_amp/tens_rate2:
        thisTension = tens_amp - (t - (tens_dur+tens_delay))*tens_rate2
    if t >= tens_dur + tens_delay + tens_amp/tens_rate2:
        thisTension = 0
    dTension = abs(thisTension-sim.lastTension)/sim.updateInterval
        
    # Set values ####BUG HERE FIX IT ASAP
    for i in range(num_neurons):
        for j in range(len(pops)):
            thisCell = sim.net.pops[pops[j]].cellGids[i]
            comp[i].append(((i+1)/(num_neurons))*thisCompression)
            sim.net.cells[thisCell].secs['soma']['mechs']['cMSC']['compression'] = (i+1)/(num_neurons)*thisCompression
            sim.net.cells[thisCell].secs['soma']['hObj'].compression_cMSC = (i+1)/(num_neurons)*thisCompression
            sim.net.cells[thisCell].secs['soma']['mechs']['cMSC']['dcompression'] = (i+1)/(num_neurons)*dCompression
            sim.net.cells[thisCell].secs['soma']['hObj'].dcompression_cMSC = (i+1)/(num_neurons)*dCompression
            tens[i].append(((i+1)/(num_neurons))*thisTension)
            sim.net.cells[thisCell].secs['soma']['mechs']['tMSC']['tension'] = (i+1)/(num_neurons)*thisTension
            sim.net.cells[thisCell].secs['soma']['hObj'].tension_tMSC = (i+1)/(num_neurons)*thisTension
    
    sim.lastCompression = thisCompression
    sim.lastTension = thisTension
    
    return sim

### Simulation options
simConfig = specs.SimConfig()                   # object of class SimConfig to store simulation configuration

simConfig.hParams['v_init'] = -40
simConfig.duration = 16e3                        # Duration of the simulation, in ms
simConfig.dt = 0.025                            # Internal integration timestep to use
simConfig.verbose = False                        # Show detailed messages
simConfig.recordTraces = {'V_soma':{'sec':'soma','loc':0.5,'var':'v'}}  # Dict with traces to record
simConfig.recordStep = 0.1                      # Step size in ms to save data (e.g. V traces, LFP, etc)
simConfig.filename = 'model_output'             # Set file output name
simConfig.saveMat = False                    # Save params, network and sim output to pickle file
sim.updateInterval = 2*simConfig.dt
simConfig.analysis['plotTraces'] = {'include': ['allCells'], 'overlay': True, 'oneFigPer': 'trace', 'figSize': (12,9)}
simConfig.analysis['plotRaster'] = {'figSize': (12,9)}

sim.create(netParams, simConfig)
sim.runSimWithIntervalFunc(sim.updateInterval, updateStrains)
#sim.simulate()
sim.analyze()

plt.figure(figsize = (12,9))
plt.subplot(2*len(pops)+2,1,2*len(pops)+1)
for i in range(num_neurons):
    plt.plot(np.linspace(0,simConfig.duration/1e3,len(comp[i])),comp[i])
plt.xticks([])
plt.ylabel('Compression')
plt.xlim([0,simConfig.duration/1e3])

plt.subplot(2*len(pops)+2,1,2*len(pops)+2)
for i in range(num_neurons):
    plt.plot(np.linspace(0,simConfig.duration/1e3,len(tens[i])),tens[i])
plt.xlabel('Time (sec)')
plt.ylabel('Tension')
plt.xlim([0,simConfig.duration/1e3])

cellNames = list(sim.simData['V_soma'].keys())
thisCell = 0
for i in range(len(pops)):
    plt.subplot(int((2*len(pops)+2)/2),1,i+1)
    for j in range(num_neurons):
        thisPlot = sim.simData['V_soma'][cellNames[thisCell]]
        plt.plot(np.linspace(0,simConfig.duration/1e3,len(thisPlot)),thisPlot)
        thisCell += 1
    plt.ylabel('Membrane Potential (mV)')
    plt.ylim([-65, 50])
    plt.yticks([-60,-40,-20,0,20,40])
    plt.text(0.5,40,pops[i],verticalalignment='center',fontsize=10)
    plt.xlim([0,simConfig.duration/1e3])
plt.show()

plt.figure(figsize = (12,9))
plt.subplot(2*len(pops)+2,1,2*len(pops)+1)
for i in range(num_neurons):
    plt.plot(np.linspace(0,simConfig.duration/1e3,len(comp[i])),comp[i])
plt.xticks([])
plt.ylabel('Compression')
plt.xlim([0,simConfig.duration/1e3])

plt.subplot(2*len(pops)+2,1,2*len(pops)+2)
for i in range(num_neurons):
    plt.plot(np.linspace(0,simConfig.duration/1e3,len(tens[i])),tens[i])
plt.xlabel('Time (sec)')
plt.ylabel('Tension')
plt.xlim([0,simConfig.duration/1e3])
    
spkID = np.array(sim.simData['spkid'], dtype = int)
spkT = np.array(sim.simData['spkt'], dtype = int)
for i in range(len(pops)):
    plt.subplot(int((2*len(pops)+2)/2),1,i+1)
    labY = sim.net.pops[pops[i]].cellGids
    labT = list()
    theseSpikes = list()
    for j in range(num_neurons):
        thisCell = sim.net.pops[pops[i]].cellGids[j]
        note = str(int(round(max(tens[j]),2)*100)) + '%'
        labT.append(note)
#        plt.text(0.5,thisCell,note,verticalalignment='center',fontsize=8)
        temp = [k for k, x in enumerate(list(spkID)) if x == thisCell]
        for n in range(len(temp)):
            theseSpikes.append(temp[n])
    plt.scatter(spkT[theseSpikes]/1e3,spkID[theseSpikes],50*np.ones((1,len(theseSpikes))),marker='|')
    plt.ylim([min(labY)-0.5,max(labY)+1.5])
    plt.xlim([0,simConfig.duration/1e3])
    if (i == 0): plt.xticks([])
    else: plt.xlabel('Time (sec)')
    plt.yticks(labY,labT)
    plt.ylabel('Elongation')
    plt.text(0.5,max(labY),pops[i])
    plt.text(comp_delay/1e3,max(labY)+1,'Compression',verticalalignment='center')
    plt.text(tens_delay/1e3,max(labY)+1,'Tension',verticalalignment='center')
plt.show()