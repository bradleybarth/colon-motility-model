# -*- coding: utf-8 -*-
"""
Created on Wed Jan 15 08:22:11 2020

@author: Bradley Barth
"""

def unique(list1): 
    import numpy as np
    x = np.array(list1) 
    return(list(np.unique(x)))

import json
import matplotlib.pyplot as plt
import seaborn as sb
import numpy as np

filename = "saved_data\\iterate_2020.01.19_convergence.json"
if filename:
    with open(filename,'r') as f:
        DS = json.load(f)

parameters = list(DS.keys())
xParamVal = parameters
yParamVal = ['syncMeasure','syncRate','connsPerCell','synsPerCell']
yParamScale = [[0,1],[0,12.5],[0,4.5],[0,4.5]]

plt.figure(figsize = [12,12])
for i in range(len(parameters)):
    xParam = list()
    yParam = [list(), list(), list(), list()]
    for j in range(len(DS[parameters[i]])):
        if parameters[i] == 'simDur':
            xParam.append(DS[parameters[i]][j]['input'][xParamVal[i]]/1e3)
        elif parameters[i] == 'num_neurons':
            xParam.append(DS[parameters[i]][j]['input'][xParamVal[i]])
        elif parameters[i] == 'nInputs':
            xParam.append(DS[parameters[i]][j]['input'][xParamVal[i]])
        for k in range(len(yParam)):
            yParam[k].append(DS[parameters[i]][j][yParamVal[k]])
    
    for k in range(len(yParamVal)):
        plt.subplot(len(yParamVal),len(parameters),k*len(parameters)+i+1)
        ax = plt.gca()
        ax.scatter(xParam,yParam[k])
        ax.set_xscale('log')
        plt.xticks(unique(xParam),unique(xParam))
        if k == 0: plt.title(xParamVal[i])
        plt.ylabel(yParamVal[k])
        plt.grid()
        plt.ylim(yParamScale[k])
        
        
yParamVal = ['syncRate','syncMeasure']   
yParamLabel = ['Output (Hz)','Synchrony [0 1]']  
xParamLabel = ['# of Neurons','Duration (s)','# of Inputs']   
yParamScale = [[[-0.5,6.5],[0,1]],[[-0.5,6.5],[0,1]],[[-0.5,13],[0,1]]]     
for i in range(len(parameters)):
    plt.figure(figsize = [6,9])
    xParam = list()
    yParam = [list(), list()]
    for j in range(len(DS[parameters[i]])):
        if parameters[i] == 'simDur':
            xParam.append(DS[parameters[i]][j]['input'][xParamVal[i]]/1e3)
        elif parameters[i] == 'num_neurons':
            xParam.append(DS[parameters[i]][j]['input'][xParamVal[i]])
        elif parameters[i] == 'nInputs':
            xParam.append(DS[parameters[i]][j]['input'][xParamVal[i]])
        for k in range(len(yParam)):
            yParam[k].append(DS[parameters[i]][j][yParamVal[k]])
    
    for k in range(len(yParamVal)):
        plt.subplot(len(yParamVal),1,k+1)
        ax = plt.gca()
        ax.scatter(xParam,yParam[k])
        ax.set_xscale('log')
        plt.xticks(unique(xParam),unique(xParam))
        if k == 1: plt.xlabel(xParamLabel[i])
        plt.ylabel(yParamLabel[k])
        plt.grid()
        plt.ylim(yParamScale[i][k])        



filename = "saved_data\\iterate_2020.01.17_09.10.58_FreqInputs.json"
if filename:
    with open(filename,'r') as f:
        DS = json.load(f)

parameters = list(DS.keys())
xParamVal = parameters
yParamVal = ['syncMeasure','syncRate','connsPerCell','synsPerCell']
#yParamScale = [[0,1],[-0.5,6.5],[-0.5,3.5],[-0.5,4]]

inputFreq = list()
inputNoise = list()
syncMeasure = list()
syncRate = list()
for i in range(len(DS['2DfreqOutputs'])):
    inputFreq.append(DS['2DfreqOutputs'][i]['input']['inputFreq'])
    inputNoise.append(DS['2DfreqOutputs'][i]['input']['inputNoise'])
    syncMeasure.append(DS['2DfreqOutputs'][i]['syncMeasure'])
    syncRate.append(DS['2DfreqOutputs'][i]['syncRate'])
    

syncMeasureHeatMap = np.array(np.zeros([len(list(set(inputFreq))),len(list(set(inputNoise))),4]))
syncRateHeatMap = np.array(np.zeros([len(list(set(inputFreq))),len(list(set(inputNoise))),4]))

fig = plt.figure(figsize = [12,9])

count = 0
for i in range(len(list(set(inputNoise)))):
    for j in range(len(list(set(inputFreq)))):
        for k in range(4):
            syncMeasureHeatMap[j,i,k] = syncMeasure[count]
            syncRateHeatMap[j,i,k] = syncRate[count]
            count = count + 1
            
avgRate=np.mean(syncRateHeatMap,2).transpose()
avgRate=np.flipud(avgRate)
stdRate=np.std(syncRateHeatMap,2).transpose()
stdRate=np.flipud(stdRate)
avgMeas=np.mean(syncMeasureHeatMap,2).transpose()
avgMeas=np.flipud(avgMeas)
stdMeas=np.std(syncMeasureHeatMap,2).transpose()
stdMeas=np.flipud(stdMeas)
            
ax = plt.subplot(221)
ax = sb.heatmap(avgRate, annot=True, vmin = 0,vmax = 5, cbar = False,
                xticklabels = unique(inputFreq), yticklabels = np.flip(unique(inputNoise)))
ax.set_ylabel('Input Noise [0 1]')
ax.set_title('Synchronization Rate (Hz)')

ax = plt.subplot(223)
ax = sb.heatmap(stdRate/avgRate, annot=True, vmin = 0, vmax = 0.2, cbar = False,
                xticklabels = unique(inputFreq), yticklabels = np.flip(unique(inputNoise)))
ax.set_ylabel('Input Noise [0 1]')
ax.set_xlabel('Input Frequency (Hz)')
ax.set_title('Synchronization Rate CV')

ax = plt.subplot(222)
ax = sb.heatmap(avgMeas, annot=True, vmin = 0, vmax = 1, cbar = False,
                xticklabels = unique(inputFreq), yticklabels = np.flip(unique(inputNoise)))
ax.set_title('Synchronization Measure [0 1]')

ax = plt.subplot(224)
ax = sb.heatmap(stdMeas/avgMeas, annot=True, vmin = 0, vmax = 0.05, cbar = False,
                xticklabels = unique(inputFreq), yticklabels = np.flip(unique(inputNoise)))
ax.set_xlabel('Input Frequency (Hz)')
ax.set_title('Synchronization Measure CV')

plt.figure()
ax = plt.subplot(111)
ax = sb.heatmap(avgRate, annot=True, vmin = 0,vmax = 5, cbar = False,
                xticklabels = unique(inputFreq), yticklabels = np.flip(unique(inputNoise)))
ax.set_ylabel('Input Noise [0 1]')
ax.set_xlabel('Input Frequency (Hz)')
ax.set_title('Synchronization Rate (Hz)')

plt.figure()
ax = plt.subplot(111)
ax = sb.heatmap(avgMeas, annot=True, vmin = 0, vmax = 1, cbar = False,
                xticklabels = unique(inputFreq), yticklabels = np.flip(unique(inputNoise)))
ax.set_title('Synchronization Measure [0 1]')
ax.set_ylabel('Input Noise [0 1]')
ax.set_xlabel('Input Frequency (Hz)')