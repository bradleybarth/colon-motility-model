# -*- coding: utf-8 -*-
"""
Created on Mon Nov 18 09:50:55 2019

@author: Bradley Barth
"""
def movAvg(a, n=3, dim=1):
    import numpy as np
    for i in range(a.shape[dim]):
        row = np.cumsum(a[:,i], dtype=float)
        row[n:] = row[n:] - row[:-n]
        a[n-1:,i] = row[n - 1:] / n
    return a

def unique(list1): 
    import numpy as np
    x = np.array(list1) 
    return(list(np.unique(x)))

def plotConns(sim):
    import matplotlib.pyplot as plt
    import numpy as np
    
    cells = dict(pop=list(),x=list(),y=list(),popColor=list(),cellType=list())
    conns = dict(
                x = dict(start=list(),dx=list()),
                y = dict(start=list(),dy=list()),
                type = list(),
            )
    labs = [*sim.net.pops]
    for i in range(len(sim.net.cells)):
        cells['pop'].append(sim.net.cells[i].tags['pop'])
        cells['x'].append(sim.net.cells[i].tags['x']/1e3)
        cells['y'].append(sim.net.cells[i].tags['y'])
        cells['cellType'].append(sim.net.cells[i].tags['cellType'])
        
        popID = labs.index(sim.net.cells[i].tags['pop'])
        cells['popColor'].append('C'+str(np.remainder(popID,10)))
        
        for j in range(len(sim.net.cells[i].conns)):
            pre = sim.net.cells[i].conns[j]['preGid']
            post = i
            if isinstance(pre,int):
                conns['x']['start'].append(sim.net.cells[pre].tags['x']/1e3)
                conns['x']['dx'].append(sim.net.cells[post].tags['x']/1e3-sim.net.cells[pre].tags['x']/1e3)
                conns['y']['start'].append(sim.net.cells[pre].tags['y'])
                conns['y']['dy'].append(sim.net.cells[post].tags['y']-sim.net.cells[pre].tags['y'])
                conns['type'].append(sim.net.cells[i].conns[j]['synMech'])
            
    netFig = plt.figure(figsize=[12,12])
    groups = unique(cells['cellType'])
    textY = np.max(cells['y']) + 0.1*np.abs(np.max(cells['y'])-np.min(cells['y']))
    for i in range(len(groups)):
        cid = cells['cellType'].index(groups[i])
        textX = np.mean(cells['x'][cid])
        plt.text(textX,textY,groups[i],horizontalalignment='center')
    
    for j in range(len(conns['type'])):
        x = conns['x']['start'][j]
        y = conns['y']['start'][j]
        dx = conns['x']['dx'][j]
        dy = conns['y']['dy'][j]
        if (conns['type'][j] == 'fEPSP' or conns['type'][j] == 'IPSP'):
            lineType = '-';
        elif (conns['type'][j] == 'esyn'):
            lineType = '-'
        else:
            lineType = '--';
        if (conns['type'][j] == 'fEPSP' or conns['type'][j] == 'EJP'):
            lineColor = 'b';
        elif (conns['type'][j] == 'esyn'):
            lineColor = 'k'
        else:
            lineColor = 'r';
        plt.plot([x,x+dx],[y,y+dy],'k-',zorder=0)

    for i in range(len(cells['pop'])):
        if (cells['pop'][i] == 'EMNdriver' or cells['pop'][i] == 'IMNdriver'):
            markerShape = 's'
            markerFill = 'k'
            markerEdge = 'k'
        elif (cells['pop'][i] == 'EMN'):
            markerShape = 'o'
            markerFill = 'k'
            markerEdge = 'k'
        elif (cells['pop'][i] == 'IMN'):
            markerShape = 'o'
            markerFill = 'w'
            markerEdge = 'k'
        elif (cells['pop'][i] == 'glia'):
            markerShape = 's'
            markerFill = 'w'
            markerEdge = 'k'
        elif (cells['pop'][i] == 'smc'):
            markerShape = 'd'
            markerFill = 'k'
            markerEdge = 'k'
        plt.scatter(cells['x'][i],cells['y'][i],150,marker=markerShape,
                          color=markerFill,edgecolors=markerEdge,zorder=10)
    ymin = np.min(cells['y']) - 0.1*np.abs(np.max(cells['y'])-np.min(cells['y']))
    ymax = np.max(cells['y']) + 0.2*np.abs(np.max(cells['y'])-np.min(cells['y']))
    xmin = np.min(cells['x']) - 0.1*np.abs(np.max(cells['x'])-np.min(cells['x']))
    xmax = np.max(cells['x']) + 0.1*np.abs(np.max(cells['x'])-np.min(cells['x']))
    plt.ylim([ymin,ymax])
    plt.xlim([xmin,xmax])
    plt.yticks([])
    plt.xticks([])
    
    legX = np.min(cells['x'])
    legY = np.min(cells['y'])
    shapes = ['s','o','d','o','o']
    fills = ['k','k','k','w','k']
    labels = ['Input','Neuron','Muscle Fiber','Inhibitory Cell','Excitatory Cell']
    shapes.reverse()
    fills.reverse()
    labels.reverse()
    for i in range(len(shapes)):
        plt.scatter(legX - 0.01*np.abs(np.max(cells['x'])-np.min(cells['x'])),
                    legY+i*0.05*np.abs(np.max(cells['y'])-np.min(cells['y'])),
                    75,marker=shapes[i],edgecolors='k',color=fills[i])
        plt.text(legX+0.01*np.abs(np.max(cells['x'])-np.min(cells['x'])),
                    legY+i*0.05*np.abs(np.max(cells['y'])-np.min(cells['y'])),
                    labels[i],horizontalalignment='left',verticalalignment='center')

    noteX = cells['x'][cells['pop'].index('IMNdriver')]
    cellY = cells['y'][cells['pop'].index('IMNdriver')]
    noteY = np.max(cells['y']) + 0.06*np.abs(np.max(cells['y'])-np.min(cells['y']))
    plt.text(noteX,noteY,' 2Hz firing ON at 2 seconds',fontsize=8,verticalalignment='center')
    plt.plot([noteX,noteX],[cellY,noteY],'k-',linewidth=0.5)
    noteX = cells['x'][cells['pop'].index('EMNdriver')]
    cellY = cells['y'][cells['pop'].index('EMNdriver')]
    noteY = np.max(cells['y']) + 0.03*np.abs(np.max(cells['y'])-np.min(cells['y']))
    plt.text(noteX,noteY,' 2Hz firing ON at 5 seconds',fontsize=8,verticalalignment='center')
    plt.plot([noteX,noteX],[cellY,noteY],'k-',linewidth=0.5)
    noteX = cells['x'][cells['pop'].index('glia')]
    cellY = cells['y'][cells['pop'].index('glia')]
    noteY = np.max(cells['y'])
    plt.text(noteX,noteY,' PGE2 release at 10 seconds',fontsize=8,verticalalignment='center')
    plt.plot([noteX,noteX],[cellY,noteY],'k-',linewidth=0.5)
        
    return netFig