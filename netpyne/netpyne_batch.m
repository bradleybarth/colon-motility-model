addpath('matlab_functions')

try 
    fileID = fopen('compile_output.out','r');
    A = [];
    while ~feof(fileID)
        text = fgetl(fileID);
        text(1) = []; text(end) = [];
        parts = split(text,', ');
        line = [];
        for i = 1:length(parts)
            line = [line str2num(parts{i})];
        end
        A = [A; line];
    end
    fclose(fileID);
    completedSets = A;
catch
    completedSets = zeros(1,6);
end

%Overview Info
numReps = 10

%Continuous Variables
%simDurVal = [1000 1821 3314 6034 10986 20000]
simDurVal = 6034
simDurSet = length(simDurVal);

%inputFreqVal = [0.1 0.2 0.5 1 2 5 10 20 50]
inputFreqVal = [1 2 5 6 7 8.5 10 12 14 17 20 25 32 40 50 100 200 500]
inputFreqSet = length(inputFreqVal);

%inputNoiseVal = [0 0.2 0.4 0.6 0.8 1.0]
inputNoiseVal = [0]
inputNoiseSet = length(inputNoiseVal);

%Discrete Variables
%numNeuronRange = [200 20000]
%numNeuronVal = [200 386 746 1439 2779 5365 10359 20000 38613 74547 143930 277890]
numNeuronVal = [200 386 746 1439 2779 5356 10359 20000]
%numNeuronVal = 2779
numNeuronSet = length(numNeuronVal);

nInputsVal = [1 3 12 42 144 500]
nInputsSet = length(nInputsVal);

%How many sims?
totalSims = numReps*simDurSet*inputFreqSet*inputNoiseSet*numNeuronSet*nInputsSet

if totalSims > 20000
    return
end

%Get Values
% numNeuronVal = GetLog10RangeInt(numNeuronRange,numNeuronSet);
% simDurVal = GetLog10RangeCont(simDurRange,simDurSet);
% inputFreqVal = GetLog10RangeCont(inputFreqRange,inputFreqSet);
% inputNoiseVal = GetLinRangeCont(inputNoiseRange,inputNoiseSet);
% nInputsVal = GetLog10RangeInt(nInputsRange,nInputsSet);

thisScript = 1;
thisSim = 1;
simsOnScript = 0;

jobPre = '#SBATCH --job-name=';
outPre = '#SBATCH --output=/hpc/home/bbb16/netpyne/scripts/';
errPre = '#SBATCH --error=/hpc/home/bbb16/netpyne/scripts/';
cdLine = 'cd /hpc/home/bbb16/netpyne';
preLine = sprintf('./../mod/x86_64/special -nobanner -python cluster_interneuron_network.py');

for i = 1:numNeuronSet
    for j = 1:simDurSet
        complexity = log10(numNeuronVal(i)*simDurVal(j));
        if complexity <= 6
            numSimsPerScript = 100;
        elseif complexity <= 7
            numSimsPerScript = 20;
        elseif complexity <= 8
            numSimsPerScript = 5;
        else
            numSimsPerScript = 1;
        end
        for k = 1:inputFreqSet
            for l = 1:inputNoiseSet
                for m = 1:nInputsSet
                    for n = 1:numReps
                        simulationID = [numNeuronVal(i),...
                            simDurVal(j),...
                            inputFreqVal(k),...
                            inputNoiseVal(l),...
                            nInputsVal(m),...
                            (n-1)];
                        if ismember(simulationID,completedSets,'rows')
                            disp(['This sim already compiled: ',num2str(simulationID)])
                            continue
                        end
                        
                        if thisSim == 1
                            % need to start a new script
                            % first sim on this script 
                            
                            % copy the template file to the new script
                            fidTemplate = fopen('itertemp.slurm','r');
                            fidNew = fopen(sprintf('scripts/netpynerun%04d.slurm',thisScript),'w');
                            
                            while (~feof(fidTemplate))
                                fprintf(fidNew,'%s',fgets(fidTemplate));
                            end
                            fprintf(fidNew,'\n%s%i_%04d',jobPre,floor(complexity),thisScript);
                            fprintf(fidNew,'\n%snetpynerun%04d.out\n',outPre,thisScript);
                            fprintf(fidNew,'\n%snetpynerun%04d.err\n',errPre,thisScript);
                            fprintf(fidNew,'\n%s\n',cdLine);

                            fclose(fidTemplate);
                        elseif thisSim < numSimsPerScript
                            % add this sim to the current script
                            
                            
                        else    % thisSim >= numSimsPerScript
                            % last sim on this script
                            
                            thisSim = 0; % will increment at the end of this loop
                        end
                        simsOnScript = simsOnScript + 1;
                        fprintf(fidNew,'\n%s %i %g %.2g %.2g %i %i %i\n',preLine,...
                            numNeuronVal(i),...
                            simDurVal(j),...
                            inputFreqVal(k),...
                            inputNoiseVal(l),...
                            nInputsVal(m),...
                            (n-1),...
                            0);
                        
                        if thisSim == 0 % if it is the last sim on this script, run
                            fclose(fidNew);
                            [~,temp] = system(sprintf('sbatch scripts/netpynerun%04d.slurm',thisScript));
                            disp(sprintf('%d %s',thisScript,temp))
                            disp([thisScript simsOnScript])
                            thisScript = thisScript + 1; % wrote a new script
                            simsOnScript = 0;
                        end
                        
                        thisSim = thisSim + 1; % increment the current sim after writing to script
                    end
                end
            end
        end
    end
end

fclose(fidNew);
[~,temp] = system(sprintf('sbatch scripts/netpynerun%04d.slurm',thisScript));
disp(sprintf('%d %s',thisScript,temp))
disp([thisScript simsOnScript])
