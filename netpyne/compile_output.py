# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 09:21:19 2020

@author: Bradley Barth
"""

import sys
import json
import os

argvLen = len(sys.argv)

path1 = os.getcwd()
path2 = 'output_data'
#path3 = sys.argv[argvLen-1]
path3 = 'numNeurons_simDur_inputFreq_inputNoise_nInputs_replicate'

newPath = os.path.join(path1,path2,path3)

allJson = [f for f in os.listdir(newPath) if os.path.isfile(os.path.join(newPath,f)) and os.path.splitext(f)[1]=='.json']

outputJson = dict()
outputJson['numNeurons'] = list()
outputJson['simDur'] = list()
outputJson['inputFreq'] = list()
outputJson['inputNoise'] = list()
outputJson['nInputs'] = list()
outputJson['replicate'] = list()
outputJson['syncMeasure'] = list()
outputJson['syncRate'] = list()
outputJson['rateMean'] = list()
outputJson['rateStd'] = list()
outputJson['1-PySpikeISISdist'] = list()
outputJson['PySpikeSync'] = list()
outputJson['connsPerCell'] = list()
outputJson['synsPerCell'] = list()
jsonParams = list(outputJson.keys())

for i in range(len(allJson)):    
    if allJson[i]:
        with open(os.path.join(newPath,allJson[i]),'r') as f:
            DS = json.load(f)
        
        if 'input' in list(DS.keys()):
            inputParams = list(DS['input'].keys())
            outputParams = list(DS.keys())
            outputParams.remove('input')
            for k in range(len(inputParams)):
                if inputParams[k] in list(outputJson.keys()):
                    outputJson[inputParams[k]].append(DS['input'][inputParams[k]])
            for k in range(len(outputParams)):
                if outputParams[k] in list(outputJson.keys()):
                    outputJson[outputParams[k]].append(DS[outputParams[k]])
        
        lengthVal = [len(outputJson[f]) for f in list(outputJson.keys())]
        while len(set(lengthVal)) > 1:
            longList = lengthVal.index(max(lengthVal))
            del(outputJson[jsonParams[longList]][-1])
            lengthVal = [len(outputJson[f]) for f in list(outputJson.keys())]
            
outputFilename = os.path.join(path1,path2,(path3+'.json'))
with open(outputFilename,'w') as f:
    json.dump(outputJson,f)