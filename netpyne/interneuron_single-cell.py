# -*- coding: utf-8 -*-
"""
Created on Mon Oct 14 15:33:56 2019

@author: Bradley Barth
"""

from netpyne import specs, sim
import numpy as np

# Network parameters
netParams = specs.NetParams()
num_neurons = 1

### Population parameters
netParams.popParams['sensory'] = {'cellType': 'IPAN', 'numCells': num_neurons}
netParams.popParams['interneuron'] = {'cellType': 'INT', 'numCells': num_neurons}
netParams.popParams['presyn'] = {'cellType': 'pre', 'numCells': num_neurons}

netParams.importCellParams(label='IPAN_rule', conds= {'cellType': 'IPAN'},
        fileName='.\\cells\\ah_template.py', cellName='ah_cell')
netParams.importCellParams(label='INT_rule', conds= {'cellType': ['INT', 'pre']},
        fileName='.\\cells\\s_template.py', cellName='s_cell')

netParams.stimSourceParams['bkg'] = {'type': 'NetStim', 'rate': 1, 'noise': 0}
netParams.stimTargetParams['bkg->pre'] = {'source': 'bkg', 'conds': {'cellType': 'pre'}, 'weight': 0.01, 'delay': 5, 'synMech': 'exc'}


### Apply strain to all populations
strainLevel = 0.15

#netParams.cellParams['IPAN_rule']['secs']['soma']['mechs']['msc']['strain'] = strainLevel
#netParams.cellParams['INT_rule']['secs']['soma']['mechs']['msc']['strain'] = strainLevel

### Synaptic mechanism parameters
netParams.synMechParams['exc'] = {'mod': 'Exp2Syn', 'tau1': 0.1, 'tau2': 5.0, 'e': 0}  # excitatory synaptic mechanism

# Simulation options
simConfig = specs.SimConfig()                   # object of class SimConfig to store simulation configuration

simConfig.duration = 3*1e3                      # Duration of the simulation, in ms
simConfig.dt = 0.025                            # Internal integration timestep to use
simConfig.verbose = False                       # Show detailed messages
simConfig.recordTraces = {'V_soma':{'sec':'soma','loc':0.5,'var':'v'}}  # Dict with traces to record
simConfig.recordStep = 0.1                      # Step size in ms to save data (e.g. V traces, LFP, etc)
simConfig.filename = 'model_output'             # Set file output name
simConfig.savePickle = False                    # Save params, network and sim output to pickle file

simConfig.analysis['plotTraces'] = {'include': ['all']}

sim.create(netParams, simConfig)
sim.simulate()
sim.analyze()