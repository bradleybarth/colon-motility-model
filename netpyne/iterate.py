# -*- coding: utf-8 -*-
"""
Created on Tue Jan 14 12:47:09 2020

@author: Bradley Barth
"""
from datetime import datetime
from interneuron_network_callable import network
import json

now = datetime.now()
date_time = now.strftime("%Y.%m.%d_%H.%M.%S")

saveFileName = 'saved_data\\iterate_' + date_time + '.json'

inputFreqRange = [2, 5, 10, 20, 50]
inputNoiseRange = [0.2, 0.4, 0.6, 0.8]
    
freqOutputs = list()
for i in range(len(inputFreqRange)):
    for k in range(len(inputNoiseRange)):
        for j in range(6):
            out = network(inputFreq=inputFreqRange[i], inputNoise=inputNoiseRange[k])
            freqOutputs.append(out)
    
output = dict()
output['2DfreqOutputs'] = freqOutputs
    
with open(saveFileName,'w') as f:
    json.dump(output,f)