# -*- coding: utf-8 -*-
"""
Created on Fri May  8 10:50:58 2020

@author: Bradley Barth
"""

from neuron import h

# template for interstitial cell of cajal described by:
# Edwards and Hirst (2006) J Physiol. 2006 Feb 15;571(Pt 1):179-89.
# https://www.ncbi.nlm.nih.gov/pubmed/16357016
# 
#
# adapted by Bradley Barth on May 19th, 2020

class Cell(object):
    def __init__(self):
        self.synlist = []
        self.createSections()
        self.buildTopology()
        self.defineGeometry()
        self.defineBiophysics()
        self.createSynapses()
        self.nclist = []

    def createSections(self):
        pass

    def buildTopology(self):
        pass

    def defineGeometry(self):
        pass

    def defineBiophysics(self):
        pass

    def createSynapses(self):
        """Add an exponentially decaying synapse """
        synsoma = h.ExpSyn(self.soma(0.5))
        synsoma.tau = 5
        synsoma.e = -20
        self.synlist.append(synsoma) # synlist is defined in Cell


    def createNetcon(self, thresh=10):
        """ created netcon to record spikes """
        nc = h.NetCon(self.soma(0.5)._ref_v, None, sec = self.soma)
        nc.threshold = thresh
        return nc
    
class icc(Cell): 
    """interstitial cell of cajal: A soma with active channels"""  #create different classes for different ICC (ICC-MY, -IM, etc.)
    def createSections(self):
        """Create the sections of the cell."""
        self.soma = h.Section(name='soma', cell=self)

    def defineGeometry(self):
        """Set the 3D geometry of the cell."""
        self.soma.L = 76.1
        self.soma.diam = 60
        self.soma.Ra = 30
        self.soma.cm = 20
    
    def defineBiophysics(self):
        """Assign the membrane properties across the cell."""
        # Insert active channels in the soma       
        
        self.soma.insert('icc')
        
    def buildTopology(self):
        pass            