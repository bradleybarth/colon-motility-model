# -*- coding: utf-8 -*-
"""
Created on Mon Oct 14 15:33:56 2019

@author: Bradley Barth
"""

from netpyne import specs, sim
import numpy as np
import matplotlib.pyplot as plt
import json

# Network parameters
netParams = specs.NetParams()

targetFreq = np.linspace(6,120,60) #[12, 38, 62]

with open('iccFreqLUT.json', 'r') as f:
    iccFreqLUT = json.load(f)


tauScaling = list()
for i in range(len(targetFreq)):
    id = np.argmin([np.abs(iccFreqLUT['frequency'][x]-targetFreq[i]) for x in range(len(iccFreqLUT['frequency']))])
    tauScaling.append(iccFreqLUT['tauScale'][id])

num_icc = len(targetFreq)

### Population parameters
netParams.popParams['icc'] = {'cellType': 'ICC', 'numCells': num_icc}

netParams.importCellParams(label='ICC_rule', conds= {'cellType': 'ICC'},
        fileName='.\\cells\\icc_template.py', cellName='icc')

netParams.cellParams['ICC_rule']['secs']['soma']['threshold'] = -25

# Simulation options
simConfig = specs.SimConfig()                   # object of class SimConfig to store simulation configuration

simConfig.duration = 60*1000                    # Duration of the simulation, in ms
simConfig.dt = 0.05                             # Internal integration timestep to use
simConfig.verbose = False                       # Show detailed messages
simConfig.recordTraces = {'Vm':{'sec':'soma','loc':0.5,'var':'v'}} # Dict with traces to record
simConfig.recordStep = 0.05                     # Step size in ms to save data (e.g. V traces, LFP, etc)

#simConfig.analysis['plotTraces'] = {'include': 'all', 'oneFigPer':'trace', 'overlay':'true'}

sim.create(netParams, simConfig)
for i in range(num_icc):
    thisCell = sim.net.pops['icc'].cellGids[i]
    sim.net.cells[thisCell].secs['soma']['mechs']['icc']['tauScale'] = tauScaling[i]
    sim.net.cells[thisCell].secs['soma']['hObj'].tauScale_icc = tauScaling[i]

sim.simulate()
sim.analyze()

counts = [list(sim.simData.spkid).count(i) for i in range(num_icc)]
percError = [(counts[i]-targetFreq[i])/targetFreq[i]*100 for i in range(len(targetFreq))]

plt.figure()
plt.plot(targetFreq,percError,'ko-')
plt.xlabel('Frequency (cycles/min)')
plt.ylabel('Percent Error')

plt.figure()
plt.plot(targetFreq,counts,'ko-')
plt.xlabel('Target Frequency (cycles/min)')
plt.ylabel('Actual Frequency (cycles/min)')