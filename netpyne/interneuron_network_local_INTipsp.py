# -*- coding: utf-8 -*-
"""
Created on Mon Oct 14 15:33:56 2019

@author: Bradley Barth
"""

from netpyne import specs, sim
import numpy as np

# Network parameters
netParams = specs.NetParams()
num_neurons = 1000
### References & Assumptions
    ### 28% of MEN are AH neurons | doi: 10.1371/journal.pone.0039887
    ### 15% of neurons are MEN | doi: 10.1371/journal.pone.0039887
    ### 7% of MEN are SAMEN | doi: 10.1371/journal.pone.0039887
    
    ### All AH cells that are MEN are RAMEN
    ### All MEN are either SAMEN or RAMEN (there are no RAMEN)
        ### Therefore 93% of MEN are RAMEN

num_ramen = np.int(0.15*num_neurons)
num_men = np.int(num_ramen/0.93)
num_samen = num_men - num_ramen

num_ah_ramen = np.int(0.28*num_men)
num_ah_samen = np.int(0)

num_s_ramen = num_ramen - num_ah_ramen
num_s_samen = num_samen

num_sens_men = num_ah_ramen
num_asc_ramen = np.int(0.5*num_s_ramen)
num_asc_samen = np.int(0.5*num_s_samen)
num_desc_ramen = num_s_ramen - num_asc_ramen
num_desc_samen = num_s_samen - num_asc_samen

num_sens_cell = np.int(0.5*num_neurons) - num_sens_men
num_asc_cell = np.int(0.25*num_neurons) - (num_asc_ramen + num_asc_samen)
num_desc_cell = np.int(0.25*num_neurons) - (num_desc_ramen + num_desc_samen)

print('Group\tRAMEN\tSAMEN\tnon-MEN')
print('Sens\t'+str(num_ah_ramen)+'\t'+str(num_ah_samen)+'\t'+str(num_sens_cell))
print('Asc\t'+str(num_asc_ramen)+'\t'+str(num_asc_samen)+'\t'+str(num_asc_cell))
print('Desc\t'+str(num_desc_ramen)+'\t'+str(num_desc_samen)+'\t'+str(num_desc_cell))

netParams.sizeX = 150*1e3
netParams.sizeY = 100
netParams.sizeZ = 0

### Population parameters
# Interneurons
netParams.popParams['asc_cell'] = {'cellType': 'INT_cell', 'numCells': num_asc_cell, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}
netParams.popParams['asc_samen'] = {'cellType': 'INT_samen', 'numCells': num_asc_samen, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}
netParams.popParams['asc_ramen'] = {'cellType': 'INT_ramen', 'numCells': num_asc_ramen, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}
netParams.popParams['desc_cell'] = {'cellType': 'INT_cell', 'numCells': num_desc_cell, 'xnormRange': [0, 1], 'ynormRange': [0.95, 1]}
netParams.popParams['desc_samen'] = {'cellType': 'INT_samen', 'numCells': num_desc_samen, 'xnormRange': [0, 1], 'ynormRange': [0.95, 1]}
netParams.popParams['desc_ramen'] = {'cellType': 'INT_ramen', 'numCells': num_desc_ramen, 'xnormRange': [0, 1], 'ynormRange': [0.95, 1]}
# Sensory neurons
netParams.popParams['sens_cell'] = {'cellType': 'SENS_cell', 'numCells': num_sens_cell, 'xnormRange': [0, 1], 'ynormRange': [0, 0.05]}
netParams.popParams['sens_men'] = {'cellType': 'SENS_men', 'numCells': num_sens_men, 'xnormRange': [0, 1], 'ynormRange': [0, 0.05]}

netParams.importCellParams(label='INT_cell_rule', conds= {'cellType': ['INT_cell']},
        fileName='s_template.py', cellName='s_cell')
netParams.importCellParams(label='INT_samen_rule', conds= {'cellType': ['INT_samen']},
        fileName='s_template.py', cellName='s_samen')
netParams.importCellParams(label='INT_ramen_rule', conds= {'cellType': ['INT_ramen']},
        fileName='s_template.py', cellName='s_ramen')

netParams.importCellParams(label='SENS_cell_rule', conds= {'cellType': ['SENS_cell']},
        fileName='ah_template.py', cellName='ah_cell')
netParams.importCellParams(label='SENS_men_rule', conds= {'cellType': ['SENS_men']},
        fileName='ah_template.py', cellName='ah_ramen')

pops = list(netParams.popParams.keys())
total_cells = 0
for i in range(len(pops)):
    total_cells += netParams.popParams[pops[i]]['numCells']

### Apply tension to all populations
amp = 0.1
rate = 0.015

### Compression driver
#comp_delay = 2e3
#comp_dur = 4e3
#comp_amp = amp
#comp_rate = rate #(elongation per ms)

### Tension driver
tens_delay = 0e3
tens_dur = 30e3
tens_amp = amp
tens_rate = rate #(elongation per ms)
tens = []

def updateStrains(t):
    # Update compression
#    compFlat = comp_amp/comp_rate + comp_delay
#    compOff = comp_delay + comp_dur
#    if t < comp_delay:
#        thisCompression = 0
#    if t >= comp_delay and t < compFlat:
#        thisCompression = (t-comp_delay)*comp_rate
#    if t >= compFlat and t < comp_dur + comp_delay:
#        thisCompression = comp_amp
#    if t >= comp_dur + comp_delay and t < compOff:
#        thisCompression = comp_amp - (t - (comp_dur+comp_delay))*comp_rate
#    if t >= compOff:
#        thisCompression = 0
    
    # Update tension
    tensFlat = tens_amp/tens_rate + tens_delay
    tensOff = tens_delay + tens_dur
    if t < tens_delay:
        thisTension = 0
    if t >= tens_delay and t < tensFlat:
        thisTension = (t-tens_delay)*tens_rate
    if t >= tensFlat and t < tens_dur + tens_delay:
        thisTension = tens_amp
    if t >= tens_dur + tens_delay and t < tensOff:
        thisTension = tens_amp - (t - (tens_dur+tens_delay))*tens_rate
    if t >= tensOff:
        thisTension = 0
        
    # Set values
    for j in range(len(pops)):
        cellsInPop = len(sim.net.pops[pops[j]].cellGids)
        for i in range(cellsInPop):
            thisCell = sim.net.pops[pops[j]].cellGids[i]
#            comp[i].append(((i+1)/(num_neurons))*thisCompression)
#            sim.net.cells[thisCell].secs['soma']['mechs']['cMSC']['compression'] = (i+1)/(num_neurons)*thisCompression
#            sim.net.cells[thisCell].secs['soma']['hObj'].compression_cMSC = (i+1)/(num_neurons)*thisCompression
            tens.append(thisTension)
            sim.net.cells[thisCell].secs['soma']['mechs']['tMSC']['tension'] = thisTension
            sim.net.cells[thisCell].secs['soma']['hObj'].tension_tMSC = thisTension
    return sim


### Synaptic mechanisms
netParams.synMechParams['fEPSP'] = {'mod': 'Exp2Syn', 'tau1': 0.7, 'tau2': 20.0, 'e': 0}
netParams.fEPSPweight = 0.0006 #0.0009125

netParams.synMechParams['sEPSP'] = {'mod': 'Exp2Syn', 'tau1': 25e3, 'tau2': 55e3, 'e': 0}
netParams.sEPSPweight = 0.00008

netParams.synMechParams['IPSP'] = {'mod': 'Exp2Syn', 'tau1': 6e3, 'tau2': 8e3, 'e': -75}
netParams.IPSPweight = 0.002



propVelocity = 100 #(um/ms)
## Ascending synaptic distribution
netParams.p_max_asc = 4000
netParams.sigma_a = 0.3
A_proj_mean = 6000
netParams.mu_a = np.log(A_proj_mean)
## Descending synaptic distribution
netParams.p_max_desc = 4000
netParams.sigma_d = 0.3
D_proj_mean = 15000
netParams.mu_d = np.log(D_proj_mean)
## Local synaptic distribution
netParams.p_max_local = 80
netParams.sigma_l = 0.3
L_proj_mean = 1000
netParams.mu_l = np.log(L_proj_mean)
## IPAN to IPAN synaptic distribution
netParams.p_max_sens = 400
netParams.sigma_s = 0.3
S_proj_mean = 1000
netParams.mu_s = np.log(S_proj_mean)


### Cell connectivity rules
netParams.connParams['asc->asc'] = {            # ascending to ascending
        'preConds': {'pop': ['asc_cell','asc_samen','asc_ramen']},             # conditions of presyn cells
        'postConds': {'pop': ['asc_cell','asc_samen','asc_ramen']},            # conditions of postsyn cells
        'probability': 'p_max_asc / ((pre_x - post_x) * sigma_a * sqrt(2*np.pi)) * exp(-((np.log(pre_x - post_x)-mu_a)**2)/(2*sigma_a**2))',             # probability of connection
        'weight': 'fEPSPweight',                # synaptic weight
        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
        'synMech': 'fEPSP'}                     # synaptic mechanism
netParams.connParams['desc->desc'] = {          # descending to descending
        'preConds': {'pop': ['desc_cell','desc_samen','desc_ramen']},            # conditions of presyn cells
        'postConds': {'pop': ['desc_cell','desc_samen','desc_ramen']},           # conditions of postsyn cells
        'probability': 'p_max_desc / ((post_x - pre_x) * sigma_d * sqrt(2*np.pi)) * exp(-((np.log(post_x - pre_x)-mu_d)**2)/(2*sigma_d**2))',             # probability of connection
        'weight': 'fEPSPweight',                # synaptic weight
        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
        'synMech': 'fEPSP'}                     # synaptic mechanism
netParams.connParams['asc->desc'] = {           # local ascending to descending and vice versa
        'preConds': {'pop': ['asc_cell','asc_samen','asc_ramen','desc_cell','desc_samen','desc_ramen']},             # conditions of presyn cells
        'postConds': {'pop': ['asc_cell','asc_samen','asc_ramen','desc_cell','desc_samen','desc_ramen']},            # conditions of postsyn cells
        'probability': '2*p_max_local / (dist_x * sigma_l * sqrt(2*np.pi)) * exp(-((np.log(dist_x)-mu_l)**2)/(2*sigma_l**2))',             # probability of connection
        'weight': 'fEPSPweight',                # synaptic weight
        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
        'synMech': 'fEPSP'}                     # synaptic mechanism
netParams.connParams['asc->desc'] = {           # local ascending to descending and vice versa
        'preConds': {'pop': ['asc_cell','asc_samen','asc_ramen','desc_cell','desc_samen','desc_ramen']},             # conditions of presyn cells
        'postConds': {'pop': ['asc_cell','asc_samen','asc_ramen','desc_cell','desc_samen','desc_ramen']},            # conditions of postsyn cells
        'probability': 'p_max_local / (dist_x * sigma_l * sqrt(2*np.pi)) * exp(-((np.log(dist_x)-mu_l)**2)/(2*sigma_l**2))',             # probability of connection
        'weight': 'IPSPweight',                 # synaptic weight
        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
        'synMech': 'IPSP'}                      # synaptic mechanism
netParams.connParams['sens->sens'] = {          # sensory to sensory (ipan to ipan)
        'preConds': {'pop': ['sens_cell','sens_men']},             # conditions of presyn cells
        'postConds': {'pop': ['sens_cell','sens_men']},            # conditions of postsyn cells
        'probability': 'p_max_sens / (dist_x * sigma_s * sqrt(2*np.pi)) * exp(-((np.log(dist_x)-mu_s)**2)/(2*sigma_s**2))',             # probability of connection
        'weight': 'sEPSPweight',                # synaptic weight
        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
        'synMech': 'sEPSP'}                     # synaptic mechanism
netParams.connParams['sens->int'] = {           # sensory to interneuron
        'preConds': {'pop': ['sens_cell','sens_men']},             # conditions of presyn cells
        'postConds': {'pop': ['asc_cell','asc_samen','asc_ramen','desc_cell','desc_samen','desc_ramen']},            # conditions of postsyn cells
        'probability': 'p_max_local / (dist_x * sigma_l * sqrt(2*np.pi)) * exp(-((np.log(dist_x)-mu_l)**2)/(2*sigma_l**2))',             # probability of connection
        'weight': 'fEPSPweight',                # synaptic weight
        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
        'synMech': 'fEPSP'}                     # synaptic mechanism

### Simulation options
simConfig = specs.SimConfig()                   # object of class SimConfig to store simulation configuration

simConfig.hParams['v_init'] = -52
simConfig.duration = 10*1e3                      # Duration of the simulation, in ms
simConfig.dt = 0.05                             # Internal integration timestep to use
simConfig.verbose = False                       # Show detailed messages
simConfig.recordStep = 0.1                      # Step size in ms to save data (e.g. V traces, LFP, etc)

simConfig.saveDataInclude = ['netParams', 'netCells', 'netPops', 'simConfig', 'simData']
simConfig.filename = 'model_output_nn_' + str(num_neurons) + '_dur10_' + str(np.round(np.log10(simConfig.duration),2)) # Set file output name
simConfig.saveJson = True                        # Save params, network and sim output to json file
sim.updateInterval = 2*simConfig.dt
simConfig.recordLFP = [[0, 10e3, 0]]

simConfig.analysis['plotRaster'] = {'spikeHist': 'overlay', 'orderBy': 'x'} # Plot a raster
simConfig.analysis['plotLFP'] = {'electrodes': [0], 'plots': ['timeSeries'], 'figSize': (12,9)}

sim.create(netParams, simConfig)

for i in range(len(sim.net.cells)):
    modifier = np.random.normal(loc=0.0,scale=0.02)
    cOn = sim.net.cells[i].secs['soma']['mechs']['cMSC']['onHalf']
    sim.net.cells[i].secs['soma']['mechs']['cMSC']['onHalf'] = (1.0+modifier)*cOn
    sim.net.cells[i].secs['soma']['hObj'].onHalf_cMSC = (1.0+modifier)*cOn
    cOff = sim.net.cells[i].secs['soma']['mechs']['cMSC']['offHalf']
    sim.net.cells[i].secs['soma']['mechs']['cMSC']['offHalf'] = (0.01*modifier)+cOff
    sim.net.cells[i].secs['soma']['hObj'].offHalf_cMSC = (0.01*modifier)+cOff
    tOn = sim.net.cells[i].secs['soma']['mechs']['tMSC']['onHalf']
    sim.net.cells[i].secs['soma']['mechs']['tMSC']['onHalf'] = (1.0+modifier)*tOn
    sim.net.cells[i].secs['soma']['hObj'].onHalf_tMSC = (1.0+modifier)*tOn
    tOff = sim.net.cells[i].secs['soma']['mechs']['tMSC']['offHalf']
    sim.net.cells[i].secs['soma']['mechs']['tMSC']['offHalf'] = (1.0+modifier)*tOff
    sim.net.cells[i].secs['soma']['hObj'].offHalf_tMSC = (1.0+modifier)*tOff

sim.runSimWithIntervalFunc(sim.updateInterval, updateStrains)
#sim.simulate()
sim.analyze()