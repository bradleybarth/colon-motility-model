# -*- coding: utf-8 -*-
"""
Created on Fri Jan 10 08:27:35 2020

@author: Bradley Barth
"""

from netpyne import specs, sim
import numpy as np

# Network parameters
netParams = specs.NetParams()
num_neurons = 100

netParams.sizeX = 150*1e3
netParams.sizeY = 100
netParams.sizeZ = 0

del1 = 0
dur1 = 3e3
spkTimes = [5]

### Regional complexity in wiring: 10.7554/eLife.42914
    ### Myenteric neuron density
        ### Proximal: ~700 per mm2
        ### Distal: ~600 per mm2
        ### #distal / #proximal = 0.85
    ### Interganglionic fibers / strand
        ### Proximal: ~36
        ### Distal: ~18
        ### #distal / #proximal = 0.5

allPops = ['P1', 'P2']
XList = dict.fromkeys(allPops,[])
CellList = dict.fromkeys(allPops,[])
for p in range(len(allPops)):
    xind = list()
    cind = list()
    for i in range(num_neurons):
        x = np.random.gumbel(0.25*netParams.sizeX,0.50*netParams.sizeX)
        while x > netParams.sizeX or x < 0:
            x = np.random.gumbel(0.25*netParams.sizeX,0.50*netParams.sizeX)
        y = np.random.uniform(0,netParams.sizeY/2)
        xind.append(x)
        cind.append({'cellLabel': allPops[p]+str(i), 'x': x, 'y': y})
    XList[allPops[p]] = xind
    CellList[allPops[p]] = cind
    
netParams.popParams['Input'] = {'cellModel': 'VecStim', 'spkTimes': spkTimes, 'numCells':1, 'xRange': [55e3, 55e3], 'yRange': [110, 110]}    

netParams.popParams['P1'] = {'cellModel': 'HH', 'cellsList': CellList['P1']}
netParams.popParams['P2'] = {'cellModel': 'HH', 'cellsList': CellList['P2']}

soma = {'geom': {}, 'mechs': {}}  # soma properties
soma['geom'] = {'diam': 18.8, 'L': 18.8, 'Ra': 123.0, 'pt3d': []}
soma['geom']['pt3d'].append((0, 0, 0, 20))
soma['geom']['pt3d'].append((0, 0, 20, 20))
soma['mechs']['hh'] = {'gnabar': 0.12, 'gkbar': 0.036, 'gl': 0.003, 'el': -70}
cellRule = {'conds': {'cellModel': 'HH'},  'secs': {}}
cellRule['secs'] = {'soma': soma}
netParams.cellParams['HH'] = cellRule  # add rule dict to list of cell property rules

soma = {'geom': {}, 'mechs': {}}  # soma properties
soma['geom'] = {'diam': 18.8, 'L': 18.8, 'Ra': 123.0, 'pt3d': []}
soma['geom']['pt3d'].append((0, 0, 0, 20))
soma['geom']['pt3d'].append((0, 0, 20, 20))
soma['mechs']['hh'] = {'gnabar': 0.12, 'gkbar': 0.036, 'gl': 0.003, 'el': -70}

def synapticConns(CellList, XList, pre, post, percentLoss = 0.2,
                  proj = lambda mu, std: np.random.normal(mu,std),
                  synsPerCellFun = lambda xNorm: 4 - 3*xNorm):
    connParams = dict({'List': [], 'synsPerConn': []})
    for i in range(len(CellList[pre])):
        nPS = np.int(np.random.normal(synsPerCellFun(XList[pre][i]/netParams.sizeX),0.75))+1
        nPS = np.max([1, nPS])
        connParams['synsPerConn'].append(nPS)
        for j in range(nPS):
            postSynLoc = proj(15e3,3e3)
            postSynCell = min(range(len(XList[post])), key=lambda k: abs((XList[post][k]-XList[pre][i])-postSynLoc))
            chance = np.random.uniform()
            if abs(XList[pre][i]-XList[post][postSynCell]) < 10e3 and chance > percentLoss:
                continue
            connParams['List'].append([i,postSynCell])
    return connParams

P1P1conn = synapticConns(CellList,XList,'P1','P1',proj = lambda x,y: np.random.normal(3e3,1e3))
P2P2conn = synapticConns(CellList,XList,'P2','P2',proj = lambda x,y: np.random.normal(-3e3,1e3))
P1P2conn = synapticConns(CellList,XList,'P1','P2',proj = lambda x,y: np.random.normal(15e3,5e3))
P2P1conn = synapticConns(CellList,XList,'P2','P1',proj = lambda x,y: np.random.normal(-6e3,2e3))

P1P1connList = P1P1conn['List']
P2P2connList = P2P2conn['List']
P1P2connList = P1P2conn['List']
P2P1connList = P2P1conn['List']

#connList = dict.fromkeys(allPops,[])
#numPostSyn = dict.fromkeys(allPops,[])
#for p in range(len(allPops)):
#    for i in range(len(CellList[allPops[p]])):
#        nPS = np.int(np.random.normal((4-3*XList[allPops[p]][i]/netParams.sizeX),0.75))+1
#        nPS = np.max([1, nPS])
#        numPostSyn.append(nPS)
#        for j in range(nPS):
#            postSynLoc = np.random.normal(15e3,3e3)
#            postSynCell = min(range(len(P2XList)), key=lambda k: abs((P2XList[k]-P1XList[i])-postSynLoc))
#            chance = np.random.uniform()
#            if abs(P1XList[i]-P2XList[postSynCell]) < 10e3 and chance > 0.2:
#                continue
#            P1P2connList.append([i,postSynCell])
#        
#P2P1connList = []
#numPostSyn = []
#for i in range(len(P2CellList)):
#    nPS = np.int(np.random.normal((4-3*P2XList[i]/netParams.sizeX),0.75))+1
#    nPS = np.max([1, nPS])
#    numPostSyn.append(nPS)
#    for j in range(nPS):
#        postSynLoc = np.random.normal(-6e3,1e3)
#        postSynCell = min(range(len(P1XList)), key=lambda k: abs((P1XList[k]-P2XList[i])-postSynLoc))
#        chance = np.random.uniform()
#        if abs(P2XList[i]-P1XList[postSynCell]) < 10e3 and chance > 0.2:
#            continue
#        P2P1connList.append([i,postSynCell])

### Synaptic mechanisms
netParams.synMechParams['exc'] = {'mod': 'Exp2Syn', 'tau1': 0.8, 'tau2': 0.2, 'e': 40}  # NMDA synaptic mechanism

netParams.connParams['Input->P1'] = {            
        'preConds': {'pop': 'Input'},             
        'postConds': {'pop': 'P2', 'x': [50e3, 60e3]},  
        'probability': 1,
        'weight': 1,                  
        'delay': 2,                             
        'synMech': 'exc'}
netParams.connParams['P1->P1'] = {            
        'preConds': {'pop': 'P1'},             
        'postConds': {'pop': 'P1'},  
        'connList': P1P1connList,
        'weight': 1,                  
        'delay': 2,                             
        'synMech': 'exc'}     
netParams.connParams['P2->P2'] = {            
        'preConds': {'pop': 'P2'},             
        'postConds': {'pop': 'P2'},  
        'connList': P2P2connList,
        'weight': 1,                  
        'delay': 2,                             
        'synMech': 'exc'}     
netParams.connParams['P1->P2'] = {            
        'preConds': {'pop': 'P1'},             
        'postConds': {'pop': 'P2'},  
        'connList': P1P2connList,
        'weight': 1,                  
        'delay': 2,                             
        'synMech': 'exc'}            
netParams.connParams['P2->P1'] = {            
        'preConds': {'pop': 'P2'},             
        'postConds': {'pop': 'P1'},  
        'connList': P2P1connList,
        'weight': 1,                  
        'delay': 2,                            
        'synMech': 'exc'}           

simConfig = specs.SimConfig()                   # object of class SimConfig to store simulation configuration

simConfig.hParams['v_init'] = -52
simConfig.duration = 100                        # Duration of the simulation, in ms
simConfig.dt = 0.05                             # Internal integration timestep to use
simConfig.verbose = False                       # Show detailed messages
simConfig.recordStep = 0.1                      # Step size in ms to save data (e.g. V traces, LFP, etc)
#simConfig.recordTraces = {'V_soma':{'sec':'soma','loc':0.5,'var':'v'}}  # Dict with traces to record
sim.updateInterval = 2*simConfig.dt
simConfig.recordLFP = [[0, 10e3, 0]]

simConfig.analysis['plotRaster'] = {'spikeHist': None, 'orderBy': 'x', 'orderInverse': 'True'} # Plot a raster
#simConfig.analysis['plotTraces'] = {'include': ['P1'], 'overlay': True, 'oneFigPer': 'trace'}
simConfig.analysis['plot2Dnet'] = True

sim.create(netParams, simConfig)

#for i in range(len(sim.net.cells)):
#    modifier = np.random.normal(loc=0.0,scale=0.02)
#    cOn = sim.net.cells[i].secs['soma']['mechs']['cMSC']['onHalf']
#    sim.net.cells[i].secs['soma']['mechs']['cMSC']['onHalf'] = (1.0+modifier)*cOn
#    sim.net.cells[i].secs['soma']['hObj'].onHalf_cMSC = (1.0+modifier)*cOn
#    cOff = sim.net.cells[i].secs['soma']['mechs']['cMSC']['offHalf']
#    sim.net.cells[i].secs['soma']['mechs']['cMSC']['offHalf'] = (0.01*modifier)+cOff
#    sim.net.cells[i].secs['soma']['hObj'].offHalf_cMSC = (0.01*modifier)+cOff
#    tOn = sim.net.cells[i].secs['soma']['mechs']['tMSC']['onHalf']
#    sim.net.cells[i].secs['soma']['mechs']['tMSC']['onHalf'] = (1.0+modifier)*tOn
#    sim.net.cells[i].secs['soma']['hObj'].onHalf_tMSC = (1.0+modifier)*tOn
#    tOff = sim.net.cells[i].secs['soma']['mechs']['tMSC']['offHalf']
#    sim.net.cells[i].secs['soma']['mechs']['tMSC']['offHalf'] = (1.0+modifier)*tOff
#    sim.net.cells[i].secs['soma']['hObj'].offHalf_tMSC = (1.0+modifier)*tOff

#sim.runSimWithIntervalFunc(sim.updateInterval, updateStrains)
sim.simulate()
sim.analyze()