# -*- coding: utf-8 -*-
"""
Created on Wed Dec  4 15:21:07 2019

@author: Bradley Barth
"""

from netpyne import specs, sim
import numpy as np

### Network parameters
netParams = specs.NetParams()
num_smfibers = 20
num_motor = 4

### Population parameters
netParams.popParams['smc'] = {'cellType': 'SMC', 'numCells': num_smfibers}

netParams.importCellParams(label='SMC_rule', conds={'cellType': 'SMC'},
        fileName='smc_template.hoc', cellName='sm_cell')

#gbarCaL = netParams.cellParams['SMC_rule']['secs']['soma']['mechs']['cal']['gcal']
#netParams.cellParams['SMC_rule']['secs']['soma']['mechs']['cal']['gcal'] = 0.01*gbarCaL


### Stimulation parameters
spkTimes = [1000]

del1 = 0.75e3
dur1 = 10e3
spkTimes = list(np.linspace(del1,del1+dur1,int(dur1*0.002+1)))
netParams.popParams['inmotorneuron'] = {'cellModel': 'VecStim', 'numCells': num_motor, 'spkTimes': spkTimes}

del1 = 1.25e3
dur1 = 10e3
spkTimes = list(np.linspace(del1,del1+dur1,int(dur1*0.002+1)))
netParams.popParams['exmotorneuron'] = {'cellModel': 'VecStim', 'numCells': num_motor, 'spkTimes': spkTimes}

del1 = 0.5e3
dur1 = 10e3
spkTimes = list(np.linspace(del1,del1+dur1,int(dur1*0.002+1)))
netParams.popParams['NOmotorneuron'] = {'cellModel': 'VecStim', 'numCells': num_motor, 'spkTimes': spkTimes}


### Synaptic mechanisms [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4478139/]
netParams.synMechParams['EJP'] = {'mod': 'Exp2Syn', 'tau1': 45, 'tau2': 90, 'e': 0}
netParams.EJPweight = 0.0012 #0.0048 #originally tuned to 0.0012 for single EJP in single, non-gap-junctioned smc;
# but with 5 fibers and 0.0048 S/cm2 gap junctions, need twice the conductance

netParams.synMechParams['fIJP'] = {'mod': 'Exp2Syn', 'tau1': 200, 'tau2': 100, 'e': -70}
netParams.synMechParams['sIJP'] = {'mod': 'Exp2Syn', 'tau1': 900, 'tau2': 850, 'e': -70}
netParams.IJPweight = 0.004 #0.002

netParams.synMechParams['esyn'] = {'mod': 'ElectSyn', 'g': 0.025} #306nS -> 0.025 S/cm2 approx. from Edwards and Hirst, 2005
gapList = []
for i in range(num_smfibers-1): gapList.append([i,i+1])
innervation = int(num_smfibers/2)-2
innervList = []
for i in range(num_motor): innervList.append([i,innervation+np.mod(i,4)])

### Cell connectivity rules
netParams.connParams['motor_gap'] = {           
        'preConds': {'pop': 'smc'},             # conditions of presyn cells
        'postConds': {'pop': 'smc'},            # conditions of postsyn cells
        'connList': gapList,                    # explicity connection
        'weight': 1,                            # synaptic weight
        'delay': 0,                             # transmission delay (ms) 
        'synMech': 'esyn',                      # electrical synapse
        'gapJunction': True,                    # gap junction
        'loc': 0.1,                             # post synaptic at "left" of cell
        'preLoc': 0.9}                          # pre synaptic at "right" of cell NOTE: bidirectional anyway
netParams.connParams['ex->SMC'] = {         
        'preConds': {'pop': 'exmotorneuron'},     # conditions of presyn cells
        'postConds': {'pop': 'smc'},            # conditions of postsyn cells
        'connList': innervList,#int(num_smfibers/2)]],                    # explicit connection
        'weight': 'EJPweight',                  # synaptic weight
        'delay': 0,                             # transmission delay (ms)
        'synMech': 'EJP'}                       # synaptic mechanism
netParams.connParams['in->SMC'] = {         
        'preConds': {'pop': 'inmotorneuron'},     # conditions of presyn cells
        'postConds': {'pop': 'smc'},            # conditions of postsyn cells
        'connList': innervList,#int(num_smfibers/2)]],                    # explicit connection
        'weight': 'IJPweight',                  # synaptic weight
        'delay': 0,                             # transmission delay (ms)
        'synMech': 'fIJP'}                       # synaptic mechanism
netParams.connParams['NO->SMC'] = {          
        'preConds': {'pop': 'NOmotorneuron'},     # conditions of presyn cells
        'postConds': {'pop': 'smc'},            # conditions of postsyn cells
        'probability': 1,
        'weight': 'IJPweight',                  # synaptic weight
        'delay': 0,                             # transmission delay (ms)
        'synMech': 'sIJP'}                       # synaptic mechanism

### Simulation options
simConfig = specs.SimConfig()                   # object of class SimConfig to store simulation configuration

simConfig.hParams['v_init'] = -52
simConfig.duration = 2e3                        # Duration of the simulation, in ms
simConfig.dt = 0.025                            # Internal integration timestep to use
simConfig.verbose = False                       # Show detailed messages
simConfig.recordTraces = {'V_soma':{'sec':'soma','loc':0.5,'var':'v'}}  # Dict with traces to record
simConfig.recordStep = 0.1                      # Step size in ms to save data (e.g. V traces, LFP, etc)
simConfig.filename = 'model_output'             # Set file output name
simConfig.savePickle = False                    # Save params, network and sim output to pickle file

simConfig.analysis['plotTraces'] = {'include': ['smc'],'overlay': 'True', 'oneFigPer':'trace'}

sim.create(netParams, simConfig)
sim.simulate()
sim.analyze()