# -*- coding: utf-8 -*-
"""
Created on Mon Oct 14 15:33:56 2019

@author: Bradley Barth
"""

from netpyne import specs, sim
import numpy as np

# Network parameters
netParams = specs.NetParams()
num_neurons = 1

### Population parameters
netParams.popParams['interneuron'] = {'cellType': 'INT', 'numCells': num_neurons}
netParams.popParams['motor'] = {'cellType': 'MOT', 'numCells': num_neurons}

netParams.importCellParams(label='S_rule', conds= {'cellType': ['INT','MOT']},
        fileName='.\\cells\\s_template.py', cellName='s_cell')


### Stimulation parameters
spkTimes = [100]
netParams.popParams['pre'] = {'cellModel': 'VecStim', 'numCells': 1, 'spkTimes': spkTimes}
spkTimes = [102]
netParams.popParams['preIP'] = {'cellModel': 'VecStim', 'numCells': 1, 'spkTimes': spkTimes}


### Synaptic mechanisms
netParams.synMechParams['fEPSP'] = {'mod': 'Exp2Syn', 'tau1': 0.5, 'tau2': 8.0, 'e': 0}
netParams.fEPSPweight = 0.0005

### Inhibition
netParams.synMechParams['preIP'] = {'mod': 'Exp2Syn', 'tau1': 0.5, 'tau2': 8.0, 'e': -75}
netParams.preIPweight = 0.08

### Cell connectivity rules
netParams.connParams['pre->INT'] = {           # spikes -> IPAN label
        'preConds': {'pop': 'pre'},             # conditions of presyn cells
        'postConds': {'pop': 'interneuron'},        # conditions of postsyn cells
        'sec': 'dend',
        'probability': 1,                       # probability of connection
        'weight': 'fEPSPweight*10',                # synaptic weight
        'delay': 0,                             # transmission delay (ms)
        'synMech': 'fEPSP'}                     # synaptic mechanism

netParams.connParams['preI->INT'] = {            # spikes -> INT label
        'preConds': {'pop': 'preIP'},             # conditions of presyn cells
        'postConds': {'pop': 'interneuron'},    # conditions of postsyn cells
        'sec': 'soma',
        'probability': 1,                       # probability of connection
        'weight': 'preIPweight',                # synaptic weight
        'delay': 0,                             # transmission delay (ms)
        'synMech': 'preIP'}                     # synaptic mechanism


netParams.connParams['INT->MOT'] = {            # spikes -> INT label
        'preConds': {'pop': 'interneuron'},             # conditions of presyn cells
        'postConds': {'pop': 'motor'},    # conditions of postsyn cells
        'sec': 'dend',
        'probability': 1,                       # probability of connection
        'weight': 'fEPSPweight',                # synaptic weight
        'delay': 0,                             # transmission delay (ms)
        'synMech': 'fEPSP'}                     # synaptic mechanism

### Simulation options
simConfig = specs.SimConfig()                   # object of class SimConfig to store simulation configuration

simConfig.hParams['v_init'] = -52
simConfig.duration = 200                        # Duration of the simulation, in ms
simConfig.dt = 0.025                            # Internal integration timestep to use
simConfig.verbose = False                       # Show detailed messages
simConfig.recordTraces = {'V_dend':{'sec':'dend','loc':0.5,'var':'v'},
                          'V_soma':{'sec':'soma','loc':0.5,'var':'v'}}  # Dict with traces to record
simConfig.recordStep = 0.1                      # Step size in ms to save data (e.g. V traces, LFP, etc)
simConfig.filename = 'model_output'             # Set file output name
simConfig.savePickle = False                    # Save params, network and sim output to pickle file

simConfig.analysis['plotTraces'] = {'include': ['interneuron','motor']}

sim.create(netParams, simConfig)
sim.simulate()
sim.analyze()