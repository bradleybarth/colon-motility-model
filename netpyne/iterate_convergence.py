# -*- coding: utf-8 -*-
"""
Created on Tue Jan 14 12:47:09 2020

@author: Bradley Barth
"""
from datetime import datetime
from interneuron_network_callable import network
import json

now = datetime.now()
date_time = now.strftime("%Y.%m.%d_%H.%M.%S")

saveFileName = 'saved_data\\iterate_' + date_time + '.json'

inputFreqRange = [2, 5, 10, 20, 50, 100, 200, 500]
inputNoiseRange = [0, 0.2, 0.4, 0.6, 0.8, 1.0]
nInputsRange = [1, 2, 5, 10, 20, 50, 100, 200, 500]
    
#inputFreqOutputs = list()
#for i in range(len(inputFreqRange)):
#    for j in range(10):
#        out = network(inputFreq = inputFreqRange[i])
#        inputFreqOutputs.append(out)
#        
#inputNoiseOutputs = list()
#for i in range(len(inputNoiseRange)):
#    for j in range(10):
#        out = network(inputNoise = inputNoiseRange[i])
#        inputNoiseOutputs.append(out)
    
nInputsOutputs = list()
for i in range(len(nInputsRange)):
    for j in range(10):
        out = network(nInputs = nInputsRange[i])
        nInputsOutputs.append(out)

output = dict()
#output['inputFreq'] = inputFreqOutputs
#output['inputNoise'] = inputNoiseOutputs
output['nInputs'] = nInputsOutputs
    
with open(saveFileName,'w') as f:
    json.dump(output,f)