# -*- coding: utf-8 -*-
"""
Created on Mon Oct 14 15:33:56 2019

@author: Bradley Barth
"""

from netpyne import specs, sim
import numpy as np

# Network parameters
netParams = specs.NetParams()
num_neurons = 3

### Population parameters
netParams.popParams['sensory1'] = {'cellType': 'IPAN', 'numCells': 1}
netParams.popParams['sensory2'] = {'cellType': 'IPAN', 'numCells': 1}
netParams.popParams['sensory3'] = {'cellType': 'IPAN', 'numCells': 1}
netParams.popParams['interneuron1'] = {'cellType': 'INT', 'numCells': 1}
netParams.popParams['interneuron2'] = {'cellType': 'INT', 'numCells': 1}
netParams.popParams['interneuron3'] = {'cellType': 'INT', 'numCells': 1}

netParams.importCellParams(label='IPAN_rule', conds= {'cellType': 'IPAN'},
        fileName='.\\cells\\ah_template.py', cellName='ah_cell')
netParams.importCellParams(label='INT_rule', conds= {'cellType': 'INT'},
        fileName='.\\cells\\s_template.py', cellName='s_cell')

### Stimulation parameters
spkTimes = [5000,5100]
#            305000,305100,
#            365000,365100]
netParams.popParams['pre1'] = {'cellModel': 'VecStim', 'numCells': 1, 'spkTimes': spkTimes}
spkTimes = [5000,5100,5200,5300,5400]
#            305000,305100,305200,305300,305400,
#            365000,365100,365200,365300,365400]
netParams.popParams['pre2'] = {'cellModel': 'VecStim', 'numCells': 1, 'spkTimes': spkTimes}
spkTimes = [5000,5100,5200,5300,5400,5500,5600,5700,5800,5900]
#            305000,305100,305200,305300,305400,305500,305600,305700,305800,305900,
#            365000,365100,365200,365300,365400,365500,365600,365700,365800,365900]
netParams.popParams['pre3'] = {'cellModel': 'VecStim', 'numCells': 1, 'spkTimes': spkTimes}


### Synaptic mechanisms
netParams.synMechParams['sEPSP'] = {'mod': 'Exp2Syn', 'tau1':20e3, 'tau2': 6e3, 'e': 0}
netParams.sEPSPweight = 0.00008

## Cell connectivity rules
netParams.connParams['pre1->IPAN1'] = {         # spikes -> IPAN1 label
        'preConds': {'pop': 'pre1'},            # conditions of presyn cells
        'postConds': {'pop': 'sensory1'},       # conditions of postsyn cells
        'probability': 1,          
        'weight': 'sEPSPweight',                # synaptic weight
        'delay': 0,                             # transmission delay (ms)
        'synMech': 'sEPSP'}                     # synaptic mechanism
netParams.connParams['pre2->IPAN2'] = {         # spikes -> IPAN2 label
        'preConds': {'pop': 'pre2'},            # conditions of presyn cells
        'postConds': {'pop': 'sensory2'},       # conditions of postsyn cells
        'probability': 1,          
        'weight': 'sEPSPweight',                # synaptic weight
        'delay': 0,                             # transmission delay (ms)
        'synMech': 'sEPSP'}                     # synaptic mechanism
netParams.connParams['pre3->IPAN3'] = {         # spikes -> IPAN3 label
        'preConds': {'pop': 'pre3'},            # conditions of presyn cells
        'postConds': {'pop': 'sensory3'},       # conditions of postsyn cells
        'probability': 1,          
        'weight': 'sEPSPweight',                # synaptic weight
        'delay': 0,                             # transmission delay (ms)
        'synMech': 'sEPSP'}                     # synaptic mechanism

netParams.connParams['pre1->INT1'] = {          # spikes -> INT1 label
        'preConds': {'pop': 'pre1'},            # conditions of presyn cells
        'postConds': {'pop': 'interneuron1'},   # conditions of postsyn cells
        'probability': 1,          
        'weight': 'sEPSPweight',                # synaptic weight
        'delay': 0,                             # transmission delay (ms)
        'synMech': 'sEPSP'}                     # synaptic mechanism
netParams.connParams['pre2->INT2'] = {          # spikes -> INT2 label
        'preConds': {'pop': 'pre2'},            # conditions of presyn cells
        'postConds': {'pop': 'interneuron2'},   # conditions of postsyn cells
        'probability': 1,          
        'weight': 'sEPSPweight',                # synaptic weight
        'delay': 0,                             # transmission delay (ms)
        'synMech': 'sEPSP'}                     # synaptic mechanism
netParams.connParams['pre3->INT3'] = {          # spikes -> INT3 label
        'preConds': {'pop': 'pre3'},            # conditions of presyn cells
        'postConds': {'pop': 'interneuron3'},   # conditions of postsyn cells
        'probability': 1,          
        'weight': 'sEPSPweight',                # synaptic weight
        'delay': 0,                             # transmission delay (ms)
        'synMech': 'sEPSP'}                     # synaptic mechanism


# Simulation options
simConfig = specs.SimConfig()                   # object of class SimConfig to store simulation configuration

simConfig.hParams['v_init'] = -47
simConfig.duration = 30*1e3                    # Duration of the simulation, in ms
simConfig.dt = 0.025                            # Internal integration timestep to use
simConfig.verbose = False                       # Show detailed messages
simConfig.recordTraces = {'V_soma':{'sec':'soma','loc':0.5,'var':'v'}}  # Dict with traces to record
simConfig.recordStep = 0.1                      # Step size in ms to save data (e.g. V traces, LFP, etc)
simConfig.filename = 'model_output'             # Set file output name
simConfig.savePickle = False                    # Save params, network and sim output to pickle file

simConfig.analysis['plotTraces'] = {'include': ['sensory1','sensory2','sensory3',
                  'interneuron1','interneuron2','interneuron3']}

sim.create(netParams, simConfig)
sim.simulate()
sim.analyze()