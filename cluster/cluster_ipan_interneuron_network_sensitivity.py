# -*- coding: utf-8 -*-
"""
Created on Mon Oct 14 15:33:56 2019

@author: Bradley Barth
"""
from netpyne import specs, sim
import numpy as np
import json
import pyspike as spk
import sys
import os
import matplotlib.pyplot as plt
from scipy import signal

argvLen = len(sys.argv)

path1 = os.getcwd()
path2 = 'scripts'
argFilename = sys.argv[argvLen-2]
rep = int(sys.argv[argvLen-1])

with open(os.path.join(path1,path2,argFilename),'r') as f:
	argJson = json.load(f)

numNeurons = int(argJson['numNeurons'][rep])
nInputs = int(argJson['nInputs'][rep])
inputFreq = argJson['inputFreq'][rep]
inputNoise = argJson['inputNoise'][rep]
ascProjDist = argJson['ascProjDist'][rep]
descProjDist = argJson['descProjDist'][rep]
locProjDist = argJson['locProjDist'][rep]
farConnMax = argJson['farConnMax'][rep]
farConnMin = argJson['farConnMin'][rep]
locConnMax = argJson['locConnMax'][rep]
locConnMin = argJson['locConnMin'][rep]
fEPSPweight = argJson['fEPSPweight'][rep]
sEPSPweight = argJson['sEPSPweight'][rep]
stimStart = argJson['stimStart'][rep]
stimStop = argJson['stimStop'][rep]
seedtype = 'default'
seedstart = 1

simDur = 60000
saveOutput = False

path = os.getcwd()    
filename = str(seedtype) + '_' + str(seedstart).zfill(3) + '_' + str(rep).zfill(7)
filepath1 = 'output_data'
filepath2 = 'ipan_monte_carlo'
outputFilename = os.path.join(path,filepath1,filepath2,filename)


# Network parameters
netParams = specs.NetParams()

### References & Assumptions
    ### 28% of MEN are AH neurons | doi: 10.1371/journal.pone.0039887
    ### 15% of neurons are MEN | doi: 10.1371/journal.pone.0039887
    ### 7% of MEN are SAMEN | doi: 10.1371/journal.pone.0039887
    
    ### All AH cells that are MEN are RAMEN
    ### All MEN are either SAMEN or RAMEN (there are no RAMEN)
        ### Therefore 93% of MEN are RAMEN
    
    ### Assume 50:50 split of interneuron MEN between ascending and descending

    ### Proportion of myenteric neurons from p. 32, Furness 2006 [guinea-pig small intestine]
        ### C-EMN       12%
        ### C-IMN       16%
        ### L-EMN       25%
        ### L-IMN       2%
        ### A-INT       5%
        ### D-INT       11%
            ### ChAT/NOS    5%      local reflex
            ### ChAT/5HT    2%      secretomotor/motility reflex
            ### ChAT/SOM    4%      migrating motor complex
        ### IPAN        26%

    ### Of all S-type cells (and MEN):
        ### 16% are C-EMN
        ### 21% are C-IMN
        ### 38% are L-EMN
        ### 3% are L-IMN
        ### 7% are A-INT
        ### 7% are D-INT/NOS
        ### 3% are D-INT/5HT
        ### 5% are D-INT/SOM


num_ramen = np.int(0.15*numNeurons)
num_men = np.int(num_ramen/0.93)
num_samen = num_men - num_ramen

num_ah_ramen = np.int(0.28*num_men)
num_ah_samen = np.int(0)

num_s_ramen = num_ramen - num_ah_ramen
num_s_samen = num_samen

num_sens_men = num_ah_ramen
num_asc_ramen = np.int(0.07*num_s_ramen)
num_asc_samen = np.int(0.07*num_s_samen)
num_desc_ramen = np.int(0.15*num_s_ramen)
num_desc_samen = np.int(0.15*num_s_samen)

num_cemn_samen = np.int(0.16*num_s_samen)
num_cemn_ramen = np.int(0.16*num_s_ramen)
num_cimn_samen = np.int(0.21*num_s_samen)
num_cimn_ramen = np.int(0.21*num_s_ramen)

num_lemn_samen = np.int(0.38*num_s_samen)
num_lemn_ramen = np.int(0.38*num_s_ramen)
num_limn_samen = np.int(0.03*num_s_samen)
num_limn_ramen = np.int(0.03*num_s_ramen)


num_sens_cell = np.int(0.26*numNeurons) - num_sens_men
num_asc_cell = np.int(0.05*numNeurons) - (num_asc_ramen + num_asc_samen)

num_desc_local = np.int(0.05*numNeurons - 0.45*(num_desc_ramen + num_desc_samen))
num_desc_5HT = np.int(0.02*numNeurons - 0.18*(num_desc_ramen + num_desc_samen))
num_desc_MMC = np.int(0.04*numNeurons - 0.37*(num_desc_ramen + num_desc_samen))

num_desc_cell = num_desc_local + num_desc_5HT + num_desc_MMC

num_cemn_cell = np.int(0.12*numNeurons) - (num_cemn_ramen + num_cemn_samen)
num_cimn_cell = np.int(0.16*numNeurons) - (num_cimn_ramen + num_cimn_samen)

num_lemn_cell = np.int(0.25*numNeurons) - (num_lemn_ramen + num_lemn_samen)
num_limn_cell = np.int(0.02*numNeurons) - (num_limn_ramen + num_limn_samen)

print('Group\tRAMEN\tSAMEN\tnon-MEN')
print('Sens\t'+str(num_ah_ramen)+'\t'+str(num_ah_samen)+'\t'+str(num_sens_cell))
print('Asc\t'+str(num_asc_ramen)+'\t'+str(num_asc_samen)+'\t'+str(num_asc_cell))
print('Desc\t'+str(num_desc_ramen)+'\t'+str(num_desc_samen)+'\t'+str(num_desc_local+num_desc_5HT+num_desc_MMC))
print('EMN\t'+str(num_cemn_ramen+num_lemn_ramen)+'\t'+str(num_cemn_samen+num_lemn_samen)+'\t'+str(num_cemn_cell+num_lemn_cell))
print('IMN\t'+str(num_cimn_ramen+num_limn_ramen)+'\t'+str(num_cimn_samen+num_limn_samen)+'\t'+str(num_cimn_cell+num_limn_cell))

netParams.sizeX = 150*1e3
netParams.sizeY = 100
netParams.sizeZ = 0

### Population parameters
netParams.importCellParams(label='S_cell_rule', conds= {'cellType': ['S_cell']},
        fileName='s_template.py', cellName='s_cell')
netParams.importCellParams(label='S_samen_rule', conds= {'cellType': ['S_samen']},
        fileName='s_template.py', cellName='s_samen')
netParams.importCellParams(label='S_ramen_rule', conds= {'cellType': ['S_ramen']},
        fileName='s_template.py', cellName='s_ramen')

netParams.importCellParams(label='AH_cell_rule', conds= {'cellType': ['AH_cell']},
        fileName='ah_template.py', cellName='ah_cell')
netParams.importCellParams(label='AH_men_rule', conds= {'cellType': ['AH_men']},
        fileName='ah_template.py', cellName='ah_ramen')

allPops = ['sens_cell','asc_cell', 'desc_cell']
cellTypes = ['AH_cell','S_cell','S_cell']
numCells = [num_sens_cell,num_asc_cell,num_desc_cell]
XList = dict.fromkeys(allPops,[])
CellList = dict.fromkeys(allPops,[])
for p in range(len(allPops)):
    xind = list()
    cind = list()
    for i in range(numCells[p]):
        x = np.random.gumbel(0.25*netParams.sizeX,0.50*netParams.sizeX)
        while x > netParams.sizeX or x < 0:
            x = np.random.gumbel(0.25*netParams.sizeX,0.50*netParams.sizeX)
        y = np.random.uniform(0,netParams.sizeY/2)
        xind.append(x)
        cind.append({'cellLabel': allPops[p]+str(i), 'x': x, 'y': y})
    XList[allPops[p]] = xind
    CellList[allPops[p]] = cind
    netParams.popParams[allPops[p]] = {'cellType': cellTypes[p], 'cellsList': cind}

pops = list(netParams.popParams.keys())
total_cells = 0
for i in range(len(pops)):
    total_cells += len(netParams.popParams[pops[i]]['cellsList'])

### Apply tension to all populations
amp = 0.2
rate = 0.015

### Compression driver
#comp_delay = 2e3
#comp_dur = 4e3
#comp_amp = amp
#comp_rate = rate #(elongation per ms)

### Tension driver
tens_delay = 1e3
tens_dur = 30e3
tens_amp = amp
tens_rate = rate #(elongation per ms)
tens = []

def updateStrains(t):
    # Update compression
#    compFlat = comp_amp/comp_rate + comp_delay
#    compOff = comp_delay + comp_dur
#    if t < comp_delay:
#        thisCompression = 0
#    if t >= comp_delay and t < compFlat:
#        thisCompression = (t-comp_delay)*comp_rate
#    if t >= compFlat and t < comp_dur + comp_delay:
#        thisCompression = comp_amp
#    if t >= comp_dur + comp_delay and t < compOff:
#        thisCompression = comp_amp - (t - (comp_dur+comp_delay))*comp_rate
#    if t >= compOff:
#        thisCompression = 0
    
    # Update tension
    tensFlat = tens_amp/tens_rate + tens_delay
    tensOff = tens_delay + tens_dur
    if t < tens_delay:
        thisTension = 0
    if t >= tens_delay and t < tensFlat:
        thisTension = (t-tens_delay)*tens_rate
    if t >= tensFlat and t < tens_dur + tens_delay:
        thisTension = tens_amp
    if t >= tens_dur + tens_delay and t < tensOff:
        thisTension = tens_amp - (t - (tens_dur+tens_delay))*tens_rate
    if t >= tensOff:
        thisTension = 0
        
    # Set values
    for j in range(len(pops)):
        cellsInPop = len(sim.net.pops[pops[j]].cellGids)
        for i in range(cellsInPop):
            thisCell = sim.net.pops[pops[j]].cellGids[i]
#            comp[i].append(((i+1)/(numNeurons))*thisCompression)
#            sim.net.cells[thisCell].secs['soma']['mechs']['cMSC']['compression'] = (i+1)/(numNeurons)*thisCompression
#            sim.net.cells[thisCell].secs['soma']['hObj'].compression_cMSC = (i+1)/(numNeurons)*thisCompression
            tens.append(thisTension)
            sim.net.cells[thisCell].secs['soma']['mechs']['tMSC']['tension'] = thisTension
            sim.net.cells[thisCell].secs['soma']['hObj'].tension_tMSC = thisTension
    return sim


### Synaptic mechanisms
netParams.synMechParams['fEPSP'] = {'mod': 'Exp2Syn', 'tau1': 0.7, 'tau2': 20.0, 'e': 0}
#netParams.fEPSPweight = 0.0006 #0.0009125
netParams.fEPSPweight = fEPSPweight

netParams.synMechParams['sEPSP'] = {'mod': 'Exp2Syn', 'tau1': 20e3, 'tau2': 6e3, 'e': 0}
#netParams.sEPSPweight = 0.00008
netParams.sEPSPweight = sEPSPweight

netParams.synMechParams['IPSP'] = {'mod': 'Exp2Syn', 'tau1': 6e3, 'tau2': 8e3, 'e': -75}
netParams.IPSPweight = 0.002



propVelocity = 100 #(um/ms)
### Ascending synaptic distribution
def synapticConns(CellList, XList, pre, post, percentLoss = 0.2,
                  proj = lambda mu, std: np.random.normal(mu,std),
                  synsPerCellFun = lambda xNorm: 4 - 3*xNorm):
    connParams = dict({'List': [], 'synsPerConn': []})
    for i in range(len(CellList[pre])):
        nPS = np.int(np.random.normal(synsPerCellFun(XList[pre][i]/netParams.sizeX),0.75))+1
        nPS = np.max([1, nPS])
        connParams['synsPerConn'].append(nPS)
        for j in range(nPS):
            postSynLoc = proj(15e3,3e3)
            postSynCell = min(range(len(XList[post])), key=lambda k: abs((XList[post][k]-XList[pre][i])-postSynLoc))
            chance = np.random.uniform()
            if abs(XList[pre][i]-XList[post][postSynCell]) < 10e3 and chance > percentLoss:
                continue
            connParams['List'].append([i,postSynCell])
    return connParams

A_A_conn = synapticConns(CellList,XList,'asc_cell','asc_cell',
            proj = lambda x,y: np.random.normal(-1*ascProjDist,1e3),
            synsPerCellFun = lambda xNorm: (farConnMax - (farConnMax-farConnMin)*xNorm))
D_D_conn = synapticConns(CellList,XList,'desc_cell','desc_cell',
            proj = lambda x,y: np.random.normal(descProjDist,3e3),
            synsPerCellFun = lambda xNorm: (farConnMax - (farConnMax-farConnMin)*xNorm))
A_D_conn = synapticConns(CellList,XList,'asc_cell','desc_cell',
            proj = lambda x,y: np.random.normal(-1*locProjDist,2e3),
            synsPerCellFun = lambda xNorm: (locConnMax - (locConnMax-locConnMin)*xNorm))
D_A_conn = synapticConns(CellList,XList,'desc_cell','asc_cell',
            proj = lambda x,y: np.random.normal(locProjDist,2e3),
            synsPerCellFun = lambda xNorm: (locConnMax - (locConnMax-locConnMin)*xNorm))
S_S_conn = synapticConns(CellList,XList,'sens_cell','sens_cell',
            proj = lambda x,y: np.random.normal(locProjDist,2e3),
			synsPerCellFun = lambda xNorm: (locConnMax - (locConnMax-locConnMin)*xNorm))
S_A_conn = synapticConns(CellList,XList,'sens_cell','asc_cell',
			proj = lambda x,y: np.random.normal(locProjDist,2e3),
			synsPerCellFun = lambda xNorm: (locConnMax - (locConnMax-locConnMin)*xNorm))
S_D_conn = synapticConns(CellList,XList,'sens_cell','desc_cell',
			proj = lambda x,y: np.random.normal(locProjDist,2e3),
			synsPerCellFun = lambda xNorm: (locConnMax - (locConnMax-locConnMin)*xNorm))


A_A_connList = A_A_conn['List']
D_D_connList = D_D_conn['List']
A_D_connList = A_D_conn['List']
D_A_connList = D_A_conn['List']
S_S_connList = S_S_conn['List']
S_A_connList = S_A_conn['List']
S_D_connList = S_D_conn['List']

#spkTimes = [1005, 1020]
#netParams.popParams['Input'] = {'cellModel': 'VecStim', 'spkTimes': spkTimes, 'numCells':2, 'xRange': [50e3, 60e3], 'yRange': [110, 110]}    

#netParams.popParams['Noise'] = {'cellModel': 'NetStim', 'rate': inputFreq, 'noise': inputNoise, 'numCells': nInputs, 'yRange': [80, 80]}    
stimStep = 1/inputFreq*1e3
spkTimes = list()
for i in range(nInputs):
    spkTimes.append(list(np.arange(stimStart,stimStop,stimStep)+np.random.uniform(0,inputNoise*stimStep,(np.size(np.arange(stimStart,stimStop,stimStep))))))
netParams.popParams['Noise'] = {'cellModel': 'VecStim', 'numCells': nInputs, 'spkTimes': spkTimes, 'yRange': [80, 80]}    

### Cell connectivity rules
netParams.connParams['Noise->sens'] = {            
    'preConds': {'pop': 'Noise'},             
    'postConds': {'pop': 'sens_cell'},  
    'probability': 'exp(-dist_x/1e3)',
    'weight': '2*fEPSPweight',                  
    'delay': 0,                             
    'synMech': 'fEPSP'}
netParams.connParams['sens->sens'] = {
	'preConds': {'pop': 'sens_cell'},
	'postConds': {'pop': 'sens_cell'},
	'connList': S_S_connList,
	'weight': 'sEPSPweight',
	'delay': 'dist_x/propVelocity',
	'synMech': 'sEPSP'}
netParams.connParams['sens->desc'] = {
	'preConds': {'pop': 'sens_cell'},
	'postConds': {'pop': 'desc_cell'},
	'connList': S_D_connList,
	'weight': 'fEPSPweight',
	'delay': 'dist_x/propVelocity',
	'synMech': 'fEPSP'}
netParams.connParams['sens->asc'] = {
	'preConds': {'pop': 'sens_cell'},
	'postConds': {'pop': 'asc_cell'},
	'connList': S_A_connList,
	'weight': 'fEPSPweight',
	'delay': 'dist_x/propVelocity',
	'synMech': 'fEPSP'}
netParams.connParams['asc->asc'] = {
    'preConds': {'pop': ['asc_cell']},
    'postConds': {'pop': ['asc_cell']},
    'connList': A_A_connList,
    'weight': 'fEPSPweight',
    'delay': 'dist_x/propVelocity',
    'synMech': 'fEPSP'}
netParams.connParams['desc->desc'] = {
    'preConds': {'pop': ['desc_cell']},
    'postConds': {'pop': ['desc_cell']},
    'connList': D_D_connList,
    'weight': 'fEPSPweight',
    'delay': 'dist_x/propVelocity',
    'synMech': 'fEPSP'}
netParams.connParams['asc->desc'] = {
    'preConds': {'pop': ['asc_cell']},
    'postConds': {'pop': ['desc_cell']},
    'connList': A_D_connList,
    'weight': 'fEPSPweight',
    'delay': 'dist_x/propVelocity',
    'synMech': 'fEPSP'}
netParams.connParams['desc->asc'] = {
    'preConds': {'pop': ['desc_cell']},
    'postConds': {'pop': ['asc_cell']},
    'connList': D_A_connList,
    'weight': 'fEPSPweight',
    'delay': 'dist_x/propVelocity',
    'synMech': 'fEPSP'}

### Simulation options
simConfig = specs.SimConfig()                   # object of class SimConfig to store simulation configuration

simConfig.hParams['v_init'] = -52
simConfig.duration = simDur                     # Duration of the simulation, in ms
simConfig.dt = 0.05                             # Internal integration timestep to use
simConfig.verbose = False                       # Show detailed messages
simConfig.recordStep = 0.1                      # Step size in ms to save data (e.g. V traces, LFP, etc)

simConfig.saveDataInclude = ['netParams', 'netCells', 'netPops', 'simConfig', 'simData']
simConfig.filename = outputFilename + '_model_output'   # Set file output name
simConfig.saveJson = saveOutput                 # Save params, network and sim output to json file
sim.updateInterval = 2*simConfig.dt                                                
simConfig.recordLFP = [[0, 10e3, 0]]

simConfig.analysis['plotRaster'] = {'include': ['sens_cell','asc_cell','desc_cell'], 'spikeHist': 'subplot',
                  'orderBy': 'x', 'orderInverse': True, 'syncLines': True} # Plot a raster
simConfig.analysis['plot2Dnet'] = True

sim.create(netParams, simConfig)

for i in range(len(sim.net.cells)):
    if sim.net.cells[i].tags['pop'] == 'Input' or sim.net.cells[i].tags['pop'] == 'Noise': continue
    modifier = np.random.normal(loc=0.0,scale=0.02)
    cOn = sim.net.cells[i].secs['soma']['mechs']['cMSC']['onHalf']
    sim.net.cells[i].secs['soma']['mechs']['cMSC']['onHalf'] = (1.0+modifier)*cOn
    sim.net.cells[i].secs['soma']['hObj'].onHalf_cMSC = (1.0+modifier)*cOn
    cOff = sim.net.cells[i].secs['soma']['mechs']['cMSC']['offHalf']
    sim.net.cells[i].secs['soma']['mechs']['cMSC']['offHalf'] = (0.01*modifier)+cOff
    sim.net.cells[i].secs['soma']['hObj'].offHalf_cMSC = (0.01*modifier)+cOff
    tOn = sim.net.cells[i].secs['soma']['mechs']['tMSC']['onHalf']
    sim.net.cells[i].secs['soma']['mechs']['tMSC']['onHalf'] = (1.0+modifier)*tOn
    sim.net.cells[i].secs['soma']['hObj'].onHalf_tMSC = (1.0+modifier)*tOn
    tOff = sim.net.cells[i].secs['soma']['mechs']['tMSC']['offHalf']
    sim.net.cells[i].secs['soma']['mechs']['tMSC']['offHalf'] = (1.0+modifier)*tOff
    sim.net.cells[i].secs['soma']['hObj'].offHalf_tMSC = (1.0+modifier)*tOff

sim.runSimWithIntervalFunc(sim.updateInterval, updateStrains)
#sim.simulate()
sim.analyze()

from itertools import compress

pops = list(sim.net.pops.keys())
cells = {k: [] for k in pops}
ALLspikes = []
for i in range(len(pops)):
    cells[pops[i]].append(sim.net.pops[pops[i]].cellGids)
    
    spkID = list(sim.simData.spkid)
    spkT = list(sim.simData.spkt)
    allSpikes = []
    for i in range(len(pops)):
        if pops[i]=='Noise': continue
        else:
            tempCells = np.in1d(spkID,cells[pops[i]])
            tempTimes = list(compress(spkT,tempCells))
            for k in range(len(tempTimes)):
                allSpikes.append(tempTimes[k]/1e3)
        
    histData = plt.hist(allSpikes,bins=int(4*30))
    hist = histData[0]
    bin_edges = histData[1]
    [f,pxx] = signal.welch(hist,1/(bin_edges[1]-bin_edges[0]))
    outputFrequency = f[np.argmax(pxx)]
    ALLspikes = ALLspikes + allSpikes


histData = plt.hist(ALLspikes,bins=int(4*30))
if len(histData) > 0:
    if len(histData[0]) > 0:
        thresh = np.argwhere(histData[0]>0.1*np.max(histData[0]))
        if len(thresh) > 0:
            startFiring = histData[1][thresh[0]]
            stopFiring = histData[1][thresh[-1]]
        else:
            startFiring = np.array(np.nan)
            stopFiring = np.array(np.nan)
        elem = np.nonzero(histData[0])
        nSpk = list(histData[0][elem])
        tSpk = list(histData[1][elem])
    else:
        startFiring = np.array(np.nan)
        stopFiring = np.array(np.nan)
        nSpk = np.array(np.nan)
        tSpk = np.array(np.nan)
else:
    startFiring = np.array(np.nan)
    stopFiring = np.array(np.nan)
    nSpk = np.array(np.nan)
    tSpk = np.array(np.nan)

spikeTxt = outputFilename + '_spikes.txt'
spikeTrains = list()
spikeJson = list()
rate = list()
for i in range(len(sim.net.cells)):
    spikeTrains.append(list())
    spikeJson.append(list())
    logInd = [(x == i) for x in sim.simData['spkid']]
    timeArray = np.array(sim.simData['spkt'])
    theseSpikes = np.array(timeArray[logInd])
    theseSpikes.sort()
    spikeTrains[i] = spk.SpikeTrain(theseSpikes,np.array([0.0, simDur]))
    spikeJson[i] = list(theseSpikes)
    if np.size(theseSpikes) > 0:
        rate.append(1/(np.mean(np.diff(theseSpikes)/1e3)))

output = dict()
syncStats = sim.analysis.spikes.calculateRate(include=['allCells','sens_cell','asc_cell','desc_cell'])

output['input'] = dict()
output['input']['numNeurons'] = numNeurons
output['input']['simDur'] = simDur
output['input']['nInputs'] = nInputs
output['input']['inputFreq'] = inputFreq
output['input']['inputNoise'] = inputNoise
output['input']['replicate'] = rep
output['input']['ascProjDist'] = ascProjDist
output['input']['descProjDist'] = descProjDist
output['input']['locProjDist'] = locProjDist
output['input']['farConnMax'] = farConnMax
output['input']['farConnMin'] = farConnMin
output['input']['locConnMax'] = locConnMax
output['input']['locConnMin'] = locConnMin
output['input']['fEPSPweight'] = fEPSPweight
output['input']['sEPSPweight'] = sEPSPweight
output['input']['stimStart'] = stimStart
output['input']['stimStop'] = stimStop

output['syncMeasure'] = sim.analysis.syncMeasure()
output['syncRate'] = (num_sens_cell*syncStats[1][1]+num_asc_cell*syncStats[1][2]+num_desc_cell*syncStats[1][3])/(num_sens_cell+num_asc_cell+num_desc_cell)
#    output['pspectrumFreq'] = outputFrequency
output['rateMean'] = np.nanmean(rate)
output['rateStd'] = np.nanstd(rate)
output['1-PySpikeISISdist'] = 1-spk.isi_distance(spikeTrains)
output['PySpikeSync'] = spk.spike_sync(spikeTrains)
output['connsPerCell'] = sim.connsPerCell
output['synsPerCell'] = sim.synsPerCell
output['firingStart'] = startFiring.item(0)
output['firingStop'] = stopFiring.item(0)
output['spkCount'] = nSpk
output['spkTime'] = tSpk

outputFilename = outputFilename + '_model_measures.json'
with open(outputFilename,'w') as f:
    json.dump(output,f)

