# -*- coding: utf-8 -*-
"""
Created on Mon Oct 14 15:33:56 2019

@author: Bradley Barth
"""
from netpyne import specs, sim
import numpy as np
import json
import pyspike as spk
import sys
import os

argvLen = len(sys.argv)

numNeurons = int(sys.argv[argvLen-7])
simDur = float(sys.argv[argvLen-6])
inputFreq = float(sys.argv[argvLen-5])
inputNoise = float(sys.argv[argvLen-4])
nInputs =int(sys.argv[argvLen-3])
rep = int(sys.argv[argvLen-2])
saveOutput = bool(int(sys.argv[argvLen-1]))

path = os.getcwd()    
filename = str(numNeurons).zfill(6) + '_' + str(int((simDur)/1e3)).zfill(4) + '_' + str(inputFreq).zfill(6) + '_' + str(inputNoise).zfill(5) + '_'  + str(int(nInputs)).zfill(5) + '_' + str(rep).zfill(4)
filepath1 = 'output_data'
filepath2 = 'numNeurons_simDur_inputFreq_inputNoise_nInputs_replicate'
outputFilename = os.path.join(path,filepath1,filepath2,filename)


# Network parameters
netParams = specs.NetParams()

### References & Assumptions
    ### 28% of MEN are AH neurons | doi: 10.1371/journal.pone.0039887
    ### 15% of neurons are MEN | doi: 10.1371/journal.pone.0039887
    ### 7% of MEN are SAMEN | doi: 10.1371/journal.pone.0039887
    
    ### All AH cells that are MEN are RAMEN
    ### All MEN are either SAMEN or RAMEN (there are no RAMEN)
        ### Therefore 93% of MEN are RAMEN
    
    ### Assume 50:50 split of interneuron MEN between ascending and descending

    ### Proportion of myenteric neurons from p. 32, Furness 2006 [guinea-pig small intestine]
        ### C-EMN       12%
        ### C-IMN       16%
        ### L-EMN       25%
        ### L-IMN       2%
        ### A-INT       5%
        ### D-INT       11%
            ### ChAT/NOS    5%      local reflex
            ### ChAT/5HT    2%      secretomotor/motility reflex
            ### ChAT/SOM    4%      migrating motor complex
        ### IPAN        26%

    ### Of all S-type cells (and MEN):
        ### 16% are C-EMN
        ### 21% are C-IMN
        ### 38% are L-EMN
        ### 3% are L-IMN
        ### 7% are A-INT
        ### 7% are D-INT/NOS
        ### 3% are D-INT/5HT
        ### 5% are D-INT/SOM


num_ramen = np.int(0.15*numNeurons)
num_men = np.int(num_ramen/0.93)
num_samen = num_men - num_ramen

num_ah_ramen = np.int(0.28*num_men)
num_ah_samen = np.int(0)

num_s_ramen = num_ramen - num_ah_ramen
num_s_samen = num_samen

num_sens_men = num_ah_ramen
num_asc_ramen = np.int(0.07*num_s_ramen)
num_asc_samen = np.int(0.07*num_s_samen)
num_desc_ramen = np.int(0.15*num_s_ramen)
num_desc_samen = np.int(0.15*num_s_samen)

num_cemn_samen = np.int(0.16*num_s_samen)
num_cemn_ramen = np.int(0.16*num_s_ramen)
num_cimn_samen = np.int(0.21*num_s_samen)
num_cimn_ramen = np.int(0.21*num_s_ramen)

num_lemn_samen = np.int(0.38*num_s_samen)
num_lemn_ramen = np.int(0.38*num_s_ramen)
num_limn_samen = np.int(0.03*num_s_samen)
num_limn_ramen = np.int(0.03*num_s_ramen)


num_sens_cell = np.int(0.26*numNeurons) - num_sens_men
num_asc_cell = np.int(0.05*numNeurons) - (num_asc_ramen + num_asc_samen)

num_desc_local = np.int(0.05*numNeurons - 0.45*(num_desc_ramen + num_desc_samen))
num_desc_5HT = np.int(0.02*numNeurons - 0.18*(num_desc_ramen + num_desc_samen))
num_desc_MMC = np.int(0.04*numNeurons - 0.37*(num_desc_ramen + num_desc_samen))

num_desc_cell = num_desc_local + num_desc_5HT + num_desc_MMC

num_cemn_cell = np.int(0.12*numNeurons) - (num_cemn_ramen + num_cemn_samen)
num_cimn_cell = np.int(0.16*numNeurons) - (num_cimn_ramen + num_cimn_samen)

num_lemn_cell = np.int(0.25*numNeurons) - (num_lemn_ramen + num_lemn_samen)
num_limn_cell = np.int(0.02*numNeurons) - (num_limn_ramen + num_limn_samen)

print('Group\tRAMEN\tSAMEN\tnon-MEN')
print('Sens\t'+str(num_ah_ramen)+'\t'+str(num_ah_samen)+'\t'+str(num_sens_cell))
print('Asc\t'+str(num_asc_ramen)+'\t'+str(num_asc_samen)+'\t'+str(num_asc_cell))
print('Desc\t'+str(num_desc_ramen)+'\t'+str(num_desc_samen)+'\t'+str(num_desc_local+num_desc_5HT+num_desc_MMC))
print('EMN\t'+str(num_cemn_ramen+num_lemn_ramen)+'\t'+str(num_cemn_samen+num_lemn_samen)+'\t'+str(num_cemn_cell+num_lemn_cell))
print('IMN\t'+str(num_cimn_ramen+num_limn_ramen)+'\t'+str(num_cimn_samen+num_limn_samen)+'\t'+str(num_cimn_cell+num_limn_cell))

netParams.sizeX = 150*1e3
netParams.sizeY = 100
netParams.sizeZ = 0

### Population parameters
# Interneurons
#netParams.popParams['asc_cell'] = {'cellType': 'S_cell', 'numCells': num_asc_cell, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}
#netParams.popParams['asc_samen'] = {'cellType': 'S_samen', 'numCells': num_asc_samen, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}
#netParams.popParams['asc_ramen'] = {'cellType': 'S_ramen', 'numCells': num_asc_ramen, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}
#netParams.popParams['desc_local'] = {'cellType': 'S_cell', 'numCells': num_desc_local, 'xnormRange': [0, 1], 'ynormRange': [0.95, 1]}
#netParams.popParams['desc_5HT'] = {'cellType': 'S_cell', 'numCells': num_desc_5HT, 'xnormRange': [0, 1], 'ynormRange': [0.95, 1]}
#netParams.popParams['desc_MMC'] = {'cellType': 'S_cell', 'numCells': num_desc_MMC, 'xnormRange': [0, 1], 'ynormRange': [0.95, 1]}
#netParams.popParams['desc_samen'] = {'cellType': 'S_samen', 'numCells': num_desc_samen, 'xnormRange': [0, 1], 'ynormRange': [0.95, 1]}
#netParams.popParams['desc_ramen'] = {'cellType': 'S_ramen', 'numCells': num_desc_ramen, 'xnormRange': [0, 1], 'ynormRange': [0.95, 1]}
## Sensory neurons
#netParams.popParams['sens_cell'] = {'cellType': 'AH_cell', 'numCells': num_sens_cell, 'xnormRange': [0, 1], 'ynormRange': [0, 0.05]}
#netParams.popParams['sens_men'] = {'cellType': 'AH_men', 'numCells': num_sens_men, 'xnormRange': [0, 1], 'ynormRange': [0, 0.05]}
## Motor neurons
#netParams.popParams['cemn_cell'] = {'cellType': 'S_cell', 'numCells': num_cemn_cell, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}
#netParams.popParams['cemn_samen'] = {'cellType': 'S_samen', 'numCells': num_cemn_ramen, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}
#netParams.popParams['cemn_ramen'] = {'cellType': 'S_ramen', 'numCells': num_cemn_ramen, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}
#netParams.popParams['cimn_cell'] = {'cellType': 'S_cell', 'numCells': num_cimn_cell, 'xnormRange': [0, 1], 'ynormRange': [0.95, 1]}
#netParams.popParams['cimn_samen'] = {'cellType': 'S_samen', 'numCells': num_cimn_ramen, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}
#netParams.popParams['cimn_ramen'] = {'cellType': 'S_ramen', 'numCells': num_cimn_ramen, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}
#netParams.popParams['lemn_cell'] = {'cellType': 'S_cell', 'numCells': num_lemn_cell, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}
#netParams.popParams['lemn_samen'] = {'cellType': 'S_samen', 'numCells': num_lemn_ramen, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}
#netParams.popParams['lemn_ramen'] = {'cellType': 'S_ramen', 'numCells': num_lemn_ramen, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}
#netParams.popParams['limn_cell'] = {'cellType': 'S_cell', 'numCells': num_limn_cell, 'xnormRange': [0, 1], 'ynormRange': [0.95, 1]}
#netParams.popParams['limn_samen'] = {'cellType': 'S_samen', 'numCells': num_limn_ramen, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}
#netParams.popParams['limn_ramen'] = {'cellType': 'S_ramen', 'numCells': num_limn_ramen, 'xnormRange': [0, 1], 'ynormRange': [0.875, 0.925]}


netParams.importCellParams(label='S_cell_rule', conds= {'cellType': ['S_cell']},
        fileName='s_template.py', cellName='s_cell')
netParams.importCellParams(label='S_samen_rule', conds= {'cellType': ['S_samen']},
        fileName='s_template.py', cellName='s_samen')
netParams.importCellParams(label='S_ramen_rule', conds= {'cellType': ['S_ramen']},
        fileName='s_template.py', cellName='s_ramen')

netParams.importCellParams(label='AH_cell_rule', conds= {'cellType': ['AH_cell']},
        fileName='ah_template.py', cellName='ah_cell')
netParams.importCellParams(label='AH_men_rule', conds= {'cellType': ['AH_men']},
        fileName='ah_template.py', cellName='ah_ramen')

allPops = ['asc_cell', 'desc_cell']
cellTypes = ['S_cell','S_cell']
numCells = [num_asc_cell,num_desc_cell]
XList = dict.fromkeys(allPops,[])
CellList = dict.fromkeys(allPops,[])
for p in range(len(allPops)):
    xind = list()
    cind = list()
    for i in range(numCells[p]):
        x = np.random.gumbel(0.25*netParams.sizeX,0.50*netParams.sizeX)
        while x > netParams.sizeX or x < 0:
            x = np.random.gumbel(0.25*netParams.sizeX,0.50*netParams.sizeX)
        y = np.random.uniform(0,netParams.sizeY/2)
        xind.append(x)
        cind.append({'cellLabel': allPops[p]+str(i), 'x': x, 'y': y})
    XList[allPops[p]] = xind
    CellList[allPops[p]] = cind
    netParams.popParams[allPops[p]] = {'cellType': cellTypes[p], 'cellsList': cind}
    
#netParams.popParams['Input'] = {'cellModel': 'VecStim', 'spkTimes': spkTimes, 'numCells':1, 'xRange': [55e3, 55e3], 'yRange': [110, 110]}    
#
#netParams.popParams['P1'] = {'cellModel': 'HH', 'cellsList': CellList['P1']}
#netParams.popParams['P2'] = {'cellModel': 'HH', 'cellsList': CellList['P2']}



pops = list(netParams.popParams.keys())
total_cells = 0
for i in range(len(pops)):
    total_cells += len(netParams.popParams[pops[i]]['cellsList'])

### Apply tension to all populations
amp = 0.2
rate = 0.015

### Compression driver
#comp_delay = 2e3
#comp_dur = 4e3
#comp_amp = amp
#comp_rate = rate #(elongation per ms)

### Tension driver
tens_delay = 1e3
tens_dur = 30e3
tens_amp = amp
tens_rate = rate #(elongation per ms)
tens = []

def updateStrains(t):
    # Update compression
#    compFlat = comp_amp/comp_rate + comp_delay
#    compOff = comp_delay + comp_dur
#    if t < comp_delay:
#        thisCompression = 0
#    if t >= comp_delay and t < compFlat:
#        thisCompression = (t-comp_delay)*comp_rate
#    if t >= compFlat and t < comp_dur + comp_delay:
#        thisCompression = comp_amp
#    if t >= comp_dur + comp_delay and t < compOff:
#        thisCompression = comp_amp - (t - (comp_dur+comp_delay))*comp_rate
#    if t >= compOff:
#        thisCompression = 0
    
    # Update tension
    tensFlat = tens_amp/tens_rate + tens_delay
    tensOff = tens_delay + tens_dur
    if t < tens_delay:
        thisTension = 0
    if t >= tens_delay and t < tensFlat:
        thisTension = (t-tens_delay)*tens_rate
    if t >= tensFlat and t < tens_dur + tens_delay:
        thisTension = tens_amp
    if t >= tens_dur + tens_delay and t < tensOff:
        thisTension = tens_amp - (t - (tens_dur+tens_delay))*tens_rate
    if t >= tensOff:
        thisTension = 0
        
    # Set values
    for j in range(len(pops)):
        cellsInPop = len(sim.net.pops[pops[j]].cellGids)
        for i in range(cellsInPop):
            thisCell = sim.net.pops[pops[j]].cellGids[i]
#            comp[i].append(((i+1)/(numNeurons))*thisCompression)
#            sim.net.cells[thisCell].secs['soma']['mechs']['cMSC']['compression'] = (i+1)/(numNeurons)*thisCompression
#            sim.net.cells[thisCell].secs['soma']['hObj'].compression_cMSC = (i+1)/(numNeurons)*thisCompression
            tens.append(thisTension)
            sim.net.cells[thisCell].secs['soma']['mechs']['tMSC']['tension'] = thisTension
            sim.net.cells[thisCell].secs['soma']['hObj'].tension_tMSC = thisTension
    return sim


### Synaptic mechanisms
netParams.synMechParams['fEPSP'] = {'mod': 'Exp2Syn', 'tau1': 0.7, 'tau2': 20.0, 'e': 0}
netParams.fEPSPweight = 0.0006 #0.0009125

netParams.synMechParams['sEPSP'] = {'mod': 'Exp2Syn', 'tau1': 25e3, 'tau2': 55e3, 'e': 0}
netParams.sEPSPweight = 0.00008

netParams.synMechParams['IPSP'] = {'mod': 'Exp2Syn', 'tau1': 6e3, 'tau2': 8e3, 'e': -75}
netParams.IPSPweight = 0.002



propVelocity = 100 #(um/ms)
### Ascending synaptic distribution
def synapticConns(CellList, XList, pre, post, percentLoss = 0.2,
                  proj = lambda mu, std: np.random.normal(mu,std),
                  synsPerCellFun = lambda xNorm: 4 - 3*xNorm):
    connParams = dict({'List': [], 'synsPerConn': []})
    for i in range(len(CellList[pre])):
        nPS = np.int(np.random.normal(synsPerCellFun(XList[pre][i]/netParams.sizeX),0.75))+1
        nPS = np.max([1, nPS])
        connParams['synsPerConn'].append(nPS)
        for j in range(nPS):
            postSynLoc = proj(15e3,3e3)
            postSynCell = min(range(len(XList[post])), key=lambda k: abs((XList[post][k]-XList[pre][i])-postSynLoc))
            chance = np.random.uniform()
            if abs(XList[pre][i]-XList[post][postSynCell]) < 10e3 and chance > percentLoss:
                continue
            connParams['List'].append([i,postSynCell])
    return connParams

A_A_conn = synapticConns(CellList,XList,'asc_cell','asc_cell',
                         proj = lambda x,y: np.random.normal(-6e3,1e3),
                         synsPerCellFun = lambda xNorm: 6 - 3*xNorm)
D_D_conn = synapticConns(CellList,XList,'desc_cell','desc_cell',
                         proj = lambda x,y: np.random.normal(15e3,3e3))
#                         synsPerCellFun = lambda xNorm: 4 - 3*xNorm)
A_D_conn = synapticConns(CellList,XList,'asc_cell','desc_cell',
                         proj = lambda x,y: np.random.normal(-1e3,2e3),
                         synsPerCellFun = lambda xNorm: 6- 3*xNorm)
D_A_conn = synapticConns(CellList,XList,'desc_cell','asc_cell',
                         proj = lambda x,y: np.random.normal(1e3,2e3))
#                         synsPerCellFun = lambda xNorm: 4 - 3*xNorm)

A_A_connList = A_A_conn['List']
D_D_connList = D_D_conn['List']
A_D_connList = A_D_conn['List']
D_A_connList = D_A_conn['List']

#spkTimes = [1005, 1020]
#netParams.popParams['Input'] = {'cellModel': 'VecStim', 'spkTimes': spkTimes, 'numCells':2, 'xRange': [50e3, 60e3], 'yRange': [110, 110]}    

netParams.popParams['Noise'] = {'cellModel': 'NetStim', 'rate': inputFreq, 'noise': inputNoise, 'numCells': nInputs, 'yRange': [80, 80]}    


netParams.p_max_asc = 4000
netParams.sigma_a = 0.3
A_proj_mean = 6000
netParams.mu_a = np.log(A_proj_mean)
## Descending synaptic distribution
netParams.p_max_desc = 4000
netParams.sigma_d = 0.3
D_proj_mean = 15000
netParams.mu_d = np.log(D_proj_mean)
## Local synaptic distribution
netParams.p_max_local = 800
netParams.sigma_l = 0.3
L_proj_mean = 1000
netParams.mu_l = np.log(L_proj_mean)
## IPAN to IPAN synaptic distribution
netParams.p_max_sens = 400
netParams.sigma_s = 0.3
S_proj_mean = 1000
netParams.mu_s = np.log(S_proj_mean)


### Cell connectivity rules
#netParams.connParams['Input->INT'] = {            
#        'preConds': {'pop': 'Input'},             
#        'postConds': {'pop': ['asc_cell','desc_cell'], 'x': [50e3, 60e3]},  
#        'probability': 'exp(-dist_x/1e3)',
#        'weight': 'fEPSPweight',                  
#        'delay': 2,                             
#        'synMech': 'fEPSP'}
netParams.connParams['Noise->INT'] = {            
        'preConds': {'pop': 'Noise'},             
        'postConds': {'pop': ['asc_cell','desc_cell']},  
        'probability': 'exp(-dist_x/1e3)',
        'weight': 'fEPSPweight',                  
        'delay': 0,                             
        'synMech': 'fEPSP'}
netParams.connParams['asc->asc'] = {            # ascending to ascending
        'preConds': {'pop': ['asc_cell']},#,'asc_samen','asc_ramen']},             # conditions of presyn cells
        'postConds': {'pop': ['asc_cell']},#,'asc_samen','asc_ramen']},            # conditions of postsyn cells
#        'probability': 'p_max_asc / ((pre_x - post_x) * sigma_a * sqrt(2*np.pi)) * exp(-((np.log(pre_x - post_x)-mu_a)**2)/(2*sigma_a**2))',             # probability of connection
        'connList': A_A_connList,
        'weight': 'fEPSPweight',                # synaptic weight
        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
        'synMech': 'fEPSP'}                     # synaptic mechanism
#netParams.connParams['descending_5HT_projection'] = {                          # descending 5HT distal projection
#        'preConds': {'pop': ['desc_5HT']},            # conditions of presyn cells
#        'postConds': {'pop': ['desc_5HT','cimn_cell','cimn_samen','cimn_ramen','limn_cell','limn_samen','limn_ramen','asc_cell','asc_samen','asc_ramen']},           # conditions of postsyn cells
#        'probability': 'p_max_desc / ((post_x - pre_x) * sigma_d * sqrt(2*np.pi)) * exp(-((np.log(post_x - pre_x)-mu_d)**2)/(2*sigma_d**2))',             # probability of connection
#        'weight': 'fEPSPweight',                # synaptic weight
#        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
#        'synMech': 'fEPSP'}                     # synaptic mechanism
#netParams.connParams['descending_5HT_local'] = {                               # descending 5HT distal projection
#        'preConds': {'pop': ['desc_5HT']},            # conditions of presyn cells
#        'postConds': {'pop': ['desc_5HT','sens_cell','sens_men','cimn_cell','cimn_samen','cimn_ramen','limn_cell','limn_samen','limn_ramen']},           # conditions of postsyn cells
#        'probability': 'p_max_local / ((post_x - pre_x) * sigma_l * sqrt(2*np.pi)) * exp(-((np.log(post_x - pre_x)-mu_l)**2)/(2*sigma_l**2))',             # probability of connection
#        'weight': 'fEPSPweight',                # synaptic weight
#        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
#        'synMech': 'fEPSP'}                     # synaptic mechanism
netParams.connParams['desc->desc'] = {          # descending to descending
        'preConds': {'pop': ['desc_cell']},#'desc_MMC','desc_samen','desc_ramen']},            # conditions of presyn cells
        'postConds': {'pop': ['desc_cell']},#'desc_MMC','desc_samen','desc_ramen']},           # conditions of postsyn cells
#        'probability': 'p_max_desc / ((post_x - pre_x) * sigma_d * sqrt(2*np.pi)) * exp(-((np.log(post_x - pre_x)-mu_d)**2)/(2*sigma_d**2))',             # probability of connection
        'connList': D_D_connList,
        'weight': 'fEPSPweight',                # synaptic weight
        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
        'synMech': 'fEPSP'}                     # synaptic mechanism
netParams.connParams['asc->desc'] = {           # local ascending to descending and vice versa
        'preConds': {'pop': ['asc_cell']},#,'asc_samen','asc_ramen','desc_MMC','desc_samen','desc_ramen']},             # conditions of presyn cells
        'postConds': {'pop': ['desc_cell']},#'asc_cell','asc_samen','asc_ramen','desc_MMC','desc_samen','desc_ramen']},            # conditions of postsyn cells
#        'probability': 'p_max_local / (dist_x * sigma_l * sqrt(2*np.pi)) * exp(-((np.log(dist_x)-mu_l)**2)/(2*sigma_l**2))',             # probability of connection
        'connList': A_D_connList,
        'weight': 'fEPSPweight',                # synaptic weight
        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
        'synMech': 'fEPSP'}                     # synaptic mechanism
netParams.connParams['desc->asc'] = {           # local ascending to descending and vice versa
        'preConds': {'pop': ['desc_cell']},#,'asc_samen','asc_ramen','desc_MMC','desc_samen','desc_ramen']},             # conditions of presyn cells
        'postConds': {'pop': ['asc_cell']},#'asc_cell','asc_samen','asc_ramen','desc_MMC','desc_samen','desc_ramen']},            # conditions of postsyn cells
#        'probability': 'p_max_local / (dist_x * sigma_l * sqrt(2*np.pi)) * exp(-((np.log(dist_x)-mu_l)**2)/(2*sigma_l**2))',             # probability of connection
        'connList': D_A_connList,
        'weight': 'fEPSPweight',                # synaptic weight
        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
        'synMech': 'fEPSP'}                     # synaptic mechanism
#netParams.connParams['sens->sens'] = {          # sensory to sensory (ipan to ipan)
#        'preConds': {'pop': ['sens_cell','sens_men']},             # conditions of presyn cells
#        'postConds': {'pop': ['sens_cell','sens_men']},            # conditions of postsyn cells
#        'probability': 'p_max_sens / (dist_x * sigma_s * sqrt(2*np.pi)) * exp(-((np.log(dist_x)-mu_s)**2)/(2*sigma_s**2))',             # probability of connection
#        'weight': 'sEPSPweight',                # synaptic weight
#        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
#        'synMech': 'sEPSP'}                     # synaptic mechanism
#netParams.connParams['sens->int'] = {           # sensory to interneuron
#        'preConds': {'pop': ['sens_cell','sens_men']},             # conditions of presyn cells
#        'postConds': {'pop': ['asc_cell','asc_samen','asc_ramen','desc_local','desc_5HT','desc_MMC','desc_samen','desc_ramen']},            # conditions of postsyn cells
#        'probability': 'p_max_local / (dist_x * sigma_l * sqrt(2*np.pi)) * exp(-((np.log(dist_x)-mu_l)**2)/(2*sigma_l**2))',             # probability of connection
#        'weight': 'fEPSPweight',                # synaptic weight
#        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
#        'synMech': 'fEPSP'}                     # synaptic mechanism
#netParams.connParams['asc->EMN'] = {          # descending to descending
#        'preConds': {'pop': ['asc_cell','asc_samen','asc_ramen']},            # conditions of presyn cells
#        'postConds': {'pop': ['cemn_cell','cemn_samen','cemn_ramen','lemn_cell','lemn_samen','lemn_ramen']},           # conditions of postsyn cells
#        'probability': 'p_max_desc / ((post_x - pre_x) * sigma_d * sqrt(2*np.pi)) * exp(-((np.log(post_x - pre_x)-mu_d)**2)/(2*sigma_d**2))',             # probability of connection
#        'weight': 'fEPSPweight',                # synaptic weight
#        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
#        'synMech': 'fEPSP'}                     # synaptic mechanism
#netParams.connParams['desc->IMN'] = {          # descending to descending
#        'preConds': {'pop': ['desc_local','desc_MMC','desc_samen','desc_ramen']},            # conditions of presyn cells
#        'postConds': {'pop': ['cimn_cell','cimn_samen','cimn_ramen','limn_cell','limn_samen','limn_ramen']},           # conditions of postsyn cells
#        'probability': 'p_max_desc / ((post_x - pre_x) * sigma_d * sqrt(2*np.pi)) * exp(-((np.log(post_x - pre_x)-mu_d)**2)/(2*sigma_d**2))',             # probability of connection
#        'weight': 'fEPSPweight',                # synaptic weight
#        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
#        'synMech': 'fEPSP'}                     # synaptic mechanism
#netParams.connParams['IMN->IPAN,EMN,INT'] = {          # IMN inhibition of IPAN, EMN, some INT
#        'preConds': {'pop': ['cimn_cell','cimn_samen','cimn_ramen','limn_cell','limn_samen','limn_ramen']},            # conditions of presyn cells
#        'postConds': {'pop': ['sens_cell','sens_men','cemn_cell','cemn_samen','cemn_ramen','lemn_cell','lemn_samen','lemn_ramen']},           # conditions of postsyn cells
#        'probability': 'p_max_local / ((post_x - pre_x) * sigma_l * sqrt(2*np.pi)) * exp(-((np.log(post_x - pre_x)-mu_l)**2)/(2*sigma_l**2))',             # probability of connection
#        'weight': 'IPSPweight',                # synaptic weight
#        'delay': 'dist_x/propVelocity',         # transmission delay (ms)
#        'synMech': 'IPSP'}                     # synaptic mechanism

### Simulation options
simConfig = specs.SimConfig()                   # object of class SimConfig to store simulation configuration

simConfig.hParams['v_init'] = -52
simConfig.duration = simDur                     # Duration of the simulation, in ms
simConfig.dt = 0.05                             # Internal integration timestep to use
simConfig.verbose = False                       # Show detailed messages
simConfig.recordStep = 0.1                      # Step size in ms to save data (e.g. V traces, LFP, etc)

simConfig.saveDataInclude = ['netParams', 'netCells', 'netPops', 'simConfig', 'simData']
simConfig.filename = outputFilename + '_model_output'   # Set file output name
simConfig.saveJson = saveOutput                 # Save params, network and sim output to json file
sim.updateInterval = 2*simConfig.dt                                                
simConfig.recordLFP = [[0, 10e3, 0]]

simConfig.analysis['plotRaster'] = {'include': ['asc_cell','desc_cell'], 'spikeHist': 'subplot',
                  'orderBy': 'x', 'orderInverse': True, 'syncLines': True} # Plot a raster
#simConfig.analysis['plotSpikeStats'] = {'include': ['asc_cell','desc_cell'], stats = ['rate','sync']}
#simConfig.analysis['plotLFP'] = {'electrodes': [0], 'plots': ['timeSeries'], 'figSize': (12,9)}
simConfig.analysis['plot2Dnet'] = True

sim.create(netParams, simConfig)

for i in range(len(sim.net.cells)):
    if sim.net.cells[i].tags['pop'] == 'Input' or sim.net.cells[i].tags['pop'] == 'Noise': continue
    modifier = np.random.normal(loc=0.0,scale=0.02)
    cOn = sim.net.cells[i].secs['soma']['mechs']['cMSC']['onHalf']
    sim.net.cells[i].secs['soma']['mechs']['cMSC']['onHalf'] = (1.0+modifier)*cOn
    sim.net.cells[i].secs['soma']['hObj'].onHalf_cMSC = (1.0+modifier)*cOn
    cOff = sim.net.cells[i].secs['soma']['mechs']['cMSC']['offHalf']
    sim.net.cells[i].secs['soma']['mechs']['cMSC']['offHalf'] = (0.01*modifier)+cOff
    sim.net.cells[i].secs['soma']['hObj'].offHalf_cMSC = (0.01*modifier)+cOff
    tOn = sim.net.cells[i].secs['soma']['mechs']['tMSC']['onHalf']
    sim.net.cells[i].secs['soma']['mechs']['tMSC']['onHalf'] = (1.0+modifier)*tOn
    sim.net.cells[i].secs['soma']['hObj'].onHalf_tMSC = (1.0+modifier)*tOn
    tOff = sim.net.cells[i].secs['soma']['mechs']['tMSC']['offHalf']
    sim.net.cells[i].secs['soma']['mechs']['tMSC']['offHalf'] = (1.0+modifier)*tOff
    sim.net.cells[i].secs['soma']['hObj'].offHalf_tMSC = (1.0+modifier)*tOff

sim.runSimWithIntervalFunc(sim.updateInterval, updateStrains)
#sim.simulate()
sim.analyze()

pops = list(sim.net.pops.keys())
cells = {k: [] for k in pops}
for i in range(len(pops)):
    cells[pops[i]].append(sim.net.pops[pops[i]].cellGids)
    
#    spkID = list(sim.simData.spkid)
#    spkT = list(sim.simData.spkt)
#    allSpikes = []
#    for i in range(len(pops)):
#        if pops[i]=='Noise': continue
#        else:
#            tempCells = np.in1d(spkID,cells[pops[i]])
#            tempTimes = list(compress(spkT,tempCells))
#            for k in range(len(tempTimes)):
#                allSpikes.append(tempTimes[k]/1e3)
#        
#    histData = plt.hist(allSpikes,bins=int(4*30))
#    hist = histData[0]
#    bin_edges = histData[1]
#    [f,pxx] = signal.welch(hist,1/(bin_edges[1]-bin_edges[0]))
#    outputFrequency = f[np.argmax(pxx)]

spikeTxt = outputFilename + '_spikes.txt'
spikeTrains = list()
spikeJson = list()
rate = list()
for i in range(len(sim.net.cells)):
    spikeTrains.append(list())
    spikeJson.append(list())
    logInd = [(x == i) for x in sim.simData['spkid']]
    timeArray = np.array(sim.simData['spkt'])
    theseSpikes = np.array(timeArray[logInd])
    theseSpikes.sort()
    spikeTrains[i] = spk.SpikeTrain(theseSpikes,np.array([0.0, simDur]))
    spikeJson[i] = list(theseSpikes)
    if np.size(theseSpikes) > 0:
        rate.append(1/(np.mean(np.diff(theseSpikes)/1e3)))

#with open(spikeTxt,'w') as f:
#    json.dump(spikeJson,f)

output = dict()
syncStats = sim.analysis.spikes.calculateRate(include=['allCells','asc_cell','desc_cell'])

output['input'] = dict()
output['input']['numNeurons'] = numNeurons
output['input']['simDur'] = simDur
output['input']['nInputs'] = nInputs
output['input']['inputFreq'] = inputFreq
output['input']['inputNoise'] = inputNoise
output['input']['replicate'] = rep

output['syncMeasure'] = sim.analysis.syncMeasure()
output['syncRate'] = (num_asc_cell*syncStats[1][1]+num_desc_cell*syncStats[1][2])/(num_asc_cell+num_desc_cell)
#    output['pspectrumFreq'] = outputFrequency
output['rateMean'] = np.nanmean(rate)
output['rateStd'] = np.nanstd(rate)
output['1-PySpikeISISdist'] = 1-spk.isi_distance(spikeTrains)
output['PySpikeSync'] = spk.spike_sync(spikeTrains)
output['connsPerCell'] = sim.connsPerCell
output['synsPerCell'] = sim.synsPerCell

outputFilename = outputFilename + '_model_measures.json'
with open(outputFilename,'w') as f:
    json.dump(output,f)
