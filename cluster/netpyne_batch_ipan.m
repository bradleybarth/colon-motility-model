addpath('matlab_functions')
%{
try 
    fileID = fopen('compile_ipan.out','r');
    A = [];
    while ~feof(fileID)
        text = fgetl(fileID);
        text(1) = []; text(end) = [];
        parts = split(text,', ');
        line = [];
        for i = 1:length(parts)
            line = [line str2num(parts{i})];
        end
        A = [A; line];
    end
    fclose(fileID);
    completedSets = A;
catch
    completedSets = zeros(1,6);
end
%}
completedSets = [];

%Overview Info
numReps = 10

simDurVal = 90000
simDurSet = length(simDurVal);

inputFreqVal = 6
inputFreqSet = length(inputFreqVal);

inputNoiseVal = 0.4
inputNoiseSet = length(inputNoiseVal);

numNeuronVal = [1000 2000 5000 10000 20000]
numNeuronSet = length(numNeuronVal);

nInputsVal = [1 2 5]
nInputsSet = length(nInputsVal);

%How many sims?
totalSims = numReps*simDurSet*inputFreqSet*inputNoiseSet*numNeuronSet*nInputsSet

if totalSims > 20000
    return
end

%Get Values
% numNeuronVal = GetLog10RangeInt(numNeuronRange,numNeuronSet);
% simDurVal = GetLog10RangeCont(simDurRange,simDurSet);
% inputFreqVal = GetLog10RangeCont(inputFreqRange,inputFreqSet);
% inputNoiseVal = GetLinRangeCont(inputNoiseRange,inputNoiseSet);
% nInputsVal = GetLog10RangeInt(nInputsRange,nInputsSet);

thisScript = 1;
thisSim = 1;
simsOnScript = 0;

jobPre = '#SBATCH --job-name=';
outPre = '#SBATCH --output=/hpc/home/bbb16/netpyne/scripts/';
errPre = '#SBATCH --error=/hpc/home/bbb16/netpyne/scripts/';
cdLine = 'cd /hpc/home/bbb16/netpyne';
preLine = sprintf('./../mod/x86_64/special -nobanner -python cluster_ipan_interneuron_network_allfEPSP.py');

for i = 1:numNeuronSet
    for j = 1:simDurSet
%         complexity = log10(numNeuronVal(i)*simDurVal(j));
%         if complexity <= 6
%             numSimsPerScript = 100;
%         elseif complexity <= 7
%             numSimsPerScript = 5;
%         elseif complexity <= 8
%             numSimsPerScript = 2;
%         else
%             numSimsPerScript = 1;
%         end
        numSimsPerScript = 1;
        for k = 1:inputFreqSet
            for l = 1:inputNoiseSet
                for m = 1:nInputsSet
                    for n = 1:numReps
                        simulationID = [numNeuronVal(i),...
                            simDurVal(j),...
                            inputFreqVal(k),...
                            inputNoiseVal(l),...
                            nInputsVal(m),...
                            (n-1)];
                        
                        if thisSim == 1
                            % need to start a new script
                            % first sim on this script 
                            
                            % copy the template file to the new script
                            fidTemplate = fopen('itertemp.slurm','r');
                            fidNew = fopen(sprintf('scripts/netpynerun%04d.slurm',thisScript),'w');
                            
                            while (~feof(fidTemplate))
                                fprintf(fidNew,'%s',fgets(fidTemplate));
                            end
                            fprintf(fidNew,'\n%s%i_%04d',jobPre,floor(complexity),thisScript);
                            fprintf(fidNew,'\n%snetpynerun%04d.out\n',outPre,thisScript);
                            fprintf(fidNew,'\n%snetpynerun%04d.err\n',errPre,thisScript);
                            fprintf(fidNew,'\n%s\n',cdLine);

                            fclose(fidTemplate);
                        elseif thisSim < numSimsPerScript
                            % add this sim to the current script
                            
                            
                        else    % thisSim >= numSimsPerScript
                            % last sim on this script
                            
                            thisSim = 0; % will increment at the end of this loop
                        end
                        simsOnScript = simsOnScript + 1;
                        fprintf(fidNew,'\n%s %i %g %.2g %.2g %i %i %i\n',preLine,...
                            numNeuronVal(i),...
                            simDurVal(j),...
                            inputFreqVal(k),...
                            inputNoiseVal(l),...
                            nInputsVal(m),...
                            (n-1),...
                            0);
                        
                        if thisSim == 0 % if it is the last sim on this script, run
                            fclose(fidNew);
                            [~,temp] = system(sprintf('sbatch scripts/mech_%04d.slurm',thisScript));
                            disp(sprintf('%d %s',thisScript,temp))
                            disp([thisScript simsOnScript])
                            thisScript = thisScript + 1; % wrote a new script
                            simsOnScript = 0;
                        end
                        
                        thisSim = thisSim + 1; % increment the current sim after writing to script
                    end
                end
            end
        end
    end
end

[~,temp] = system(sprintf('sbatch scripts/netpynerun%04d.slurm',thisScript));
disp(sprintf('%d %s',thisScript,temp))
disp([thisScript simsOnScript])
