close all; clear all

path1 = pwd;
path2 = 'output_data';
filename = 'monte_carlo_CV_0.1.json';
text = fileread(fullfile(path1,path2,filename));
value = jsondecode(text);
clear text

fields = fieldnames(value);
keep =  value.EPSPweight >= 0.000528;
S = struct;
for i = 1:length(fields)
    S.(fields{i}) = value.(fields{i})(keep);
end

fprintf('\nY,X,mean,standard deviation,R^2,p-value\n')
for i = [1 3 4 5 6 7 8 9 10 11 12 13]
    [ax, b0, b1] = sensitivity(S,i,17);
end

fprintf('\nY,X,mean,standard deviation,R^2,p-value\n')
for i = [1 3 6 7 8 9 10 11 12]
    [ax, b0, b1] = sensitivity(S,i,21);
end