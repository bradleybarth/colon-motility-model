addpath('matlab_functions')
close all; clear all
path1 = pwd;
path2 = 'output_data';
filename = '500x_2000_90s_6Hz_0n_2i_mech_0.1CV.json';
text = fileread(fullfile(path1,path2,filename));
value = jsondecode(text);
clear text

figure
histogram(value.rateMean,30)
xlabel('Mean firing rate (Hz)')
ylabel('Trial count')

figure
for i = 1:length(value.numNeurons)
    if value.rateMean(i) > 4.5
        mrk = 'k-';
    elseif value.rateMean(i) < 4.5
        mrk = 'r-';
    end
    plot(value.spkTime{i},value.spkCount{i}./value.numNeurons(i),mrk)
    hold on
end
xlabel('Time (s)')
ystr = sprintf('Spikes per 750ms\nnormalized to number of cells');
ylabel(ystr)
ylim([0 10])
text(2,9.5,'Black: mean firing rate > 4.5','Color','k')
text(2,9,'Red: mean firing rate < 4.5','Color','r')

return

fields = fieldnames(value.projDistance);
for i = 1:length(fields)
    id = strfind(fields{i},'__');
    if length(id) > 1
        endpiece = id(2);
    else
        endpiece = length(fields{i});
    end
        group{i} = fields{i}(1:endpiece);
end
group = unique(group);

outStr = sprintf('%s\t%s\t%s','R','p-value','Field');
disp(outStr)
allX = [];
allY = [];
for m = 1:length(group)
%     figure
%     hold on
    theseFields = find(contains(fields,group{m}));
    for n = 1:length(theseFields)
        j = theseFields(n);
        Y = nan(size(value.rateMean));
        for i = 1:length(value.numNeurons)
            X = value.rateMean(i);
            if X > 4.5, mrk = 'k+';
            elseif X < 4.5, mrk = 'rx';
            end
            if isempty(value.projDistance(i).(fields{j}))
                continue
            end
%             scatter(X*ones(size(value.projDistance(i).(fields{j}))),...
%                 value.projDistance(i).(fields{j}),mrk)
            Y(i) = length(value.projDistance(i).(fields{j}));
%             scatter(X,Y(i),mrk)
            allY = [allY; value.projDistance(i).(fields{j})];
            allX = [allX; X*ones(size(value.projDistance(i).(fields{j})))];
        end
        [R,P] = coefDeter(allY,allX);
        outStr = sprintf('%3g\t%3g\t%s',R(1,2)^2,P(1,2),fields{j});
        disp(outStr)
    end
%     title(group{m})
end



outStr = sprintf('%s\t%s\t%s','R','p-value','Field');
disp(outStr)
allX = [];
allY = [];
fields = fieldnames(value.mechanoProps);
subfields = fieldnames(value.mechanoProps(1).sens_cell);
for m = 1:length(fields)
%     figure
    for n = 1:4
%         subplot(2,2,n)
%         hold on
        for i = 1:length(value.numNeurons)
            X = value.rateMean(i);
            if X > 4.5, mrk = 'k+';
            elseif X < 4.5, mrk = 'rx';
            end
            thisSet = value.mechanoProps(i).(fields{m}).(subfields{n});
%             scatter(X*ones(size(thisSet)),thisSet,mrk)
            allY = [allY; thisSet];
            allX = [allX; X*ones(size(thisSet))];
        end
%         xlabel(subfields{n})
        [R,P] = coefDeter(allY,allX);
        outStr = sprintf('%3g\t%3g\t%s',R(1,2)^2,P(1,2),[fields{m} subfields{n}]);
        disp(outStr)
    end
%     title(fields{m})
end

function [R,P] = coefDeter(X,Y)

X = X(~isnan(Y));
Y = Y(~isnan(Y));

Xscore = (X - nanmean(X))./nanstd(X);
Yscore = (Y - nanmean(Y))./nanstd(Y);

[R,P] = corrcoef(Xscore,Yscore);
end