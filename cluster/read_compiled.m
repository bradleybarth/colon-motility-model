path1 = pwd;
path2 = 'output_data';
filename = 'numNeurons_simDur_inputFreq_inputNoise_nInputs_replicate.json';
text = fileread(fullfile(path1,path2,filename));
value = jsondecode(text);
clear text

numNeurons = unique(value.numNeurons);
simDur = unique(value.simDur);
inputFreq = unique(value.inputFreq);
inputNoise = unique(value.inputNoise);
nInputs = unique(value.nInputs);
replicate = unique(value.replicate);

indices = nan(length(numNeurons),...
    length(simDur),...
    length(inputFreq),...
    length(inputNoise),...
    length(nInputs),...
    length(value.numNeurons));
for i = 1:length(numNeurons)
    for j = 1:length(simDur)
        for k = 1:length(inputFreq)
            for l = 1:length(inputNoise)
                for m = 1:length(nInputs)
                    indices(i,j,k,l,m,:) = logical((value.numNeurons==numNeurons(i)).*...
                        (value.simDur==simDur(j)).*...
                        (value.inputFreq==inputFreq(k)).*...
                        (value.inputNoise==inputNoise(l)).*...
                        (value.nInputs==nInputs(m)));
                end
            end
        end
    end
end
temp = [];
for i = 1:length(numNeurons)
    for j = 1:length(simDur)
        for k = 1:length(inputFreq)
            for l = 1:length(inputNoise)
                for m = 1:length(nInputs)
                    temp = [temp; numNeurons(i) nanmean(value.rateMean(...
                        logical(squeeze(indices(i,j,k,l,m,:)))))];
                end
            end
        end
    end
end

F = [1 2 5 10 20 50];
for f = 1:length(F)
    keep3 = logical(value.inputNoise == 0);
    keep2 = logical(value.inputFreq == F(f));
    keep1 = logical(value.simDur == 6034);
    keep = logical(keep1.*keep2.*keep3);
    clear valueNew
    valueNew.numNeurons = value.numNeurons(keep);
    valueNew.simDur = value.simDur(keep);
    valueNew.inputFreq= value.inputFreq(keep);
    valueNew.inputNoise = value.inputNoise(keep);
    valueNew.nInputs = value.nInputs(keep);
    valueNew.replicate = value.replicate(keep);
    valueNew.syncMeasure = value.syncMeasure(keep);
    valueNew.syncRate = value.syncRate(keep);
    valueNew.rateMean = value.rateMean(keep);
    valueNew.rateStd = value.rateStd(keep);
    valueNew.x1_PySpikeISISdist = value.x1_PySpikeISISdist(keep);
    valueNew.PySpikeSync = value.PySpikeSync(keep);
    valueNew.PySpikeSync = value.connsPerCell(keep);
    valueNew.PySpikeSync = value.PySpikeSync(keep);
    valueNew.connsPerCell = value.connsPerCell(keep);
    valueNew.synsPerCell = value.synsPerCell(keep);
    ax = plotTheseArgsRatio(valueNew,1,5,9);
    str1 = sprintf(['InF = ',num2str(F(f)),'\nInNoise = 0\ndur = 6034']);
    text(0.21,4,str1);
end