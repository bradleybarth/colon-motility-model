addpath('matlab_functions')

% try 
%     fileID = fopen('compile_montecarlo.out','r');
%     A = [];
%     while ~feof(fileID)
%         text = fgetl(fileID);
%         text(1) = []; text(end) = [];
%         parts = split(text,', ');
%         line = [];
%         for i = 1:length(parts)
%             line = [line str2num(parts{i})];
%         end
%         A = [A; line];
%     end
%     fclose(fileID);
%     completedSets = A;
% catch
%     completedSets = zeros(1,6);
% end

% initialize and seed
seedtype = 'default';
seedstart = 1;
rng(seedtype);
rng(seedstart);

%Overview Info
numTrials = 1000;

CV = 0.1;

% ipan firing rate with deformation from doi: 10.1371/journal.pone.0039887
%   25th percentile:    3.10 Hz
%   median:             6.05 Hz
%   75th percentile:    8.86 Hz
%   use random normal distribution with 6.05 Hz mean, 2.88 Hz standard deviation

% random normal
inputFreqMu = 6.05;
inputFreqSt = 2.88;
inputFreqVal = inputFreqMu + inputFreqSt.*randn(numTrials,1);
inputFreqSet = length(inputFreqVal);

ascProjMu = 6e3;
ascProjSt = CV*ascProjMu;
ascProjVal = ascProjMu + ascProjSt.*randn(numTrials,1);
ascProjSet = length(ascProjVal);

descProjMu = 15e3;
descProjSt = CV*descProjMu;
descProjVal = descProjMu + descProjSt.*randn(numTrials,1);
descProjSet = length(descProjVal);

locProjMu = 1e3;
locProjSt = CV*locProjMu;
locProjVal = locProjMu + locProjSt.*randn(numTrials,1);
locProjSet = length(locProjVal);

fEPSPMu = 0.0006;
fEPSPSt = CV*fEPSPMu;
fEPSPVal = fEPSPMu + fEPSPSt.*randn(numTrials,1);
fEPSPSet = length(fEPSPVal);

sEPSPMu = 0.00008;
sEPSPSt = CV*sEPSPMu;
sEPSPVal = sEPSPMu + sEPSPSt.*randn(numTrials,1);
sEPSPSet = length(sEPSPVal);

farConnMaxMu = 6;
farConnMaxSt = CV*farConnMaxMu;
farConnMaxVal = farConnMaxMu + farConnMaxSt.*randn(numTrials,1);
farConnMaxSet = length(farConnMaxVal);

farConnMinMu = 3;
farConnMinSt = CV*farConnMinMu;
farConnMinVal = farConnMinMu + farConnMinSt.*randn(numTrials,1);
farConnMinSet = length(farConnMinVal);

localConnMaxMu = 4;
localConnMaxSt = CV*localConnMaxMu;
localConnMaxVal = localConnMaxMu + localConnMaxSt.*randn(numTrials,1);
localConnMaxSet = length(localConnMaxVal);

localConnMinMu = 1;
localConnMinSt = CV*localConnMinMu;
localConnMinVal = localConnMinMu + localConnMinSt.*randn(numTrials,1);
localConnMinSet = length(localConnMinVal);

numNeuronMu = 5000;
numNeuronSt = CV*numNeuronMu;
numNeuronVal = numNeuronMu + numNeuronSt.*randn(numTrials,1);
numNeuronSet = length(numNeuronVal);

numInputMu = 5;
numInputSt = CV*numInputMu;
numInputVal = numInputMu + numInputSt.*randn(numTrials,1);
numInputSet = length(numInputVal);

stimStartMu = 100;
stimStartSt = CV*stimStartMu;
stimStartVal = stimStartMu + stimStartSt.*randn(numTrials,1);
stimStartSet = length(stimStartVal);

stimStopMu = 15000;
stimStopSt = CV*stimStopMu;
stimStopVal = stimStopMu + stimStopSt.*randn(numTrials,1);
stimStopSet = length(stimStopVal);

% random uniform
inputNoiseMin = 0;
inputNoiseMax = 1;
inputNoiseVal = inputNoiseMin + (inputNoiseMax-inputNoiseMin).*rand(numTrials,1);
inputNoiseSet = length(inputNoiseVal);

%How many sims?
if numTrials > 20000
    return
end

argJson = struct;
argJson.numNeurons = round(numNeuronVal);
argJson.nInputs = round(numInputVal);
argJson.inputFreq = inputFreqVal;
argJson.inputNoise = inputNoiseVal;
argJson.ascProjDist = ascProjVal;
argJson.descProjDist = descProjVal;
argJson.locProjDist = locProjVal;
argJson.farConnMax = farConnMaxVal;
argJson.farConnMin = farConnMinVal;
argJson.locConnMax = localConnMaxVal;
argJson.locConnMin = localConnMinVal;
argJson.fEPSPweight = fEPSPVal;
argJson.sEPSPweight = sEPSPVal;
argJson.stimStart = stimStartVal;
argJson.stimStop = stimStopVal;

path1 = 'scripts';
argFile = 'ipan_montecarlo_arg.json';
txt = jsonencode(argJson);
fid = fopen(fullfile(path1,argFile),'w');
if fid == -1, error('Cannot create JSON file'); end
fwrite(fid, txt, 'char');
fclose(fid);

thisScript = 1;
thisSim = 1;
simsOnScript = 0;

jobPre = '#SBATCH --job-name=';
outPre = '#SBATCH --output=/hpc/home/bbb16/netpyne/scripts/';
errPre = '#SBATCH --error=/hpc/home/bbb16/netpyne/scripts/';
cdLine = 'cd /hpc/home/bbb16/netpyne';
preLine = sprintf('./../mod/x86_64/special -nobanner -python cluster_ipan_interneuron_network_sensitivity.py');

complexity = 0;
for i = 1:numTrials
    numSimsPerScript = 10;

%     simulationID = [numNeuronVal(i),...
%         simDurVal(j),...
%         inputFreqVal(k),...
%         inputNoiseVal(l),...
%         nInputsVal(m),...
%         (n-1)];
%     if ismember(simulationID,completedSets,'rows')
%         disp(['This sim already compiled: ',num2str(simulationID)])
%         continue
%     end

    if thisSim == 1
        % need to start a new script
        % first sim on this script 

        % copy the template file to the new script
        fidTemplate = fopen('itertemp.slurm','r');
        fidNew = fopen(sprintf('scripts/netpynerun%04d.slurm',thisScript),'w');

        while (~feof(fidTemplate))
            fprintf(fidNew,'%s',fgets(fidTemplate));
        end
        fprintf(fidNew,'\n%s%i_%04d',jobPre,floor(complexity),thisScript);
        fprintf(fidNew,'\n%snetpynerun%04d.out\n',outPre,thisScript);
        fprintf(fidNew,'\n%snetpynerun%04d.err\n',errPre,thisScript);
        fprintf(fidNew,'\n%s\n',cdLine);

        fclose(fidTemplate);
    elseif thisSim < numSimsPerScript
        % add this sim to the current script


    else    % thisSim >= numSimsPerScript
        % last sim on this script

        thisSim = 0; % will increment at the end of this loop
    end
    simsOnScript = simsOnScript + 1;
    fprintf(fidNew,'%s %s %i\n',preLine,...
        argFile,...
        (i-1));
    
 



    if thisSim == 0 % if it is the last sim on this script, run
        fclose(fidNew);
        [~,temp] = system(sprintf('sbatch scripts/netpynerun%04d.slurm',thisScript));
        disp(sprintf('%d %s',thisScript,temp))
        disp([thisScript simsOnScript])
        thisScript = thisScript + 1; % wrote a new script
        simsOnScript = 0;
    end

    thisSim = thisSim + 1; % increment the current sim after writing to script
end

try
    fclose(fidNew);
end
[~,temp] = system(sprintf('sbatch scripts/netpynerun%04d.slurm',thisScript));
disp(sprintf('%d %s',thisScript,temp))
disp([thisScript simsOnScript])
