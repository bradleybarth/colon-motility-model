addpath('matlab_functions')

% try 
%     fileID = fopen('compile_montecarlo.out','r');
%     A = [];
%     while ~feof(fileID)
%         text = fgetl(fileID);
%         text(1) = []; text(end) = [];
%         parts = split(text,', ');
%         line = [];
%         for i = 1:length(parts)
%             line = [line str2num(parts{i})];
%         end
%         A = [A; line];
%     end
%     fclose(fileID);
%     completedSets = A;
% catch
%     completedSets = zeros(1,6);
% end

% initialize and seed
seedtype = 'default';
seedstart = 1;
rng(seedtype);
rng(seedstart);

%Overview Info
numTrials = 1000;

% ipan firing rate with deformation from doi: 10.1371/journal.pone.0039887
%   25th percentile:    3.10 Hz
%   median:             6.05 Hz
%   75th percentile:    8.86 Hz
%   use random normal distribution with 6.05 Hz mean, 2.88 Hz standard deviation

% random normal
inputFreqMu = 6.05;
inputFreqSt = 2.88;
inputFreqVal = inputFreqMu + inputFreqSt.*randn(numTrials,1);
inputFreqSet = length(inputFreqVal);

ascProjMu = 6e3;
ascProjSt = 2e3;
ascProjVal = ascProjMu + ascProjSt.*randn(numTrials,1);
ascProjSet = length(ascProjVal);

descProjMu = 15e3;
descProjSt = 5e3;
descProjVal = descProjMu + descProjSt.*randn(numTrials,1);
descProjSet = length(descProjVal);

locProjMu = 1e3;
locProjSt = 0.5e3;
locProjVal = locProjMu + locProjSt.*randn(numTrials,1);
locProjSet = length(locProjVal);

EPSPMu = 0.0006;
EPSPSt = 0.0002;
EPSPVal = EPSPMu + EPSPSt.*randn(numTrials,1);
EPSPSet = length(EPSPVal);

farConnMaxMu = 6;
farConnMaxSt = 2;
farConnMaxVal = farConnMaxMu + farConnMaxSt.*randn(numTrials,1);
farConnMaxSet = length(farConnMaxVal);

farConnMinMu = 3;
farConnMinSt = 2;
farConnMinVal = farConnMinMu + farConnMinSt.*randn(numTrials,1);
farConnMinSet = length(farConnMinVal);

localConnMaxMu = 4;
localConnMaxSt = 2;
localConnMaxVal = localConnMaxMu + localConnMaxSt.*randn(numTrials,1);
localConnMaxSet = length(localConnMaxVal);

localConnMinMu = 1;
localConnMinSt = 1;
localConnMinVal = localConnMinMu + localConnMinSt.*randn(numTrials,1);
localConnMinSet = length(localConnMinVal);




% random uniform
inputNoiseMin = 0;
inputNoiseMax = 1;
inputNoiseVal = inputNoiseMin + (inputNoiseMax-inputNoiseMin).*rand(numTrials,1);
inputNoiseSet = length(inputNoiseVal);

numNeuronMin = log10(50000);
numNeuronMax = log10(100);
numNeuronVal = numNeuronMin + (numNeuronMax-numNeuronMin).*rand(numTrials,1);
numNeuronSet = length(numNeuronVal);

numInputMin = log10(5);
numInputMax = log10(10000);
numInputVal = numInputMin + (numInputMax-numInputMin).*rand(numTrials,1);
numInputSet = length(numInputVal);



%How many sims?
if numTrials > 20000
    return
end

thisScript = 1;
thisSim = 1;
simsOnScript = 0;

jobPre = '#SBATCH --job-name=';
outPre = '#SBATCH --output=/hpc/home/bbb16/netpyne/scripts/';
errPre = '#SBATCH --error=/hpc/home/bbb16/netpyne/scripts/';
cdLine = 'cd /hpc/home/bbb16/netpyne';
preLine = sprintf('./../mod/x86_64/special -nobanner -python cluster_interneuron_network_sensitivity.py');

complexity = 0;
for i = 1:numTrials
    numSimsPerScript = 10;

%     simulationID = [numNeuronVal(i),...
%         simDurVal(j),...
%         inputFreqVal(k),...
%         inputNoiseVal(l),...
%         nInputsVal(m),...
%         (n-1)];
%     if ismember(simulationID,completedSets,'rows')
%         disp(['This sim already compiled: ',num2str(simulationID)])
%         continue
%     end

    if thisSim == 1
        % need to start a new script
        % first sim on this script 

        % copy the template file to the new script
        fidTemplate = fopen('itertemp.slurm','r');
        fidNew = fopen(sprintf('scripts/netpynerun%04d.slurm',thisScript),'w');

        while (~feof(fidTemplate))
            fprintf(fidNew,'%s',fgets(fidTemplate));
        end
        fprintf(fidNew,'\n%s%i_%04d',jobPre,floor(complexity),thisScript);
        fprintf(fidNew,'\n%snetpynerun%04d.out\n',outPre,thisScript);
        fprintf(fidNew,'\n%snetpynerun%04d.err\n',errPre,thisScript);
        fprintf(fidNew,'\n%s\n',cdLine);

        fclose(fidTemplate);
    elseif thisSim < numSimsPerScript
        % add this sim to the current script


    else    % thisSim >= numSimsPerScript
        % last sim on this script

        thisSim = 0; % will increment at the end of this loop
    end
    simsOnScript = simsOnScript + 1;
    fprintf(fidNew,'\n%s %i %i %.2g %.2g %.f %.f %.f %.2f %.2f %.2f %.2f %.7f %i %s %i\n',preLine,...
        round(10^(numNeuronVal(i))),...
        round(10^(numInputVal(i))),...
        inputFreqVal(i),...
        inputNoiseVal(i),...
        ascProjVal(i),...
        descProjVal(i),...
        locProjVal(i),...
        farConnMaxVal(i),...
        farConnMinVal(i),...
        localConnMaxVal(i),...
        localConnMinVal(i),...
        EPSPVal(i),...
        (i-1),...
        seedtype,...
        seedstart);
    
 



    if thisSim == 0 % if it is the last sim on this script, run
        fclose(fidNew);
        [~,temp] = system(sprintf('sbatch scripts/netpynerun%04d.slurm',thisScript));
        disp(sprintf('%d %s',thisScript,temp))
        disp([thisScript simsOnScript])
        thisScript = thisScript + 1; % wrote a new script
        simsOnScript = 0;
    end

    thisSim = thisSim + 1; % increment the current sim after writing to script
end

try
    fclose(fidNew);
end
[~,temp] = system(sprintf('sbatch scripts/netpynerun%04d.slurm',thisScript));
disp(sprintf('%d %s',thisScript,temp))
disp([thisScript simsOnScript])
