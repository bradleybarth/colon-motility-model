close all; clear all;

path1 = pwd;
path2 = 'output_data';
filename = 'numNeurons_simDur_inputFreq_inputNoise_nInputs_replicate.json';
text = fileread(fullfile(path1,path2,filename));
value = jsondecode(text);
clear text

inputFreq = unique(value.inputFreq);
ratio = unique(value.numNeurons./value.nInputs);
allFreq = nan(length(inputFreq),length(ratio));
allRatio = nan(length(inputFreq),length(ratio));
allRates = nan(length(inputFreq),length(ratio));
color = nan(length(inputFreq),length(ratio),3);

%% OUTPUT FREQUENCY x POPULATION RATIO for each INPUT FREQUENCY
figure
axPop = subplot(1,1,1);
hold on
noiseVal = logical(value.inputNoise == 0);
simDuVal = logical(value.simDur == 6034);
ratio = unique(value.numNeurons./value.nInputs);
rates = zeros(size(ratio));
iF = [1 2 5 6 7 8.5 10 12 14 17 20 25 32 40 50 100 200 500];
leg = cell(size(iF));
n = zeros(length(iF),length(ratio));
clr = round(linspace(1,256,length(iF)));
clr2 = round(linspace(1,256,length(ratio)));
cmap = colormap;

legendTicks = [1 5 10 20 50];
legendLabels = cell(length(legendTicks),1);
legendTicks = interp1(iF,clr,legendTicks);
for i = 1:length(legendTicks)
    [~,id] = min(abs(clr-legendTicks(i)));
    legendLabels{i} = sprintf('%.0f',iF(id));
end
for i = 1:length(iF)
    iFreqVal = logical(value.inputFreq == iF(i)); 
    for j = 1:length(ratio)
        ratioVal = logical((value.numNeurons./value.nInputs) == ratio(j));
        keep = logical(noiseVal.*simDuVal.*iFreqVal.*ratioVal);
        n(i,j) = sum(keep);
        rates(j) = nanmean(value.rateMean(keep));
        allFreq(i,j) = iF(i);
        allRatio(i,j) = ratio(j);
        allRates(i,j) = rates(j);
        color(i,j,:) = cmap(clr2(j),:);
    end
    scatter(ratio,rates,[],cmap(clr(i),:),'filled')
%     [iFclr(i),'o'])
    leg{i} = [num2str(iF(i)),' Hz Input'];
end
% legend(leg)
set(axPop,'XScale','log')
set(axPop,'YScale','log')
c = colorbar;
caxis([0 256])
c.Ticks = legendTicks;
c.TickLabels = legendLabels;
c.Label.String = 'Input frequency (Hz)';
xlabel('# Neurons : # Inputs')
ylabel('Mean firing rate (Hz)')
ylim([0.5 100])
grid on
hold off


%% OUTPUT FREQUENCY x INPUT FREQUENCY at distinct POPULATION RATIOs
figure
axFreq = subplot(1,1,1);
hold on
theseRatios = ratio(sum(n,1)>=150);
% theseRatios = [min(theseRatios)...
%     quantile(theseRatios,0.25)...
%     median(theseRatios)...
%     quantile(theseRatios,0.75)...
%     max(theseRatios)];

% theseRatios = theseRatios((end-5):end);

iF = [1 2 5 6 7 8.5 10 12 14 17 20 25 32 40 50 100 200 500];
rates = zeros(size(iF));
errs = zeros(size(iF));
Ratioclr = 'kcrbgy';
leg = cell(size(theseRatios));
clr = round(linspace(1,256,length(theseRatios)));
legendTicks = 0:4;
legendLabels = cell(length(legendTicks),1);
legendTicks = interp1(theseRatios,clr,10.^legendTicks);
for i = 1:length(legendTicks)
    [~,id] = min(abs(clr-legendTicks(i)));
    legendLabels{i} = sprintf('10^%.1g',log10(theseRatios(id)));
    if abs(theseRatios(id)) < 1
        legendLabels{i} = sprintf('%s^-^0^.^%i',...
            legendLabels{i}(1:end-5),...
            str2double(legendLabels{i}(end)));
%         legendLabels{i} = sprintf('10^-^%ig',abs(log10(theseRatios(id))));
    end
end
for i = 1:length(theseRatios)
    ratioVal = logical((value.numNeurons./value.nInputs) == theseRatios(i));
    for j = 1:length(iF)
        iFreqVal = logical(value.inputFreq == iF(j));
        keep = logical(noiseVal.*simDuVal.*iFreqVal.*ratioVal);
        rates(j) = nanmean(value.rateMean(keep));
        errs(j) = nanstd(value.rateMean(keep));
    end
%     plot(iF,rates,[Ratioclr(i),'-o'])
    e = errorbar(iF,rates,errs);%r[Ratioclr(i),'-o'])
    e.Marker = 'o';
    e.Color = cmap(clr(i),:);
    leg{i} = [num2str(round(theseRatios(i),1)),' Neurons : Inputs'];
end
% legend(leg,'Location','southeast')
% set(axFreq,'XScale','log')
% set(axFreq,'YScale','log')
c = colorbar;
caxis([0 256])
c.Ticks = legendTicks;
c.TickLabels = legendLabels;
c.Label.String = '# Neurons : # Inputs';
c.Direction = 'reverse';
xlabel('Input firing rate (Hz)')
ylabel('Mean firing rate (Hz)')
axis([0 500 0 500])
set(axFreq,'XTick',0:5:500)
grid on
hold off

%% OUTPUT FREQUENCY x INPUT FREQUENCY at distinct POPULATION RATIOs
figure
axFreq2 = subplot(1,1,1);
hold on
theseRatios = ratio(sum(n,1)>=150);
% theseRatios = [min(theseRatios)...
%     quantile(theseRatios,0.25)...
%     median(theseRatios)...
%     quantile(theseRatios,0.75)...
%     max(theseRatios)];

theseRatios = theseRatios((end-5):end);

iF = [1 2 5 6 7 8.5 10 12 14 17 20 25 32 40 50];% 100 200 500];
rates = zeros(size(iF));
errs = zeros(size(iF));
Ratioclr = 'kcrbgy';
leg = cell(size(theseRatios));
clr = round(linspace(1,256,length(theseRatios)));
for i = 1:length(theseRatios)
    ratioVal = logical((value.numNeurons./value.nInputs) == theseRatios(i));
    for j = 1:length(iF)
        iFreqVal = logical(value.inputFreq == iF(j));
        keep = logical(noiseVal.*simDuVal.*iFreqVal.*ratioVal);
        rates(j) = nanmean(value.rateMean(keep));
        errs(j) = nanstd(value.rateMean(keep));
    end
%     plot(iF,rates,[Ratioclr(i),'-o'])
    e = errorbar(iF,rates,errs);%r[Ratioclr(i),'-o'])
    e.Marker = 'o';
    e.Color = 'k';%cmap(clr(i),:);
    leg{i} = [num2str(round(theseRatios(i),1)),' Neurons : Inputs'];
end
% legend(leg,'Location','southeast')
set(axFreq2,'XScale','log')
% set(axFreq,'YScale','log')
% c = colorbar;
% caxis([0 256])
% c.Ticks = legendTicks;
% c.TickLabels = legendLabels;
% c.Label.String = '# Neurons : # Inputs';
% c.Direction = 'reverse';
xlabel('Input firing rate (Hz)')
ylabel('Mean firing rate (Hz)')
% axis([0 50 0 10])
ylim([0 15]);
% set(axFreq2,'XTick',0:5:500)
grid on
hold off

return
%% OUTPUT FREQUENCY x INPUT FREQUENCY x POPULATION RATIOs [3D scatter plot]
figure
ax3d = subplot(1,1,1);
scatter3(allFreq(:),allRatio(:),allRates(:),[],reshape(color,length(allFreq(:)),3),'filled')
set(ax3d,'YDir','reverse')
set(ax3d,'XScale','log')
set(ax3d,'YScale','log')
set(ax3d,'ZScale','log')
axis([0.9 550 0.1 10^5 1 100])
ax3d.Projection = "perspective";
set(gcf,'color','w')
az = -90;
el = 0;
view([az,el])

degStep = 1;
detlaT = 0.0005;
fCount = 180;
f = getframe(gcf);
[im,map] = rgb2ind(f.cdata,256,'nodither');
im(1,1,1,fCount) = 0;
k = 1;
% spin 30� right
for i = [-90:degStep:0 -1:-degStep:-89]
    az = i;
    view([az,el])
    f = getframe(gcf);
    im(:,:,1,k) = rgb2ind(f.cdata,map,'nodither');
    k = k + 1;
end

imwrite(im,map,'Animation.gif','DelayTime',detlaT,'LoopCount',inf)