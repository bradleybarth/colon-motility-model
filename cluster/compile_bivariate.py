# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 09:21:19 2020

@author: Bradley Barth
"""

import sys
import json
import os

argvLen = len(sys.argv)

path1 = os.getcwd()
path2 = 'output_data'
#path3 = sys.argv[argvLen-1]
path3 = 'inputFreq_inputNoise_projMod_EPSPMod_connMod'

newPath = os.path.join(path1,path2,path3)

allJson = [f for f in os.listdir(newPath) if os.path.isfile(os.path.join(newPath,f)) and os.path.splitext(f)[1]=='.json']

outputFile = os.path.join(path1,path2,path3+'.json')

if os.path.isfile(outputFile):
    with open(outputFile,'r') as f:
        outputJson = json.load(f)
else:
    outputJson = dict()
    outputJson['numNeurons'] = list()
    outputJson['simDur'] = list()
    outputJson['inputFreq'] = list()
    outputJson['inputNoise'] = list()
    outputJson['nInputs'] = list()
    outputJson['replicate'] = list()
    outputJson['projMod'] = list()
    outputJson['EPSPMod'] = list()
    outputJson['connMod'] = list()
    outputJson['syncMeasure'] = list()
    outputJson['syncRate'] = list()
    outputJson['rateMean'] = list()
    outputJson['rateStd'] = list()
    outputJson['1-PySpikeISISdist'] = list()
    outputJson['PySpikeSync'] = list()
    outputJson['connsPerCell'] = list()
    outputJson['synsPerCell'] = list()
    
    
jsonParams = list(outputJson.keys())
inputLabel = ['inputFreq','inputNoise','projMod','EPSPMod','connMod','replicate']
inputParams = ['numNeurons','simDur','inputFreq','inputNoise','nInputs','replicate','projMod','EPSPMod','connMod']
check = list()
for i in range(len(outputJson['numNeurons'])):
    check.append(list())
    for j in range(len(inputLabel)):
        check[i].append(outputJson[inputLabel[j]][i])
    print(check[i])

for i in range(len(allJson)):    
    if allJson[i]:
        with open(os.path.join(newPath,allJson[i]),'r') as f:
            DS = json.load(f)
        
        if 'input' in list(DS.keys()):          
            outputParams = list(DS.keys())
            outputParams.remove('input')
            test = list()
            for k in range(len(inputLabel)):
                if inputLabel[k] in list(outputJson.keys()):
                    test.append(DS['input'][inputLabel[k]])
            
            if test not in check:            
                for k in range(len(inputParams)):
                    if inputParams[k] in list(outputJson.keys()):
                        outputJson[inputParams[k]].append(DS['input'][inputParams[k]])
                for k in range(len(outputParams)):
                    if outputParams[k] in list(outputJson.keys()):
                        outputJson[outputParams[k]].append(DS[outputParams[k]])
            
                lengthVal = [len(outputJson[f]) for f in list(outputJson.keys())]
                while len(set(lengthVal)) > 1:
                    longList = lengthVal.index(max(lengthVal))
                    del(outputJson[jsonParams[longList]][-1])
                    lengthVal = [len(outputJson[f]) for f in list(outputJson.keys())]
#            else:
#                print(test)
            
outputFilename = os.path.join(path1,path2,(path3+'.json'))
with open(outputFilename,'w') as f:
    json.dump(outputJson,f)
