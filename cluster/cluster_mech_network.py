# -*- coding: utf-8 -*-
"""
Created on Mon Oct 14 15:33:56 2019

@author: Bradley Barth
"""
from netpyne import specs, sim
import numpy as np
import json
import pyspike as spk
import sys
import os
import matplotlib.pyplot as plt
from scipy import signal
from itertools import compress, product

#argvLen = len(sys.argv)

path1 = os.getcwd()
path2 = 'scripts'

argvLen = len(sys.argv)

numNeurons = int(sys.argv[argvLen-6])
simDur = int(sys.argv[argvLen-5])
inputFreq = float(sys.argv[argvLen-4])
inputNoise = float(sys.argv[argvLen-3])
nInputs = int(sys.argv[argvLen-2])
rep = int(sys.argv[argvLen-1])

ascProjDist = 6e3
descProjDist = 15e3
locProjDist = 1e3
farConnMax = 3
farConnMin = 1
locConnMax = 2
locConnMin = 1
fEPSPweight = 0.0006
sEPSPweight = 0.00008
fIPSPweight = 0.004
sIPSPweight = 0.002
stimStart = 100
stimStop = 5000

seedtype = 'default'
seedstart = 1

saveOutput = False

path = os.getcwd()    
filename = str(numNeurons).zfill(6) + '_' + str(int((simDur)/1e3)).zfill(4) + '_' + str(inputFreq).zfill(6) + '_' + str(inputNoise).zfill(5) + '_'  + str(int(nInputs)).zfill(5) + '_' + str(rep).zfill(4)
filepath1 = 'output_data'
filepath2 = 'mech_numNeurons_simDur_inputFreq_inputNoise_nInputs_replicate'
outputFilename = os.path.join(path,filepath1,filepath2,filename)


# Network parameters
netParams = specs.NetParams()

### References & Assumptions
    ### 28% of MEN are AH neurons | doi: 10.1371/journal.pone.0039887
    ### 15% of neurons are MEN | doi: 10.1371/journal.pone.0039887
    ### 7% of MEN are SAMEN | doi: 10.1371/journal.pone.0039887
    
    ### All AH cells that are MEN are RAMEN
    ### All MEN are either SAMEN or RAMEN (there are no RAMEN)
        ### Therefore 93% of MEN are RAMEN
    
    ### Assume 50:50 split of interneuron MEN between ascending and descending

    ### Proportion of myenteric neurons from p. 32, Furness 2006 [guinea-pig small intestine]
        ### C-EMN       12%
        ### C-IMN       16%
        ### L-EMN       25%
        ### L-IMN       2%
        ### A-INT       5%
        ### D-INT       11%
            ### ChAT/NOS    5%      local reflex
            ### ChAT/5HT    2%      secretomotor/motility reflex
            ### ChAT/SOM    4%      migrating motor complex
        ### IPAN        26%

    ### Of all S-type cells (and MEN):
        ### 16% are C-EMN
        ### 21% are C-IMN
        ### 38% are L-EMN
        ### 3% are L-IMN
        ### 7% are A-INT
        ### 7% are D-INT/NOS
        ### 3% are D-INT/5HT
        ### 5% are D-INT/SOM


num_ramen = np.int(0.15*numNeurons)
num_men = np.int(num_ramen/0.93)
num_samen = num_men - num_ramen

num_ah_ramen = np.int(0.28*num_men)
num_ah_samen = np.int(0)

num_s_ramen = num_ramen - num_ah_ramen
num_s_samen = num_samen

num_sens_men = num_ah_ramen
num_asc_ramen = np.int(0.07*num_s_ramen)
num_asc_samen = np.int(0.07*num_s_samen)
num_desc_ramen = np.int(0.15*num_s_ramen)
num_desc_samen = np.int(0.15*num_s_samen)

num_cemn_samen = np.int(0.16*num_s_samen)
num_cemn_ramen = np.int(0.16*num_s_ramen)
num_cimn_samen = np.int(0.21*num_s_samen)
num_cimn_ramen = np.int(0.21*num_s_ramen)

num_lemn_samen = np.int(0.38*num_s_samen)
num_lemn_ramen = np.int(0.38*num_s_ramen)
num_limn_samen = np.int(0.03*num_s_samen)
num_limn_ramen = np.int(0.03*num_s_ramen)


num_sens_cell = np.int(0.26*numNeurons) - num_sens_men
num_asc_cell = np.int(0.05*numNeurons) - (num_asc_ramen + num_asc_samen)

num_desc_5HT = np.int(0.02*numNeurons - 0.18*(num_desc_ramen + num_desc_samen))
num_desc_5HT_ramen = np.int(0.18*num_desc_ramen)
num_desc_5HT_samen = np.int(0.18*num_desc_samen)

num_desc_NOS = np.int(0.05*numNeurons - 0.45*(num_desc_ramen + num_desc_samen))
num_desc_NOS_ramen = np.int(0.45*num_desc_ramen)
num_desc_NOS_samen = np.int(0.45*num_desc_samen)

num_desc_MMC = np.int(0.04*numNeurons - 0.37*(num_desc_ramen + num_desc_samen))
num_desc_MMC_ramen = np.int(0.37*num_desc_ramen)
num_desc_MMC_samen = np.int(0.37*num_desc_samen)

num_desc_cell = num_desc_NOS + num_desc_5HT + num_desc_MMC

num_cemn_cell = np.int(0.12*numNeurons) - (num_cemn_ramen + num_cemn_samen)
num_cimn_cell = np.int(0.16*numNeurons) - (num_cimn_ramen + num_cimn_samen)

num_lemn_cell = np.int(0.25*numNeurons) - (num_lemn_ramen + num_lemn_samen)
num_limn_cell = np.int(0.02*numNeurons) - (num_limn_ramen + num_limn_samen)

print('Group\tRAMEN\tSAMEN\tnon-MEN')
print('Sens\t'+str(num_ah_ramen)+'\t'+str(num_ah_samen)+'\t'+str(num_sens_cell))
print('Asc\t'+str(num_asc_ramen)+'\t'+str(num_asc_samen)+'\t'+str(num_asc_cell))
print('Desc\t'+str(num_desc_ramen)+'\t'+str(num_desc_samen)+'\t'+str(num_desc_NOS+num_desc_5HT+num_desc_MMC))
print('EMN\t'+str(num_cemn_ramen+num_lemn_ramen)+'\t'+str(num_cemn_samen+num_lemn_samen)+'\t'+str(num_cemn_cell+num_lemn_cell))
print('IMN\t'+str(num_cimn_ramen+num_limn_ramen)+'\t'+str(num_cimn_samen+num_limn_samen)+'\t'+str(num_cimn_cell+num_limn_cell))

netParams.sizeX = 150*1e3
netParams.sizeY = 100
netParams.sizeZ = 0

### Population parameters
netParams.importCellParams(label='S_cell_rule', conds= {'cellType': ['S_cell']},
        fileName='s_template.py', cellName='s_cell')
netParams.importCellParams(label='S_samen_rule', conds= {'cellType': ['S_samen']},
        fileName='s_template.py', cellName='s_samen')
netParams.importCellParams(label='S_ramen_rule', conds= {'cellType': ['S_ramen']},
        fileName='s_template.py', cellName='s_ramen')

netParams.importCellParams(label='AH_cell_rule', conds= {'cellType': ['AH_cell']},
        fileName='ah_template.py', cellName='ah_cell')
netParams.importCellParams(label='AH_men_rule', conds= {'cellType': ['AH_men']},
        fileName='ah_template.py', cellName='ah_ramen')

allPops = ['sens_cell','sens_ramen',
           'asc_cell','asc_ramen','asc_samen',
           '5HTIN_cell','5HTIN_ramen','5HTIN_samen',
           'NOSIN_cell','NOSIN_ramen','NOSIN_samen']
cellTypes = ['AH_cell','AH_men',
             'S_cell','S_ramen','S_samen',
             'S_cell','S_ramen','S_samen',
             'S_cell','S_ramen','S_samen']
numCells = [num_sens_cell,num_sens_men,
            num_asc_cell,num_asc_ramen,num_asc_samen,
            num_desc_5HT,num_desc_5HT_ramen,num_desc_5HT_samen,
            num_desc_NOS,num_desc_NOS_ramen,num_desc_NOS_samen]
XList = dict.fromkeys(allPops,[])
CellList = dict.fromkeys(allPops,[])
for p in range(len(allPops)):
    xind = list()
    cind = list()
    for i in range(numCells[p]):
        x = np.random.gumbel(0.25*netParams.sizeX,0.50*netParams.sizeX)
        while x > netParams.sizeX or x < 0:
            x = np.random.gumbel(0.25*netParams.sizeX,0.50*netParams.sizeX)
        y = np.random.uniform(0,netParams.sizeY/2)
        xind.append(x)
        cind.append({'cellLabel': allPops[p]+str(i), 'x': x, 'y': y})
    XList[allPops[p]] = xind
    CellList[allPops[p]] = cind
    netParams.popParams[allPops[p]] = {'cellType': cellTypes[p], 'cellsList': cind}

sens = ['sens_cell','sens_ramen']
asc = ['asc_cell','asc_ramen','asc_samen']
d5HT = ['5HTIN_cell','5HTIN_ramen','5HTIN_samen']
dNOS = ['NOSIN_cell','NOSIN_ramen','NOSIN_samen']

pops = list(netParams.popParams.keys())
total_cells = 0
for i in range(len(pops)):
    total_cells += len(netParams.popParams[pops[i]]['cellsList'])

### Apply tension to all populations
amp = 0.2
rate = 0.001

### Compression driver
#comp_delay = 2e3
#comp_dur = 4e3
#comp_amp = amp
#comp_rate = rate #(elongation per ms)

### Tension driver
tens_delay = stimStart
tens_dur = stimStop
tens_amp = amp
tens_rate = rate #(elongation per ms)
tens = []

def updateStrains(t):
    if (t==sim.updateInterval):
        sim.lastCompression = 0
        sim.lastTension = 0
    # Update compression
#    compFlat = comp_amp/comp_rate + comp_delay
#    compOff = comp_delay + comp_dur
#    if t < comp_delay:
#        thisCompression = 0
#    if t >= comp_delay and t < compFlat:
#        thisCompression = (t-comp_delay)*comp_rate
#    if t >= compFlat and t < comp_dur + comp_delay:
#        thisCompression = comp_amp
#    if t >= comp_dur + comp_delay and t < compOff:
#        thisCompression = comp_amp - (t - (comp_dur+comp_delay))*comp_rate
#    if t >= compOff:
#        thisCompression = 0
    
    # Update tension
    tensFlat = tens_amp/tens_rate + tens_delay
    tensOff = tens_delay + tens_dur
    if t < tens_delay:
        thisTension = 0
    if t >= tens_delay and t < tensFlat:
        thisTension = (t-tens_delay)*tens_rate
    if t >= tensFlat and t < tens_dur + tens_delay:
        thisTension = tens_amp
    if t >= tens_dur + tens_delay and t < tensOff:
        thisTension = tens_amp - (t - (tens_dur+tens_delay))*tens_rate
    if t >= tensOff:
        thisTension = 0
    dTension = abs(thisTension-sim.lastTension)/sim.updateInterval
        
    # Set values
    for j in range(len(pops)):
        cellsInPop = len(sim.net.pops[pops[j]].cellGids)
        for i in range(cellsInPop):
            thisCell = sim.net.pops[pops[j]].cellGids[i]
#            comp[i].append(((i+1)/(numNeurons))*thisCompression)
#            sim.net.cells[thisCell].secs['soma']['mechs']['cMSC']['compression'] = (i+1)/(numNeurons)*thisCompression
#            sim.net.cells[thisCell].secs['soma']['hObj'].compression_cMSC = (i+1)/(numNeurons)*thisCompression
            tens.append(thisTension)
            sim.net.cells[thisCell].secs['soma']['mechs']['tMSC']['tension'] = thisTension
            sim.net.cells[thisCell].secs['soma']['hObj'].tension_tMSC = thisTension
            sim.net.cells[thisCell].secs['soma']['mechs']['tMSC']['dtension'] = dTension
            sim.net.cells[thisCell].secs['soma']['hObj'].dtension_tMSC = dTension
            
    sim.lastTension = thisTension
    return sim


### Synaptic mechanisms
netParams.synMechParams['fEPSP'] = {'mod': 'Exp2Syn', 'tau1': 0.7, 'tau2': 20.0, 'e': 0}
#netParams.fEPSPweight = 0.0006 #0.0009125
netParams.fEPSPweight = fEPSPweight

netParams.synMechParams['sEPSP'] = {'mod': 'Exp2Syn', 'tau1': 20e3, 'tau2': 6e3, 'e': 0}
#netParams.sEPSPweight = 0.00008
netParams.sEPSPweight = sEPSPweight

netParams.synMechParams['sIPSP'] = {'mod': 'Exp2Syn', 'tau1': 6e3, 'tau2': 8e3, 'e': -80}
netParams.sIPSPweight = sIPSPweight #0.002

netParams.synMechParams['fIPSP'] = {'mod': 'Exp2Syn', 'tau1': 200, 'tau2': 400, 'e': -80}
netParams.fIPSPweight = fIPSPweight #0.004



propVelocity = 100 #(um/ms)
### Synaptic distribution
def synapticConns(CellList, XList, pre, post, percentLoss = 0.2,
                  proj = lambda mu, std: np.random.normal(mu,std),
                  synsPerCellFun = lambda xNorm: 4 - 3*xNorm,
                  mu = 0, std = 1e3):
    pair = list(product(pre,post))
    connParams = [[] for i in range(len(pair))]
    availableXLoc = []
    for i in post: availableXLoc.extend(XList[i])
    for m in range(len(pre)):
        for i in range(len(CellList[pre[m]])):
            nPS = np.int(np.random.normal(synsPerCellFun(XList[pre[m]][i]/netParams.sizeX),0.75))+1
            nPS = np.max([1, nPS])
            for j in range(nPS):
                postSynLoc = proj(15e3,3e3)
                postSynCell = min(range(len(availableXLoc)), key=lambda k: abs((availableXLoc[k]-XList[pre[m]][i])-postSynLoc))
                if (abs(XList[pre[m]][i]-availableXLoc[postSynCell]) < mu-std or
                    abs(XList[pre[m]][i]-availableXLoc[postSynCell]) > mu+std):
                    continue
                p = 0
                while p < len(post):
                    if postSynCell < len(CellList[post[p]]): postGroup = p
                    else: postSynCell = postSynCell - len(CellList[post[p]])
                    p = p + 1    
                connParams[m*len(post)+postGroup].append([i,postSynCell])
    return connParams


### Connect populations
def connectPops(CellList,XList,PrePop,PostPop,projType):
    if (projType == 'Local'):
        projFun = lambda x,y: np.random.normal(locProjDist,2e3)
        sPCFun = lambda xNorm: (locConnMax - (locConnMax-locConnMin)*xNorm)
        meanDist = locProjDist
        stdDist = 2e3
    if (projType == 'AFar'):
        projFun = lambda x,y: np.random.normal(-1*ascProjDist,1e3)
        sPCFun = lambda xNorm: (farConnMax - (farConnMax-farConnMin)*xNorm)
        meanDist = ascProjDist
        stdDist = 1e3
    if (projType == 'DFar'):
        projFun = lambda x,y: np.random.normal(descProjDist,3e3)
        sPCFun = lambda xNorm: (farConnMax - (farConnMax-farConnMin)*xNorm)
        meanDist = descProjDist
        stdDist = 3e3
    conns = synapticConns(CellList,XList,PrePop,PostPop,
                          proj = projFun, synsPerCellFun = sPCFun,
                          mu = meanDist, std = stdDist)
    return conns

A_A_conn = connectPops(CellList,XList,asc,asc,'AFar')
D_D_conn = connectPops(CellList,XList,d5HT,d5HT,'DFar')
A_D_conn = connectPops(CellList,XList,asc,d5HT,'AFar')
D_A_conn = connectPops(CellList,XList,d5HT,asc,'DFar')
S_S_conn = connectPops(CellList,XList,sens,sens,'Local')
S_A_conn = connectPops(CellList,XList,sens,asc,'Local')
S_D_conn = connectPops(CellList,XList,sens,d5HT,'Local')
D_S_conn = connectPops(CellList,XList,d5HT,sens,'DFar')
D_N_conn = connectPops(CellList,XList,d5HT,dNOS,'DFar')
N_N_conn = connectPops(CellList,XList,dNOS,dNOS,'DFar')
N_S_conn = connectPops(CellList,XList,dNOS,sens,'DFar')

#A_A_conn = synapticConns(CellList,XList,'asc_cell','asc_cell',
#            proj = lambda x,y: np.random.normal(-1*ascProjDist,1e3),
#            synsPerCellFun = lambda xNorm: (farConnMax - (farConnMax-farConnMin)*xNorm))
#D_D_conn = synapticConns(CellList,XList,'5HTIN_cell','5HTIN_cell',
#            proj = lambda x,y: np.random.normal(descProjDist,3e3),
#            synsPerCellFun = lambda xNorm: (farConnMax - (farConnMax-farConnMin)*xNorm))
#A_D_conn = synapticConns(CellList,XList,'asc_cell','5HTIN_cell',
#            proj = lambda x,y: np.random.normal(-1*ascProjDist,1e3),
#            synsPerCellFun = lambda xNorm: (farConnMax - (farConnMax-farConnMin)*xNorm))
#D_A_conn = synapticConns(CellList,XList,'5HTIN_cell','asc_cell',
#			proj = lambda x,y: np.random.normal(descProjDist,3e3),
#			synsPerCellFun = lambda xNorm: (farConnMax - (farConnMax-farConnMin)*xNorm))
#S_S_conn = synapticConns(CellList,XList,'sens_cell','sens_cell',
#            proj = lambda x,y: np.random.normal(locProjDist,2e3),
#			synsPerCellFun = lambda xNorm: (locConnMax - (locConnMax-locConnMin)*xNorm))
#S_A_conn = synapticConns(CellList,XList,'sens_cell','asc_cell',
#			proj = lambda x,y: np.random.normal(locProjDist,2e3),
#			synsPerCellFun = lambda xNorm: (locConnMax - (locConnMax-locConnMin)*xNorm))
#S_D_conn = synapticConns(CellList,XList,'sens_cell','5HTIN_cell',
#			proj = lambda x,y: np.random.normal(locProjDist,2e3),
#			synsPerCellFun = lambda xNorm: (locConnMax - (locConnMax-locConnMin)*xNorm))
#D_S_conn = synapticConns(CellList,XList,'5HTIN_cell','sens_cell',
#			proj = lambda x,y: np.random.normal(descProjDist,3e3),
#			synsPerCellFun = lambda xNorm: (farConnMax - (farConnMax-farConnMin)*xNorm))
#D_N_conn = synapticConns(CellList,XList,'5HTIN_cell','NOSIN_cell',
#            proj = lambda x,y: np.random.normal(descProjDist,3e3),
#            synsPerCellFun = lambda xNorm: (farConnMax - (farConnMax-farConnMin)*xNorm))
#N_N_conn = synapticConns(CellList,XList,'NOSIN_cell','NOSIN_cell',
#            proj = lambda x,y: np.random.normal(descProjDist,3e3),
#            synsPerCellFun = lambda xNorm: (farConnMax - (farConnMax-farConnMin)*xNorm))
#N_S_conn = synapticConns(CellList,XList,'NOSIN_cell','sens_cell',
#            proj = lambda x,y: np.random.normal(descProjDist,3e3),
#            synsPerCellFun = lambda xNorm: (farConnMax - (farConnMax-farConnMin)*xNorm))
#
#A_A_connList = A_A_conn['List']
#D_D_connList = D_D_conn['List']
#A_D_connList = A_D_conn['List']
#D_A_connList = D_A_conn['List']
#S_S_connList = S_S_conn['List']
#S_A_connList = S_A_conn['List']
#S_D_connList = S_D_conn['List']
#D_S_connList = D_S_conn['List']
#D_N_connList = D_N_conn['List']
#N_N_connList = N_N_conn['List']
#N_S_connList = N_S_conn['List']

#spkTimes = [1005, 1020]
#netParams.popParams['Input'] = {'cellModel': 'VecStim', 'spkTimes': spkTimes, 'numCells':2, 'xRange': [50e3, 60e3], 'yRange': [110, 110]}    

#netParams.popParams['Noise'] = {'cellModel': 'NetStim', 'rate': inputFreq, 'noise': inputNoise, 'numCells': nInputs, 'yRange': [80, 80]}    
stimStep = 1/inputFreq*1e3
spkTimes = list()
for i in range(nInputs):
    spkTimes.append(list(np.arange(stimStart,stimStop,stimStep)+np.random.uniform(0,inputNoise*stimStep,(np.size(np.arange(stimStart,stimStop,stimStep))))))
netParams.popParams['Noise'] = {'cellModel': 'VecStim', 'numCells': nInputs, 'spkTimes': spkTimes, 'yRange': [80, 80]}    

### Cell connectivity rules
netParams.connParams['Noise->sens'] = {            
    'preConds': {'pop': 'Noise'},             
    'postConds': {'pop': 'sens_cell'},  
    'probability': 'exp(-dist_x/1e3)',
    'weight': '2*fEPSPweight',                  
    'delay': 0,                             
    'synMech': 'fEPSP'}

groupLabel = ['sens->sens','sens->5HTIN','sens->asc','asc->asc','5HTIN->5HTIN','asc->5HTIN','5HTIN->asc','5HTIN->sens','5HTIN->NOSIN','NOSIN->NOSIN','NOSIN->sens']
preGroup = [sens,sens,sens,asc,d5HT,asc,d5HT,d5HT,d5HT,dNOS,dNOS]
postGroup = [sens,d5HT,asc,asc,d5HT,d5HT,asc,sens,dNOS,dNOS,sens]
connGroup = [S_S_conn,S_D_conn,S_A_conn,A_A_conn,D_D_conn,A_D_conn,D_A_conn,D_S_conn,D_N_conn,N_N_conn,N_S_conn]
groupWeight = ['sEPSPweight','fEPSPweight','fEPSPweight','fEPSPweight','fEPSPweight','fEPSPweight','fEPSPweight','fEPSPweight','fEPSPweight','fEPSPweight','sIPSPweight']
groupMech = ['sEPSP','fEPSP','fEPSP','fEPSP','fEPSP','fEPSP','fEPSP','fEPSP','fEPSP','fEPSP','sIPSP']

for i in range(len(groupLabel)):
    pair = list(product(preGroup[i],postGroup[i]))
    for j in range(len(pair)):
        netParams.connParams[groupLabel[i]+str(pair[j])] = {
                'preConds': {'pop': pair[j][0]},
                'postConds': {'pop': pair[j][1]},
                'connList': connGroup[i][j],
                'weight': groupWeight[i],
                'delay': 'dist_x/propVelocity',
                'synMech': groupMech[i]}

#netParams.connParams['sens->sens'] = {
#	'preConds': {'pop': 'sens_cell'},
#	'postConds': {'pop': 'sens_cell'},
#	'connList': S_S_connList,
#	'weight': 'sEPSPweight',
#	'delay': 'dist_x/propVelocity',
#	'synMech': 'sEPSP'}
#netParams.connParams['sens->5HTIN'] = {
#	'preConds': {'pop': 'sens_cell'},
#	'postConds': {'pop': '5HTIN_cell'},
#	'connList': S_D_connList,
#	'weight': 'fEPSPweight',
#	'delay': 'dist_x/propVelocity',
#	'synMech': 'fEPSP'}
#netParams.connParams['sens->asc'] = {
#	'preConds': {'pop': 'sens_cell'},
#	'postConds': {'pop': 'asc_cell'},
#	'connList': S_A_connList,
#	'weight': 'fEPSPweight',
#	'delay': 'dist_x/propVelocity',
#	'synMech': 'fEPSP'}
#netParams.connParams['asc->asc'] = {
#    'preConds': {'pop': ['asc_cell']},
#    'postConds': {'pop': ['asc_cell']},
#    'connList': A_A_connList,
#    'weight': 'fEPSPweight',
#    'delay': 'dist_x/propVelocity',
#    'synMech': 'fEPSP'}
#netParams.connParams['5HTIN->5HTIN'] = {
#    'preConds': {'pop': ['5HTIN_cell']},
#    'postConds': {'pop': ['5HTIN_cell']},
#    'connList': D_D_connList,
#    'weight': 'fEPSPweight',
#    'delay': 'dist_x/propVelocity',
#    'synMech': 'fEPSP'}
#netParams.connParams['asc->5HTIN'] = {
#    'preConds': {'pop': ['asc_cell']},
#    'postConds': {'pop': ['5HTIN_cell']},
#    'connList': A_D_connList,
#    'weight': 'fEPSPweight',
#    'delay': 'dist_x/propVelocity',
#    'synMech': 'fEPSP'}
#netParams.connParams['5HTIN->asc'] = {
#    'preConds': {'pop': ['5HTIN_cell']},
#    'postConds': {'pop': ['asc_cell']},
#    'connList': D_A_connList,
#    'weight': 'fEPSPweight',
#    'delay': 'dist_x/propVelocity',
#    'synMech': 'fEPSP'}
#netParams.connParams['5HTIN->sens'] = {
#    'preConds': {'pop': ['5HTIN_cell']},
#    'postConds': {'pop': ['sens_cell']},
#    'connList': D_S_connList,
#    'weight': 'fEPSPweight',
#    'delay': 'dist_x/propVelocity',
#    'synMech': 'fEPSP'}
#netParams.connParams['5HTIN->NOSIN'] = {
#    'preConds': {'pop': ['5HTIN_cell']},
#    'postConds': {'pop': ['NOSIN_cell']},
#    'connList': D_N_connList,
#    'weight': 'fEPSPweight',
#    'delay': 'dist_x/propVelocity',
#    'synMech': 'fEPSP'}
#netParams.connParams['NOSIN->NOSIN'] = {
#    'preConds': {'pop': ['NOSIN_cell']},
#    'postConds': {'pop': ['NOSIN_cell']},
#    'connList': N_N_connList,
#    'weight': 'fEPSPweight',
#    'delay': 'dist_x/propVelocity',
#    'synMech': 'fEPSP'}
#netParams.connParams['NOSIN->sens'] = {
#    'preConds': {'pop': ['NOSIN_cell']},
#    'postConds': {'pop': ['sens_cell']},
#    'connList': N_S_connList,
#    'weight': 'sIPSPweight',
#    'delay': 'dist_x/propVelocity',
#    'synMech': 'sIPSP'}

### Simulation options
simConfig = specs.SimConfig()                   # object of class SimConfig to store simulation configuration

simConfig.hParams['v_init'] = -52
simConfig.duration = simDur                     # Duration of the simulation, in ms
simConfig.dt = 0.05                             # Internal integration timestep to use
simConfig.verbose = False                       # Show detailed messages
simConfig.recordStep = 0.1                      # Step size in ms to save data (e.g. V traces, LFP, etc)

simConfig.saveDataInclude = ['netParams', 'netCells', 'netPops', 'simConfig', 'simData']
simConfig.filename = outputFilename + '_model_output'   # Set file output name
simConfig.saveJson = saveOutput                 # Save params, network and sim output to json file
sim.updateInterval = 2*simConfig.dt                                                
simConfig.recordLFP = [[0, 10e3, 0]]

simConfig.analysis['plotRaster'] = {'include': sens + asc + d5HT + dNOS, 'spikeHist': 'subplot',
                  'orderBy': 'x', 'orderInverse': True, 'syncLines': True} # Plot a raster
simConfig.analysis['plot2Dnet'] = True

sim.create(netParams, simConfig)

for i in range(len(sim.net.cells)):
    if sim.net.cells[i].tags['pop'] == 'Input' or sim.net.cells[i].tags['pop'] == 'Noise': continue
    modifier = np.random.normal(loc=0.0,scale=0.02)
    cOn = sim.net.cells[i].secs['soma']['mechs']['cMSC']['onHalf']
    sim.net.cells[i].secs['soma']['mechs']['cMSC']['onHalf'] = (1.0+modifier)*cOn
    sim.net.cells[i].secs['soma']['hObj'].onHalf_cMSC = (1.0+modifier)*cOn
    cOff = sim.net.cells[i].secs['soma']['mechs']['cMSC']['offHalf']
    sim.net.cells[i].secs['soma']['mechs']['cMSC']['offHalf'] = (0.01*modifier)+cOff
    sim.net.cells[i].secs['soma']['hObj'].offHalf_cMSC = (0.01*modifier)+cOff
    tOn = sim.net.cells[i].secs['soma']['mechs']['tMSC']['onHalf']
    sim.net.cells[i].secs['soma']['mechs']['tMSC']['onHalf'] = (1.0+modifier)*tOn
    sim.net.cells[i].secs['soma']['hObj'].onHalf_tMSC = (1.0+modifier)*tOn
    tOff = sim.net.cells[i].secs['soma']['mechs']['tMSC']['offHalf']
    sim.net.cells[i].secs['soma']['mechs']['tMSC']['offHalf'] = (1.0+modifier)*tOff
    sim.net.cells[i].secs['soma']['hObj'].offHalf_tMSC = (1.0+modifier)*tOff

sim.runSimWithIntervalFunc(sim.updateInterval, updateStrains)
#sim.simulate()
sim.analyze()

pops = list(sim.net.pops.keys())
cells = {k: [] for k in pops}
ALLspikes = []
for i in range(len(pops)):
    cells[pops[i]].append(sim.net.pops[pops[i]].cellGids)
    
    spkID = list(sim.simData.spkid)
    spkT = list(sim.simData.spkt)
    allSpikes = []
    for i in range(len(pops)):
        if pops[i]=='Noise': continue
        else:
            tempCells = np.in1d(spkID,cells[pops[i]])
            tempTimes = list(compress(spkT,tempCells))
            for k in range(len(tempTimes)):
                allSpikes.append(tempTimes[k]/1e3)
        
    histData = plt.hist(allSpikes,bins=int(4*30))
    hist = histData[0]
    bin_edges = histData[1]
    [f,pxx] = signal.welch(hist,1/(bin_edges[1]-bin_edges[0]))
    outputFrequency = f[np.argmax(pxx)]
    ALLspikes = ALLspikes + allSpikes


histData = plt.hist(ALLspikes,bins=int(4*30))
if len(histData) > 0:
    if len(histData[0]) > 0:
        thresh = np.argwhere(histData[0]>0.1*np.max(histData[0]))
        if len(thresh) > 0:
            startFiring = histData[1][thresh[0]]
            stopFiring = histData[1][thresh[-1]]
        else:
            startFiring = np.array(np.nan)
            stopFiring = np.array(np.nan)
        elem = np.nonzero(histData[0])
        nSpk = list(histData[0][elem])
        tSpk = list(histData[1][elem])
    else:
        startFiring = np.array(np.nan)
        stopFiring = np.array(np.nan)
        nSpk = np.array(np.nan)
        tSpk = np.array(np.nan)
else:
    startFiring = np.array(np.nan)
    stopFiring = np.array(np.nan)
    nSpk = np.array(np.nan)
    tSpk = np.array(np.nan)

spikeTxt = outputFilename + '_spikes.txt'
spikeTrains = list()
spikeJson = list()
rate = list()
for i in range(len(sim.net.cells)):
    spikeTrains.append(list())
    spikeJson.append(list())
    logInd = [(x == i) for x in sim.simData['spkid']]
    timeArray = np.array(sim.simData['spkt'])
    theseSpikes = np.array(timeArray[logInd])
    theseSpikes.sort()
    spikeTrains[i] = spk.SpikeTrain(theseSpikes,np.array([0.0, simDur]))
    spikeJson[i] = list(theseSpikes)
    if np.size(theseSpikes) > 0:
        rate.append(1/(np.mean(np.diff(theseSpikes)/1e3)))

connDict = dict()
connLabels = list(netParams.connParams.keys())
for i in range(len(connLabels)):
    connDict[connLabels[i]] = list()
for i in range(len(sim.net.cells)):
    for j in range(len(sim.net.cells[i].conns)):
        connDict[sim.net.cells[i].conns[j].label].append(
                sim.net.cells[i].tags['x'] - sim.net.cells[sim.net.cells[i].conns[j]['preGid']].tags['x'])

mechDict = dict()
mechType = ['tMSC','cMSC']
mechParam = ['offHalf','onHalf']
for i in range(len(pops)):
    mechDict[pops[i]] = dict()
    for j in range(len(mechType)):
        for k in range(len(mechParam)):
            mechDict[pops[i]][mechType[j]+'_'+mechParam[k]] = list()
for i in range(len(sim.net.cells)):
    if sim.net.cells[i].tags['pop'] == 'Noise': continue
    for j in range(len(mechType)):
        for k in range(len(mechParam)):
            mechDict[sim.net.cells[i].tags['pop']][mechType[j]+'_'+mechParam[k]].append(
                    sim.net.cells[i].secs['soma']['mechs'][mechType[j]][mechParam[k]])

inputCells = sim.net.pops['Noise'].cellGids
inputCellLoc = list()
for i in range(len(inputCells)):
    inputCellLoc.append(sim.net.cells[inputCells[i]].tags['x'])

output = dict()

output['input'] = dict()
output['input']['numNeurons'] = numNeurons
output['input']['simDur'] = simDur
output['input']['nInputs'] = nInputs
output['input']['inputFreq'] = inputFreq
output['input']['inputNoise'] = inputNoise
output['input']['replicate'] = rep
output['input']['ascProjDist'] = ascProjDist
output['input']['descProjDist'] = descProjDist
output['input']['locProjDist'] = locProjDist
output['input']['farConnMax'] = farConnMax
output['input']['farConnMin'] = farConnMin
output['input']['locConnMax'] = locConnMax
output['input']['locConnMin'] = locConnMin
output['input']['fEPSPweight'] = fEPSPweight
output['input']['sEPSPweight'] = sEPSPweight
output['input']['fIPSPweight'] = fIPSPweight
output['input']['sIPSPweight'] = sIPSPweight
output['input']['stimStart'] = stimStart
output['input']['stimStop'] = stimStop

output['syncMeasure'] = sim.analysis.syncMeasure()
output['rateMean'] = np.nanmean(rate)
output['rateStd'] = np.nanstd(rate)
output['1-PySpikeISISdist'] = 1-spk.isi_distance(spikeTrains)
output['PySpikeSync'] = spk.spike_sync(spikeTrains)
output['connsPerCell'] = sim.connsPerCell
output['synsPerCell'] = sim.synsPerCell
output['firingStart'] = startFiring.item(0)
output['firingStop'] = stopFiring.item(0)
output['spkCount'] = nSpk
output['spkTime'] = tSpk
output['projDistance'] = connDict
output['mechanoProps'] = mechDict
output['inputCellX'] = inputCellLoc


outputFilename = outputFilename + '_model_measures.json'
with open(outputFilename,'w') as f:
    json.dump(output,f)

