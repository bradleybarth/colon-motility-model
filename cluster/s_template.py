from matplotlib import pyplot
import random
from datetime import datetime
import pickle
from neuron import h


# template for mechanosensitive s-type electrophysiological characteristics described by:
# Joel Bornstein at ISAN 2019, Los Angeles, CA
#      https://data.sparc.science/sim/#/dataset/30
#      Joel Bornstein, & Parvin Zarei Eskikand. (n.d.). Small Enteric Neural 
#          Network Simulator. https://doi.org/10.26275/I3XV-FOFG
#
# with characteristics from:
# Spencer and Smith, 2004
#      https://doi.org/10.1113/jphysiol.2004.063586
#      Spencer, N. J. and Smith, T. K. (2004), Mechanosensory S-neurons rather 
#          than AH-neurons appear to generate a rhythmic motor pattern in guinea
#          -pig distal colon. The Journal of Physiology, 558: 577-596.
#
# adapted by Bradley Barth on October 29th, 2019


class Cell(object):
    def __init__(self):
        self.synlist = []
        self.createSections()
        self.buildTopology()
        self.defineGeometry()
        self.defineBiophysics()
        self.createSynapses()
        self.nclist = []

    def createSections(self):
        pass

    def buildTopology(self):
        pass

    def defineGeometry(self):
        pass

    def defineBiophysics(self):
        pass

    def createSynapses(self):
        """Add an exponentially decaying synapse """
        synsoma = h.ExpSyn(self.soma(0.5))
        synsoma.tau = 5
        synsoma.e = -20
        self.synlist.append(synsoma) # synlist is defined in Cell


    def createNetcon(self, thresh=10):
        """ created netcon to record spikes """
        nc = h.NetCon(self.soma(0.5)._ref_v, None, sec = self.soma)
        nc.threshold = thresh
        return nc
        
class s_usamen(Cell): 
    """S-MEN cell: A soma with active channels""" 
    def createSections(self):
        """Create the sections of the cell."""
        self.soma = h.Section(name='soma', cell=self)

    def defineGeometry(self):
        """Set the 3D geometry of the cell."""
        self.soma.L = 23.0
        self.soma.diam = 10.0
        self.soma.Ra = 30
        self.soma.cm = 1
    
    def defineBiophysics(self):
        """Assign the membrane properties across the cell."""
        # Insert active channels in the soma        
        self.soma.insert('pas')
        self.soma.g_pas = 0.0005  # Passive conductance in S/cm2
        self.soma.e_pas = -47.0    # Leak reversal potential mV
        
        self.soma.insert('nav13')
        self.soma.gbar_nav13 = 0.2
        
        self.soma.insert('nav17')
        
        self.soma.insert('kdrBS')
        self.soma.gbar_kdrBS = 0.008           
        
        self.soma.insert('ka')
        self.soma.gbar_ka = 0.1
        
        self.soma.insert('kmtwt')
        self.soma.gbar_kmtwt = 0.0005
    
        self.soma.insert('cMSC')
        self.soma.gbar_cMSC = 0.003
        self.soma.onHalf_cMSC = 0.07
        self.soma.onTauOffset_cMSC = 0.01
        self.soma.onTauSlope_cMSC = 0.005
        self.soma.onTauHalf_cMSC = 0.01
        self.soma.offTauOffset_cMSC = 0.1667
        self.soma.onTauMax_cMSC = 500
        self.soma.offTauMax_cMSC = 300
    
        self.soma.insert('tMSC')
        self.soma.gbar_tMSC = 0.0003
        self.soma.offTauOffset_tMSC = 0.5
        self.soma.offSlope_tMSC = 0.01
        self.soma.offTauSlope_tMSC = 0.05
    
class s_samen(Cell): 
    """S-MEN cell: A soma with active channels""" 
    def createSections(self):
        """Create the sections of the cell."""
        self.soma = h.Section(name='soma', cell=self)

    def defineGeometry(self):
        """Set the 3D geometry of the cell."""
        self.soma.L = 23.0
        self.soma.diam = 10.0
        self.soma.Ra = 30
        self.soma.cm = 1
    
    def defineBiophysics(self):
        """Assign the membrane properties across the cell."""
        # Insert active channels in the soma        
        self.soma.insert('pas')
        self.soma.g_pas = 0.0005  # Passive conductance in S/cm2
        self.soma.e_pas = -47.0    # Leak reversal potential mV
        
        self.soma.insert('nav13')
        self.soma.gbar_nav13 = 0.2
        
        self.soma.insert('nav17')
        
        self.soma.insert('kdrBS')
        self.soma.gbar_kdrBS = 0.008           
        
        self.soma.insert('ka')
        self.soma.gbar_ka = 0.1
        
        self.soma.insert('kmtwt')
        self.soma.gbar_kmtwt = 0.0005
    
        self.soma.insert('cMSC')
        self.soma.gbar_cMSC = 0.003
        self.soma.onHalf_cMSC = 0.07
        self.soma.onTauOffset_cMSC = 0.01
        self.soma.onTauSlope_cMSC = 0.005
        self.soma.onTauHalf_cMSC = 0.01
        self.soma.offTauOffset_cMSC = 0.1667
        self.soma.onTauMax_cMSC = 500
        self.soma.offTauMax_cMSC = 300
    
        self.soma.insert('tMSC')
        self.soma.gbar_tMSC = 0.0003
        self.soma.offTauOffset_tMSC = 0.2
        self.soma.offSlope_tMSC = 0.01
        self.soma.offTauSlope_tMSC = 0.05
    
class s_ramen(Cell): 
    """S-MEN cell: A soma with active channels""" 
    def createSections(self):
        """Create the sections of the cell."""
        self.soma = h.Section(name='soma', cell=self)

    def defineGeometry(self):
        """Set the 3D geometry of the cell."""
        self.soma.L = 23.0
        self.soma.diam = 10.0
        self.soma.Ra = 30
        self.soma.cm = 1
    
    def defineBiophysics(self):
        """Assign the membrane properties across the cell."""
        # Insert active channels in the soma        
        self.soma.insert('pas')
        self.soma.g_pas = 0.0005  # Passive conductance in S/cm2
        self.soma.e_pas = -47.0    # Leak reversal potential mV
        
        self.soma.insert('nav13')
        self.soma.gbar_nav13 = 0.2
        
        self.soma.insert('nav17')
        
        self.soma.insert('kdrBS')
        self.soma.gbar_kdrBS = 0.008           
        
        self.soma.insert('ka')
        self.soma.gbar_ka = 0.1
        
        self.soma.insert('kmtwt')
        self.soma.gbar_kmtwt = 0.0005
    
        self.soma.insert('cMSC')
        self.soma.gbar_cMSC = 0.002
        self.soma.onHalf_cMSC = 0.07
        self.soma.onTauOffset_cMSC = 0.01
        self.soma.onTauSlope_cMSC = 0.005
        self.soma.onTauHalf_cMSC = 0.01
        self.soma.offTauOffset_cMSC = 0.1667
        self.soma.onTauMax_cMSC = 500
        self.soma.offTauMax_cMSC = 300
    
        self.soma.insert('tMSC')
        self.soma.gbar_tMSC = 0.0025
        self.soma.onHalf_tMSC = 0.07
        self.soma.offHalf_tMSC = 0
        self.soma.onTauOffset_tMSC = 0.01
        self.soma.onTauSlope_tMSC = 0.005
        self.soma.onTauHalf_tMSC = 0.01
        self.soma.offTauOffset_tMSC = 0.1667
        self.soma.onTauMax_tMSC = 500
        self.soma.offTauMax_tMSC = 300
        self.soma.offTauOffset_tMSC = 0.2
        
    def buildTopology(self):
        pass            
        
class s_cell(Cell): 
    """S cell: A soma with active channels""" 
    def createSections(self):
        """Create the sections of the cell."""
        self.soma = h.Section(name='soma', cell=self)

    def defineGeometry(self):
        """Set the 3D geometry of the cell."""
        self.soma.L = 23.0
        self.soma.diam = 10.0
        self.soma.Ra = 30
        self.soma.cm = 1
    
    def defineBiophysics(self):
        """Assign the membrane properties across the cell."""
        # Insert active channels in the soma        
        self.soma.insert('pas')
        self.soma.g_pas = 0.0005  # Passive conductance in S/cm2
        self.soma.e_pas = -47.0    # Leak reversal potential mV
        
        self.soma.insert('nav13')
        self.soma.gbar_nav13 = 0.2
        
        self.soma.insert('nav17')
        
        self.soma.insert('kdrBS')
        self.soma.gbar_kdrBS = 0.008           
        
        self.soma.insert('ka')
        self.soma.gbar_ka = 0.1
        
        self.soma.insert('kmtwt')
        self.soma.gbar_kmtwt = 0.0005
    
        self.soma.insert('cMSC')
        self.soma.gbar_cMSC = 0.0
    
        self.soma.insert('tMSC')
        self.soma.gbar_tMSC = 0.0
        
        
    def buildTopology(self):
        pass