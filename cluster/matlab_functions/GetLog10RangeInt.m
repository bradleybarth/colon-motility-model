function vec = GetLog10RangeInt(A,n)

if n == 1
    vec = round(min(A))
else
    vec = round(logspace(log10(A(1)),log10(A(2)),n))
end
%double check
if (length(vec)~=n)
    if (length(vec)==(n-1) && vec(end) ~= A(2))
        vec(end+1) = A(2)
    else
        error('Matching Lengths Unable to be Achieved')
        vec
    end
end

% make sure minimum value is 1
vec(vec<1) = 1;

end