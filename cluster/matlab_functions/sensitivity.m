function [ax, b0, b1] = sensitivity(S,var1,var2)

fields = fieldnames(S);
X = S.(fields{var1});
Y = S.(fields{var2});

X = X(~isnan(Y));
Y = Y(~isnan(Y));

Xscore = (X - nanmean(X))./nanstd(X);
Yscore = (Y - nanmean(Y))./nanstd(Y);

[R,P] = corrcoef(Xscore,Yscore);

figure
ax = subplot(1,1,1);
plot(Xscore,Yscore,'k.');
axis(3.5*[-1 1 -1 1])
grid on

xstr = sprintf('%s (%.2g,%.2g)',fields{var1},nanmean(X),nanstd(X));
xlabel(xstr)
ystr = sprintf('%s (%.2g,%.2g)',fields{var2},nanmean(Y),nanstd(Y));
ylabel(ystr)

x = [ones(length(Xscore),1) Xscore];
b = x\Yscore;

b0 = b(1);
b1 = b(2);

newX = [-3.5;3.5];
Y2 = b1*newX + b0;
hold on
plot(newX,Y2,'r-')
set(ax,'FontSize',16)

statStr = sprintf('R^2 = %.3f\np = %.3g',R(1,2)^2,P(1,2));
text(-3,3,statStr,'HorizontalAlignment','left','VerticalAlignment','top','FontSize',14);

var2Mu = sprintf('\\mu = %.2g',nanmean(Y));
var2Std1 = sprintf('\\mu+\\sigma = %.2g',nanmean(Y)+nanstd(Y));
var2Std2 = sprintf('\\mu-\\sigma = %.2g',nanmean(Y)-nanstd(Y));
text(-3.3,0,var2Mu,'HorizontalAlignment','left','VerticalAlignment','middle','FontSize',14);
text(-3.3,1,var2Std1,'HorizontalAlignment','left','VerticalAlignment','middle','FontSize',14);
text(-3.3,-1,var2Std2,'HorizontalAlignment','left','VerticalAlignment','middle','FontSize',14);

var1Mu = sprintf('%.2g',nanmean(X));
var1Std1 = sprintf('%.2g',nanmean(X)+nanstd(X));
var1Std2 = sprintf('%.2g',nanmean(X)-nanstd(X));
text(0,-3.3,var1Mu,'HorizontalAlignment','center','VerticalAlignment','bottom','FontSize',14);
text(1,-3.3,var1Std1,'HorizontalAlignment','left','VerticalAlignment','bottom','FontSize',14);
text(-1,-3.3,var1Std2,'HorizontalAlignment','right','VerticalAlignment','bottom','FontSize',14);

fprintf('%s,%s,%.3g,%.3g,%.3g,%.3g\n',fields{var2},fields{var1},nanmean(X),nanstd(X),R(1,2)^2,P(1,2))