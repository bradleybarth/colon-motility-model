function ax = plotTheseArgsRatio(dataStruct,xVar1,xVar2,yVar)

fields = fieldnames(dataStruct);
numNeurons = unique(dataStruct.numNeurons);
simDur = unique(dataStruct.simDur);
inputFreq = unique(dataStruct.inputFreq);
inputNoise = unique(dataStruct.inputNoise);
nInputs = unique(dataStruct.nInputs);
replicate = unique(dataStruct.replicate);

indices = nan(length(numNeurons),...
    length(simDur),...
    length(inputFreq),...
    length(inputNoise),...
    length(nInputs),...
    length(dataStruct.numNeurons));
for i = 1:length(numNeurons)
    for j = 1:length(simDur)
        for k = 1:length(inputFreq)
            for l = 1:length(inputNoise)
                for m = 1:length(nInputs)
                    indices(i,j,k,l,m,:) = logical((dataStruct.numNeurons==numNeurons(i)).*...
                        (dataStruct.simDur==simDur(j)).*...
                        (dataStruct.inputFreq==inputFreq(k)).*...
                        (dataStruct.inputNoise==inputNoise(l)).*...
                        (dataStruct.nInputs==nInputs(m)));
                end
            end
        end
    end
end

xData1 = getfield(dataStruct,fields{xVar1});
xData2 = getfield(dataStruct,fields{xVar2});
xData = xData1./xData2;
yData = getfield(dataStruct,fields{yVar});
temp = [];
for i = 1:length(numNeurons)
    for j = 1:length(simDur)
        for k = 1:length(inputFreq)
            for l = 1:length(inputNoise)
                for m = 1:length(nInputs)
                    index = logical(squeeze(indices(i,j,k,l,m,:)));
                    temp = [temp;...
                        nanmean(xData(index)),...
                        nanmean(yData(index))];
                end
            end
        end
    end
end

figure
ax = subplot(1,1,1);
scatter(xData,yData,5*ones(length(xData),1),'k','filled')
hold on
scatter(temp(:,1),temp(:,2),10*ones(size(temp,1),1),'r','filled')
set(gca,'XScale','log')
xlabel([fields{xVar1},' / ',fields{xVar2}])
ylabel(fields{yVar})
grid on
hold off