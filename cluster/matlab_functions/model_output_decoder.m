fpath = 'C:\Users\Bradley Barth\Box\Home Folder bbb16\Sync\School\grill_lab\projects\ex-vivo-colon-motility\gitlab-repository\netpyne';
fname = 'model_output_nn_5000_dur10_3.85_inFreq_5.4_inNoise_0.2_inN_10.json';
val = jsondecode(fileread(fullfile(fpath,fname)));

%% Get meta and identifiers
pops = fieldnames(val.net.pops);
cells = cell(length(pops),1);
x_pos = nan(length(val.net.cells),1);
y_pos = nan(length(val.net.cells),1);
synList = zeros(length(val.net.cells),1);
for i = 1:length(pops)
    cells{i} = getfield(val.net.pops,pops{i}).cellGids;
end
conns = [];
for i = 1:length(val.net.cells)
    x_pos(i) = 150-val.net.cells{i}.tags.x/1e3;
    y_pos(i) = 150-val.net.cells{i}.tags.y/1e3;
    if strcmp(val.net.cells{i}.tags.pop,'Noise'),continue,end
    for j = 1:length(val.net.cells{i}.conns)        
        if strcmp(val.net.cells{val.net.cells{i}.conns(j).preGid+1}.tags.pop,'Noise'),continue,end
        synList(val.net.cells{i}.conns(j).preGid+1) = ...
            synList(val.net.cells{i}.conns(j).preGid+1)+1;
        if strcmp(val.net.cells{val.net.cells{i}.conns(j).preGid+1}.tags.pop,'asc_cell')
            popLabel = 2;
        elseif strcmp(val.net.cells{val.net.cells{i}.conns(j).preGid+1}.tags.pop,'desc_cell')
            popLabel = 3;
        else
            popLabel = 1;
        end
        conns = [conns; i val.net.cells{i}.conns(j).preGid+1 popLabel];
    end
end
spkt = val.simData.spkt/1e3;
spkid = val.simData.spkid;

%% Make raster plot
figure
subplot(4,1,1:3)
clr = {'k.','r.','b.'};
allSpikes = [];
allCells = length(cells{2})+length(cells{3});
for i = 1:length(pops)
    if strcmp(pops{i},'Noise'), continue, end
    tempCells = ismember(spkid,cells{i});
    scatter(spkt(tempCells),x_pos(spkid(tempCells)+1),clr{i})
    allSpikes = [allSpikes spkt(tempCells)'];
    hold on
end
hold off
box on
axis([1 7 0 150])
set(gca,'YTickLabel',[150:-50:0])
set(gca,'XTickLabel',[])
set(gca,'FontName','Roboto Medium','FontSize',10)
ylab = sprintf('Position\n(mm)');
ylabel(ylab)
legend('Ascending','Descending','Location','northeast')

% and spike histogram subplot
subplot(4,1,4)
H = histogram(allSpikes,6*30,'FaceColor',0.167*[1 1 1]);
set(gca,'FontName','Roboto Medium')
xlabel('Time (s)')
ylab = sprintf('Number\nof spikes');
ylabel(ylab)
xlim([1 7])
set(gca,'XTick',1:7)
set(gca,'FontName','Roboto Medium','FontSize',10)
set(gca,'XTickLabel',0:6)

%% Make network graphic
figure
subplot(1,5,1:3)
clr = {'ko','ro','bo'};
for i = 1:length(pops)
    if strcmp(pops{i},'Noise'), continue, end
    tempCells = cells{i}+1;
    scatter(y_pos(tempCells),x_pos(tempCells),30,clr{i},'filled')
    hold on
end
xlim([149.94 150.01])
hold off
box on
set(gca,'XTick',[])
ytick = get(gca,'YTick');
set(gca,'YTickLabels',fliplr(ytick));
ylabel('Position (mm)')
xlab = sprintf('Cell Distribution');
title(xlab)
legend('Ascending','Descending','Location','southoutside','Orientation','horizontal')
set(gca,'FontName','Roboto Medium','FontSize',10)
subplot(1,5,4)
h = histogram(x_pos,linspace(0,150,31),'orientation','horizontal','FaceColor',0.167*[1 1 1]);
X = xlim;
xlim([0 ceil(X(2)/10)*10])
ylim([0 150])
set(gca,'YTick',[])
xlab = sprintf('Cells\nper 5mm');
title(xlab)
Xbar = h.BinEdges(2:end)-h.BinWidth/2;
meanSyn = zeros(size(h.Values));
for i = 1:length(h.Values)
    meanSyn(i) = mean(synList(x_pos<h.BinEdges(i+1)));
end
set(gca,'FontName','Roboto Medium','FontSize',10)
subplot(1,5,5)
barh(Xbar,meanSyn,1,'FaceAlpha',0.6,'FaceColor',0.167*[1 1 1]);
ylim([0 150])
xlab = sprintf('Synapses\nper cell');
title(xlab)
X = xlim;
xlim([0 ceil(X(2))])
set(gca,'YTick',[])
set(gca,'FontName','Roboto Medium','FontSize',10)

%% Projection probability
dist_1d = (x_pos(conns(:,2))-x_pos(conns(:,1)));
figure
histogram(dist_1d(conns(:,3)==2),linspace(-25,25,26),'FaceColor','r')
hold on
histogram(dist_1d(conns(:,3)==3),linspace(-25,25,26),'FaceColor','b')
xlim([-25 25])
legend('Ascending','Descending','Location','northwest')
X = xlim;
Y = ylim;
text(0.025*(X(2)-X(1))+X(1),0.825*(Y(2)-Y(1))+Y(1),...
    ['Population A:D ratio = ',...
    num2str(round(length(cells{2})/length(cells{3}),2))],...
    'HorizontalAlignment','left')
xlabel('Projection distance (mm)')
set(gca,'FontName','Roboto Medium','FontSize',10)

%% Network with noise
figure
subplot(1,5,1:3)
clr = {'ko','ro','bo'};
for i = 1:length(pops)
    tempCells = cells{i}+1;
     if strcmp(pops{i},'Noise')
         y_pos(tempCells) = 149.945;
     end
    scatter(y_pos(tempCells),x_pos(tempCells),30,clr{i},'filled')
    hold on
end
xlim([149.94 150.01])
hold off
box on
set(gca,'XTick',[])
ytick = get(gca,'YTick');
set(gca,'YTickLabels',fliplr(ytick));
ylabel('Position (mm)')
xlab = sprintf('Cell Distribution');
title(xlab)
legend('Input','Ascending','Descending','Location','southoutside','Orientation','horizontal')
set(gca,'FontName','Roboto Medium','FontSize',10)