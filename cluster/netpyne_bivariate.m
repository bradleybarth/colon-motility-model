addpath('matlab_functions')

try 
    fileID = fopen('compile_bivariate.out','r');
    A = [];
    while ~feof(fileID)
        text = fgetl(fileID);
        text(1) = []; text(end) = [];
        parts = split(text,', ');
        line = [];
        for i = 1:length(parts)
            line = [line str2num(parts{i})];
        end
        A = [A; line];
    end
    fclose(fileID);
    completedSets = A;
catch
    completedSets = zeros(1,6);
end

%Overview Info
numReps = 10

inputFreqVal = [1 2 5 10 20 50]
inputFreqSet = length(inputFreqVal);

inputNoiseVal = [0 0.2 0.4 0.6 0.8 1.0]
inputNoiseSet = length(inputNoiseVal);

projModVal = [0.1 0.5 0.9 0.99 1 1.01 1.1 2 10]
projModSet = length(projModVal);

EPSPModVal = [0.1 0.5 0.9 0.99 1 1.01 1.1 2 10]
EPSPModSet = length(EPSPModVal);

connModVal = [0.1 0.5 0.9 0.99 1 1.01 1.1 2 10]
connModSet = length(connModVal);

%How many sims?
totalSims = numReps*inputFreqSet*inputNoiseSet*(projModSet+EPSPModSet+connModSet);

if totalSims > 20000
    return
end

thisScript = 1;
thisSim = 1;
simsOnScript = 0;

jobPre = '#SBATCH --job-name=';
outPre = '#SBATCH --output=/hpc/home/bbb16/netpyne/scripts/';
errPre = '#SBATCH --error=/hpc/home/bbb16/netpyne/scripts/';
cdLine = 'cd /hpc/home/bbb16/netpyne';
preLine = sprintf('./../mod/x86_64/special -nobanner -python cluster_interneuron_network_sensitivity.py');

numSimsPerScript = 10;
complexity = log10(20000*6000);
for i = 1:inputFreqSet
    for j = 1:inputNoiseSet
        for k = 1:numReps
            
            m = 5;
            n = 5;
            for l = 1:projModSet
                simulationID = [inputFreqVal(i),...
                    inputNoiseVal(j),...
                    projModVal(l),...
                    EPSPModVal(m),...
                    connModVal(n),...
                    (k-1)];
                if ismember(simulationID,completedSets,'rows')
                    disp(['This sim already compiled: ',num2str(simulationID)])
                    continue
                end

                if thisSim == 1
                    % need to start a new script
                    % first sim on this script 

                    % copy the template file to the new script
                    fidTemplate = fopen('itertemp.slurm','r');
                    fidNew = fopen(sprintf('scripts/netpynerun%04d.slurm',thisScript),'w');

                    while (~feof(fidTemplate))
                        fprintf(fidNew,'%s',fgets(fidTemplate));
                    end
                    fprintf(fidNew,'\n%s%i_%04d',jobPre,floor(complexity),thisScript);
                    fprintf(fidNew,'\n%snetpynerun%04d.out\n',outPre,thisScript);
                    fprintf(fidNew,'\n%snetpynerun%04d.err\n',errPre,thisScript);
                    fprintf(fidNew,'\n%s\n',cdLine);

                    fclose(fidTemplate);
                elseif thisSim < numSimsPerScript
                    % add this sim to the current script


                else    % thisSim >= numSimsPerScript
                    % last sim on this script

                    thisSim = 0; % will increment at the end of this loop
                end
                simsOnScript = simsOnScript + 1;
                fprintf(fidNew,'\n%s %i %g %.2g %.2g %i %i %i\n',preLine,...
                    inputFreqVal(i),...
                    inputNoiseVal(j),...
                    projModVal(l),...
                    EPSPModVal(m),...
                    connModVal(n),...
                    (k-1));

                if thisSim == 0 % if it is the last sim on this script, run
                    fclose(fidNew);
                    [~,temp] = system(sprintf('sbatch scripts/netpynerun%04d.slurm',thisScript));
                    disp(sprintf('%d %s',thisScript,temp))
                    disp([thisScript simsOnScript])
                    thisScript = thisScript + 1; % wrote a new script
                    simsOnScript = 0;
                end

                thisSim = thisSim + 1; % increment the current sim after writing to script
            end
            
            l = 5;
            n = 5;
            for m = 1:EPSPModSet
                simulationID = [inputFreqVal(i),...
                    inputNoiseVal(j),...
                    projModVal(l),...
                    EPSPModVal(m),...
                    connModVal(n),...
                    (k-1)];
                if ismember(simulationID,completedSets,'rows')
                    disp(['This sim already compiled: ',num2str(simulationID)])
                    continue
                end

                if thisSim == 1
                    % need to start a new script
                    % first sim on this script 

                    % copy the template file to the new script
                    fidTemplate = fopen('itertemp.slurm','r');
                    fidNew = fopen(sprintf('scripts/netpynerun%04d.slurm',thisScript),'w');

                    while (~feof(fidTemplate))
                        fprintf(fidNew,'%s',fgets(fidTemplate));
                    end
                    fprintf(fidNew,'\n%s%i_%04d',jobPre,floor(complexity),thisScript);
                    fprintf(fidNew,'\n%snetpynerun%04d.out\n',outPre,thisScript);
                    fprintf(fidNew,'\n%snetpynerun%04d.err\n',errPre,thisScript);
                    fprintf(fidNew,'\n%s\n',cdLine);

                    fclose(fidTemplate);
                elseif thisSim < numSimsPerScript
                    % add this sim to the current script


                else    % thisSim >= numSimsPerScript
                    % last sim on this script

                    thisSim = 0; % will increment at the end of this loop
                end
                simsOnScript = simsOnScript + 1;
                fprintf(fidNew,'\n%s %i %g %.2g %.2g %i %i %i\n',preLine,...
                    inputFreqVal(i),...
                    inputNoiseVal(j),...
                    projModVal(l),...
                    EPSPModVal(m),...
                    connModVal(n),...
                    (k-1));

                if thisSim == 0 % if it is the last sim on this script, run
                    fclose(fidNew);
                    [~,temp] = system(sprintf('sbatch scripts/netpynerun%04d.slurm',thisScript));
                    disp(sprintf('%d %s',thisScript,temp))
                    disp([thisScript simsOnScript])
                    thisScript = thisScript + 1; % wrote a new script
                    simsOnScript = 0;
                end

                thisSim = thisSim + 1; % increment the current sim after writing to script
            end
            
            l = 5;
            m = 5;
            for n = 1:connModSet
                simulationID = [inputFreqVal(i),...
                    inputNoiseVal(j),...
                    projModVal(l),...
                    EPSPModVal(m),...
                    connModVal(n),...
                    (k-1)];
                if ismember(simulationID,completedSets,'rows')
                    disp(['This sim already compiled: ',num2str(simulationID)])
                    continue
                end

                if thisSim == 1
                    % need to start a new script
                    % first sim on this script 

                    % copy the template file to the new script
                    fidTemplate = fopen('itertemp.slurm','r');
                    fidNew = fopen(sprintf('scripts/netpynerun%04d.slurm',thisScript),'w');

                    while (~feof(fidTemplate))
                        fprintf(fidNew,'%s',fgets(fidTemplate));
                    end
                    fprintf(fidNew,'\n%s%i_%04d',jobPre,floor(complexity),thisScript);
                    fprintf(fidNew,'\n%snetpynerun%04d.out\n',outPre,thisScript);
                    fprintf(fidNew,'\n%snetpynerun%04d.err\n',errPre,thisScript);
                    fprintf(fidNew,'\n%s\n',cdLine);

                    fclose(fidTemplate);
                elseif thisSim < numSimsPerScript
                    % add this sim to the current script


                else    % thisSim >= numSimsPerScript
                    % last sim on this script

                    thisSim = 0; % will increment at the end of this loop
                end
                simsOnScript = simsOnScript + 1;
                fprintf(fidNew,'\n%s %i %g %.2g %.2g %i %i %i\n',preLine,...
                    inputFreqVal(i),...
                    inputNoiseVal(j),...
                    projModVal(l),...
                    EPSPModVal(m),...
                    connModVal(n),...
                    (k-1));

                if thisSim == 0 % if it is the last sim on this script, run
                    fclose(fidNew);
                    [~,temp] = system(sprintf('sbatch scripts/netpynerun%04d.slurm',thisScript));
                    disp(sprintf('%d %s',thisScript,temp))
                    disp([thisScript simsOnScript])
                    thisScript = thisScript + 1; % wrote a new script
                    simsOnScript = 0;
                end

                thisSim = thisSim + 1; % increment the current sim after writing to script
            end
        end
    end
end

fclose(fidNew);
[~,temp] = system(sprintf('sbatch scripts/netpynerun%04d.slurm',thisScript));
disp(sprintf('%d %s',thisScript,temp))
disp([thisScript simsOnScript])
