close all; clear all;

path1 = pwd;
path2 = 'output_data';
filename = 'inputFreq_inputNoise_projMod_EPSPMod_connMod.json';
text = fileread(fullfile(path1,path2,filename));
value = jsondecode(text);
clear text

%% OUTPUT FREQUENCY x INPUT FREQUENCY for each NOISE LEVEL
figure
axNoise = subplot(1,1,1);
hold on
neuroVal = logical(value.numNeurons == 20000);
simDuVal = logical(value.simDur == 6000);
projModVal = logical(value.projMod == 1);
EPSPModVal = logical(value.EPSPMod == 1);
connModVal = logical(value.connMod == 1);
iF = [1 2 5 10 20 50];
iN = [0 0.2 0.4 0.6 0.8 1.0];
leg = cell(size(iN));
n = zeros(length(iN),length(iF));

clr = round(linspace(1,256,length(iN)));
cmap = colormap(jet);

legendTicks = iN;
legendLabels = cell(length(legendTicks),1);
for i = 1:length(legendTicks)
    legendLabels{i} = sprintf('%i%% Noise',(iN(i)*100));
end

noiseMark = {'-o','--o','-.o','-d','--d','-.d'};
for i = 1:length(iN)
    iNoiseVal = logical(value.inputNoise == iN(i));
    for j = 1:length(iF)
        iFreqVal = logical(value.inputFreq == iF(j)); 
        keep = logical(neuroVal.*simDuVal.*iFreqVal.*iNoiseVal.*projModVal.*EPSPModVal.*connModVal);
        n(i,j) = sum(keep);
        rates(j) = nanmean(value.rateMean(keep));
        rateErr(j) = nanstd(value.rateMean(keep));
    end
    errorbar(iF,rates,rateErr,noiseMark{i},'color',cmap(clr(i),:))
    leg{i} = [num2str(iN(i)*100),' % Noise'];
end
legend(legendLabels,'Location','northwest')
set(axNoise,'XScale','log')
xlabel('Input frequency (Hz)')
ylabel('Mean � std firing rate (Hz)')
title('Noise sensitivity')
ylim([0 10]);%5])
grid on
hold off

%% OUTPUT FREQUENCY x INPUT FREQUENCY for each NOISE LEVEL and for each PROJECTION MODIFIER
figure
axProj = subplot(1,1,1);
hold on
neuroVal = logical(value.numNeurons == 20000);
simDuVal = logical(value.simDur == 6000);
EPSPModVal = logical(value.EPSPMod == 1);
connModVal = logical(value.connMod == 1);
projMod = unique(value.projMod);
iF = [1 2 5 10 20 50];
iN = [0 0.2 0.4 0.6 0.8 1.0];
leg = cell(size(iN));
n = zeros(length(projMod),length(iN),length(iF));

clr = round(linspace(1,256,length(projMod)));
cmap = colormap(jet);

legendTicks = projMod;
legendLabels = cell(length(legendTicks),1);
for i = 1:length(legendTicks)
    legendLabels{i} = sprintf('%.2f',projMod(i));
end

for i = 1:length(iN)
    iNoiseVal = logical(value.inputNoise == iN(i));
    for h = 1:length(projMod)
        projModVal = logical(value.projMod == projMod(h));
        for j = 1:length(iF)
            iFreqVal = logical(value.inputFreq == iF(j)); 
            keep = logical(neuroVal.*simDuVal.*iFreqVal.*iNoiseVal.*projModVal.*EPSPModVal.*connModVal);
            n(h,i,j) = sum(keep);
            rates(j) = nanmean(value.rateMean(keep));
            rateErr(j) = nanstd(value.rateMean(keep));
        end
        errorbar(iF,rates,rateErr,noiseMark{i},'color',cmap(clr(h),:))
        leg{i} = [num2str(iN(i)*100),' % Noise'];
    end
end
legend(legendLabels,'Location','northwest')
set(axProj,'XScale','log')
xlabel('Input frequency (Hz)')
ylabel('Mean � std firing rate (Hz)')
title('Projection distance sensitivity')
ylim([0 10])
grid on
hold off

figure
iNoiseVal = logical(value.inputNoise == 0.4);
iFreqVal = logical(value.inputFreq <= 10);
for h = 1:length(projMod)
    projModVal = logical(value.projMod == projMod(h));
    keep = logical(neuroVal.*simDuVal.*iFreqVal.*projModVal.*iNoiseVal.*EPSPModVal.*connModVal);
    rates(h) = nanmean(value.rateMean(keep));
    rateErr(h) = nanstd(value.rateMean(keep));
end
errorbar(1:length(projMod),rates,rateErr,'kx')
set(gca,'XTickLabel',{'',projMod,''})
axis([0 length(projMod)+1 0 5])
grid on
text(1,4.25,'[1 2 5 10] Hz input with 40% noise')
title('Projection distance sensitivity')
xlabel('Projection scale factor')
ylabel('Mean � std firing rate (Hz)')

%% OUTPUT FREQUENCY x INPUT FREQUENCY for each NOISE LEVEL and for each EPSP MODIFIER
figure
axEPSP = subplot(1,1,1);
hold on
neuroVal = logical(value.numNeurons == 20000);
simDuVal = logical(value.simDur == 6000);
projModVal = logical(value.projMod == 1);
connModVal = logical(value.connMod == 1);
EPSPMod = unique(value.EPSPMod);
iF = [1 2 5 10 20 50];
iN = [0 0.2 0.4 0.6 0.8 1.0];
leg = cell(size(iN));
n = zeros(length(EPSPMod),length(iN),length(iF));

clr = round(linspace(1,256,length(EPSPMod)));
cmap = colormap(jet);

legendTicks = EPSPMod;
legendLabels = cell(length(legendTicks),1);
for i = 1:length(legendTicks)
    legendLabels{i} = sprintf('%.2f',EPSPMod(i));
end
clear rates rateErr
for i = 1:length(iN)
    iNoiseVal = logical(value.inputNoise == iN(i));
    for h = 1:length(EPSPMod)
        EPSPModVal = logical(value.EPSPMod == EPSPMod(h));
        for j = 1:length(iF)
            iFreqVal = logical(value.inputFreq == iF(j)); 
            keep = logical(neuroVal.*simDuVal.*iFreqVal.*iNoiseVal.*projModVal.*EPSPModVal.*connModVal);
            n(h,i,j) = sum(keep);
            rates(j) = nanmean(value.rateMean(keep));
            rateErr(j) = nanstd(value.rateMean(keep));
        end
        errorbar(iF,rates,rateErr,noiseMark{i},'color',cmap(clr(h),:))
        leg{i} = [num2str(iN(i)*100),' % Noise'];
    end
end
legend(legendLabels,'Location','northwest')
set(axEPSP,'XScale','log')
xlabel('Input frequency (Hz)')
ylabel('Mean � std firing rate (Hz)')
title('EPSP strength sensitivity')
ylim([0 15])
grid on
hold off

figure
iNoiseVal = logical(value.inputNoise == 0.4);
iFreqVal = logical(value.inputFreq <= 10);
for h = 1:length(EPSPMod)
    EPSPModVal = logical(value.EPSPMod == EPSPMod(h));
    keep = logical(neuroVal.*simDuVal.*iFreqVal.*projModVal.*iNoiseVal.*EPSPModVal.*connModVal);
    rates(h) = nanmean(value.rateMean(keep));
    rateErr(h) = nanstd(value.rateMean(keep));
end
errorbar(1:length(EPSPMod),rates,rateErr,'kx')
set(gca,'XTickLabel',{'',EPSPMod,''})
axis([0 length(EPSPMod)+1 0 5])
grid on
text(1,4.25,'[1 2 5 10] Hz input with 40% noise')
title('EPSP weight sensitivity')
xlabel('EPSP weight scale factor')
ylabel('Mean � std firing rate (Hz)')

%% OUTPUT FREQUENCY x INPUT FREQUENCY for each NOISE LEVEL and for each CONNECTION MODIFIER
figure
axEPSP = subplot(1,1,1);
hold on
neuroVal = logical(value.numNeurons == 20000);
simDuVal = logical(value.simDur == 6000);
projModVal = logical(value.projMod == 1);
EPSPModVal = logical(value.EPSPMod == 1);
connMod = unique(value.connMod);
iF = [1 2 5 10 20 50];
iN = [0 0.2 0.4 0.6 0.8 1.0];
leg = cell(size(iN));
n = zeros(length(connMod),length(iN),length(iF));

clr = round(linspace(1,256,length(connMod)));
cmap = colormap(jet);

legendTicks = connMod;
legendLabels = cell(length(legendTicks),1);
for i = 1:length(legendTicks)
    legendLabels{i} = sprintf('%.2f',connMod(i));
end
clear rates rateErr
for i = 1:length(iN)
    iNoiseVal = logical(value.inputNoise == iN(i));
    for h = 1:length(connMod)
        connModVal = logical(value.connMod == connMod(h));
        for j = 1:length(iF)
            iFreqVal = logical(value.inputFreq == iF(j)); 
            keep = logical(neuroVal.*simDuVal.*iFreqVal.*iNoiseVal.*projModVal.*EPSPModVal.*connModVal);
            n(h,i,j) = sum(keep);
            rates(j) = nanmean(value.rateMean(keep));
            rateErr(j) = nanstd(value.rateMean(keep));
        end
        errorbar(iF,rates,rateErr,noiseMark{i},'color',cmap(clr(h),:))
        leg{i} = [num2str(iN(i)*100),' % Noise'];
    end
end
legend(legendLabels,'Location','northwest')
set(axEPSP,'XScale','log')
xlabel('Input frequency (Hz)')
ylabel('Mean � std firing rate (Hz)')
title('Number of synapses sensitivity')
ylim([0 15])
grid on
hold off

figure
iNoiseVal = logical(value.inputNoise == 0.4);
iFreqVal = logical(value.inputFreq <= 10);
for h = 1:length(connMod)
    connModVal = logical(value.connMod == connMod(h));
    keep = logical(neuroVal.*simDuVal.*iFreqVal.*projModVal.*iNoiseVal.*EPSPModVal.*connModVal);
    rates(h) = nanmean(value.rateMean(keep));
    rateErr(h) = nanstd(value.rateMean(keep));
    conns(h) = nanmean(value.connsPerCell(keep));
    connsErr(h) = nanstd(value.connsPerCell(keep));
end
errorbar(1:length(connMod),rates,rateErr,'kx')
set(gca,'XTickLabel',{'',connMod,''})
axis([0 length(connMod)+1 0 5])
grid on
text(1,4.25,'[1 2 5 10] Hz input with 40% noise')
title('Number of connections sensitivity')
xlabel('Number of connections scale factor')
ylabel('Mean � std firing rate (Hz)')


keep = logical(neuroVal.*simDuVal.*iFreqVal.*projModVal.*iNoiseVal.*EPSPModVal);
figure
errorbar(1:length(connMod),conns,connsErr,'kx')
set(gca,'XTickLabel',{'',connMod,''})
axis([0 length(connMod)+1 0 6])
grid on
xlabel('Number of connections scale factor')
ylabel('Mean number of connections per cell')