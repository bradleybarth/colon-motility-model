# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 10:18:16 2020

@author: Bradley Barth
"""
from netpyne import specs, sim
import matplotlib.pyplot as plt
import random as rand
from math import exp
import numpy as np

### SWITCHED the role of WEIGHT and STRENGTH

simdur = 100*1e3

N = 10
d = 10/(N-1)
sig1 = 3.5
sig2 = 2
cij = 0.5

rand.seed(3)

netParams = specs.NetParams()

netParams.importCellParams(label='rule', conds= {'cellType': ['cell']},
        fileName='CRcell.py', cellName='CRcell')

netParams.popParams['cell'] = {'cellType': 'cell', 'numCells': N}

iamp = np.zeros(N)
for i in range(N):
   iamp[i] = 12.55 + rand.random()*0.9
#    iamp[i] = 10.55 + rand.random()*0.9
    
netParams.synMechParams['excit'] = {'mod': 'myexpsyn', 'e': 20, 'strength': cij}
netParams.synMechParams['inhib'] = {'mod': 'myexpsyn', 'e': -40, 'strength': cij}
    
ics = []
netcons = []
postsyns = []
mijs = []

excitPairs = []
excitWeights = []
inhibPairs = []
inhibWeights = []
for i in range(netParams.popParams.cell['numCells']):
    netParams.stimSourceParams['input_'+str(i)] = {'type': 'IClamp', 'del': 0, 'dur': simdur, 'amp': iamp[i]}
    netParams.stimTargetParams['input_to_'+str(i)] = {
            'source': 'input_'+str(i),
            'sec':'soma',
            'loc': 0.5,
            'conds': {'cellList': [i]}}

    for j in range(netParams.popParams.cell['numCells']):
        
        if abs(i-j)<(N/2):
            dij = d*abs(i-j)
        else:
            dij = d*(N-abs(i-j)) 
        # print(dij)
    
        Mij = (1-((dij**2)/(sig1**2))) * exp(-1*(dij**2)/(2*(sig2**2)))
        # print(Mij)
    
    
#        postsyn = postcell.syn
        if Mij>=0:
#            postsyn.e = 20
            thisSyn = 'excit'
        else:
#            postsyn.e = -40
            thisSyn = 'inhib'

#        sj = h.NetCon(precell.soma(0.5)._ref_v, postsyn, sec=precell.soma)
        # sj.delay = 0.1

#        postsyn.strength = abs(Mij)/N
        # postsyn.strength = 1
        if i!=j:
            if thisSyn=='excit':
                excitWeights.append(abs(Mij)/N)
                excitPairs.append([i,j])
            else:
                inhibWeights.append(abs(Mij)/N)
                inhibPairs.append([i,j])
            # sj.weight[0] = 1

        #need to save references
        mijs.append(Mij)
#        postsyns.append(postsyn)
#        netcons.append(sj)
            
netParams.connParams['excitatory'] = {            
    'preConds': {'pop': 'cell'},             
    'postConds': {'pop': 'cell'},  
    'cellList': excitPairs,
    'weight': excitWeights,                  
    'delay': 0,                             
    'synMech': 'excit'}     

netParams.connParams['inhibitory'] = {            
    'preConds': {'pop': 'cell'},             
    'postConds': {'pop': 'cell'},  
    'cellList': inhibPairs,
    'weight': inhibWeights,                  
    'delay': 0,                             
    'synMech': 'inhib'}   
        
        
        
simConfig = specs.SimConfig()                   # object of class SimConfig to store simulation configuration

simConfig.hParams['v_init'] = -61
simConfig.duration = simdur                     # Duration of the simulation, in ms
simConfig.dt = 0.025                             # Internal integration timestep to use
simConfig.verbose = False                       # Show detailed messages
simConfig.recordStep = 0.1                      # Step size in ms to save data (e.g. V traces, LFP, etc)
simConfig.recordTraces = {'V_soma': {'sec':'soma','loc':0.5,'var':'v'}}

simConfig.saveJson = False                 # Save params, network and sim output to json file   

#simConfig.analysis['plotRaster'] = {'include': ['allCells'], 'spikeHist': 'overlay'}
#simConfig.analysis['plotTraces'] = {'include': ['allCells'], 'overlay': True, 'oneFigPer':'trace'}

sim.createSimulateAnalyze(netParams, simConfig)
plt.scatter(sim.simData['spkt'],sim.simData['spkid'])
plt.xlim(simdur-100,simdur)