from neuron import h
from CRcell import CRcell
from matplotlib import pyplot as plt
import random as rand
from math import exp
import numpy as np
import json

h.load_file('stdrun.hoc')
rand.seed(3)
axis_font = {'fontname':'Arial', 'size':'14'}

simdur = 100*1e3

N = 400
d = 10/(N-1)
sig1 = 3.5
sig2 = 2
cij = 2.5#0.5


cells = [CRcell(i) for i in range(N)]

iamp = np.zeros(N)
# iamp[0] = 11
# iamp[1] = 0
for i in range(N):
    iamp[i] = 10.55 + rand.random()*0.9

# dij = d*1
# Mij = (1-((dij**2)/(sig1**2))) * exp(-1*(dij**2)/(2*(sig2**2)))

ics = []
netcons = []
postsyns = []
mijs = []
for i, precell in enumerate(cells):
    ic = h.IClamp(precell.soma(0.5))
    ic.amp = iamp[i]
    ic.delay = 0
    ic.dur = simdur
    ics.append(ic)


    for j, postcell in enumerate(cells):

        if abs(i-j)<(N/2):
            dij = d*abs(i-j)
        else:
            dij = d*(N-abs(i-j)) 
        # print(dij)
    
        Mij = (1-((dij**2)/(sig1**2))) * exp(-1*(dij**2)/(2*(sig2**2)))
        # print(Mij)

        postsyn = postcell.syn
        if Mij>=0:
            postsyn.e = 20
        else:
            postsyn.e = -40

        sj = h.NetCon(precell.soma(0.5)._ref_v, postsyn, sec=precell.soma)
        # sj.delay = 0.1

        postsyn.strength = abs(Mij)/N
        # postsyn.strength = 1
        if i==j:
            sj.weight[0] = 0
        else:
            sj.weight[0] = cij
            # sj.weight[0] = 1

        #need to save references
        mijs.append(Mij)
        postsyns.append(postsyn)
        netcons.append(sj)






h.tstop = simdur
t = h.Vector().record(h._ref_t)
h.finitialize(-65)
h.continuerun(h.tstop)

#calculate lfp
#gsyncombo = []
#for thecell in cells:
#    gsyncombo = np.append(gsyncombo, np.array(thecell.gsyn))
#gsync_rshp = gsyncombo.reshape(N,int(simdur/h.dt)+1)
#gmean = np.mean(gsync_rshp, axis=0)
#gsum = np.sum(gsync_rshp, axis=0)
#LFP = gsum/N

#store spike times
spike_times = {cell._gid: list(cell.spike_times) for cell in cells}
spikedata = json.dumps(spike_times, indent=4)
with open('spike_times.json', 'w') as f:
    f.write(spikedata)


# a=plt.figure()
# # plt.plot(t, cells[0].gsyn)
# plt.plot(t,cells[1].gsyn)
# plt.show()


# b=plt.figure()
# plt.plot(t, cells[0].vtrace)
# plt.plot(t, cells[1].vtrace)
# plt.plot(t, cells[2].vtrace)
# plt.show()

#raster plot
c=plt.figure()
for cell in cells:
    if cell.spike_times:
        plt.scatter(cell.spike_times,  [cell._gid]*len(cell.spike_times), s=3, c='black')
plt.xlabel('Time (ms)', **axis_font)
plt.xlim(99900, 100000)
plt.ylabel('j, no neuron', **axis_font)
# plt.plot(t, LFP)
c.show()


# d=plt.figure()
# # plt.plot(t, LFP)
# plt.plot(t, cells[1].gsyn)
# plt.xlabel('Time (ms)', **axis_font)
# plt.ylabel('LFP', **axis_font)
# d.show()