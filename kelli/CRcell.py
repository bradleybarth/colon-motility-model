from neuron import h

class CRcell(object):
    def __init__(self):

        self._define_geometry()
        self._define_biophysics()


        self.vtrace = h.Vector().record(self.soma(0.5)._ref_v)  #track membrane potential

        #exponential synapse on cell to listen/ be capable of receiving synaptic event
        self.syn = h.myexpsyn(self.soma(0.5))
        # self.syn = h.alphasynapse(self.soma(0.5))
        self.gsyn = h.Vector().record(self.syn._ref_g)

        #net con for spike detection for raster plot
        self.nc = h.NetCon(self.soma(0.5)._ref_v, None, sec=self.soma)
        self.spike_times = h.Vector()
        self.nc.record(self.spike_times)

    def _define_geometry(self):
        soma = h.Section(name = 'soma', cell = self)
        # soma.insert('extracellular')
        self.soma = soma

    def _define_biophysics(self):
        self.soma.insert('hh')