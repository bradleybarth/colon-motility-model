TITLE Lees-Green NaV channel

: Author: Bradley Barth (bradley.barth@duke.edu)
: Grill Lab
: Duke University, USA
:
: Adapted from: 
: Edwards and Hirst (2006) J Physiol. 2006 Feb 15;571(Pt 1):179-89.
: https://www.ncbi.nlm.nih.gov/pubmed/16357016


NEURON {
    SUFFIX icc
    NONSPECIFIC_CURRENT Irest, Iprim
    
    RANGE Erest, Grest, Eprim, Gprim, tauA, tauScale
}                     

UNITS {
    (S)     = (siemens)
    (nS)    = (nanosiemens)
    (mV)    = (millivolt)
    (mA)    = (milliamp)	
    (molar) = (1/liter)
    (mM)    = (millimolar)
    FARADAY = (faraday) (millicoulombs)
    R       = (k-mole) (joule/degC)
    
}

PARAMETER {
    v (mV)
    dt (ms)
    
    Erest   = -65 (mV)
    Grest   = 81 (nS)
    Eprim   = -20 (mV)
    Gprim   = 35000 (nS)
    
    tauA    = 150 (ms)    
    tauScale= 1 (1)
    
    area    = 20000 (micron2)

}

ASSIGNED {
    Irest (mA/cm2)
    Iprim (mA/cm2)
    
    Ainf (1)
    Ninf (1)
    tauN (ms)
    
    grest (S/cm2)
    gprim (S/cm2)
    
}

STATE {
    A (1)
    N (1)

}


BREAKPOINT {      
    grest   = (0.1) * Grest / area
    gprim   = (0.1) * Gprim / area
    SOLVE states METHOD cnexp
    Irest   = grest * (v - Erest)
    Iprim   = gprim * A * N * (v - Eprim)
    
}
  
INITIAL {
    rates(v)
    A   = Ainf
    N   = Ninf
    
}

DERIVATIVE states {
    rates(v)
    
    A' = (Ainf - A)/(tauA*tauScale)
    N' = (Ninf - N)/(tauN*tauScale)
    
}

PROCEDURE rates(v (mV)) { 
    UNITSOFF
    Ainf = 1 / (1 + exp(-55.8 - v))
    Ninf = 1/(1 + exp(v + 61.3))
    tauN = 250 + 25750 / (1 + exp(2 * (v + 56.3)))
    UNITSON

}