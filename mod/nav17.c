/* Created by Language version: 7.5.0 */
/* VECTORIZED */
#define NRN_VECTORIZED 1
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "scoplib_ansi.h"
#undef PI
#define nil 0
#include "md1redef.h"
#include "section.h"
#include "nrniv_mf.h"
#include "md2redef.h"
 
#if METHOD3
extern int _method3;
#endif

#if !NRNGPU
#undef exp
#define exp hoc_Exp
extern double hoc_Exp(double);
#endif
 
#define nrn_init _nrn_init__nav17
#define _nrn_initial _nrn_initial__nav17
#define nrn_cur _nrn_cur__nav17
#define _nrn_current _nrn_current__nav17
#define nrn_jacob _nrn_jacob__nav17
#define nrn_state _nrn_state__nav17
#define _net_receive _net_receive__nav17 
#define rates rates__nav17 
#define scheme1 scheme1__nav17 
 
#define _threadargscomma_ _p, _ppvar, _thread, _nt,
#define _threadargsprotocomma_ double* _p, Datum* _ppvar, Datum* _thread, _NrnThread* _nt,
#define _threadargs_ _p, _ppvar, _thread, _nt
#define _threadargsproto_ double* _p, Datum* _ppvar, Datum* _thread, _NrnThread* _nt
 	/*SUPPRESS 761*/
	/*SUPPRESS 762*/
	/*SUPPRESS 763*/
	/*SUPPRESS 765*/
	 extern double *getarg();
 /* Thread safe. No static _p or _ppvar. */
 
#define t _nt->_t
#define dt _nt->_dt
#define gnabar _p[0]
#define alphaD _p[1]
#define betaD _p[2]
#define mam _p[3]
#define mah _p[4]
#define mas _p[5]
#define sam _p[6]
#define sah _p[7]
#define sas _p[8]
#define mbm _p[9]
#define sbm _p[10]
#define jina17 _p[11]
#define gna _p[12]
#define ina _p[13]
#define O _p[14]
#define C1 _p[15]
#define C2 _p[16]
#define C3 _p[17]
#define I _p[18]
#define I1 _p[19]
#define I2 _p[20]
#define I3 _p[21]
#define I10S _p[22]
#define I11S _p[23]
#define I12S _p[24]
#define I13S _p[25]
#define I20S _p[26]
#define I21S _p[27]
#define I22S _p[28]
#define I23S _p[29]
#define ID _p[30]
#define ID1 _p[31]
#define ID2 _p[32]
#define ID3 _p[33]
#define ena _p[34]
#define DO _p[35]
#define DC1 _p[36]
#define DC2 _p[37]
#define DC3 _p[38]
#define DI _p[39]
#define DI1 _p[40]
#define DI2 _p[41]
#define DI3 _p[42]
#define DI10S _p[43]
#define DI11S _p[44]
#define DI12S _p[45]
#define DI13S _p[46]
#define DI20S _p[47]
#define DI21S _p[48]
#define DI22S _p[49]
#define DI23S _p[50]
#define DID _p[51]
#define DID1 _p[52]
#define DID2 _p[53]
#define DID3 _p[54]
#define am _p[55]
#define bm _p[56]
#define ah _p[57]
#define bh _p[58]
#define as _p[59]
#define bs _p[60]
#define ad _p[61]
#define bd _p[62]
#define htau _p[63]
#define hinf _p[64]
#define v _p[65]
#define _g _p[66]
#define _ion_ena	*_ppvar[0]._pval
#define _ion_ina	*_ppvar[1]._pval
#define _ion_dinadv	*_ppvar[2]._pval
 
#if MAC
#if !defined(v)
#define v _mlhv
#endif
#if !defined(h)
#define h _mlhh
#endif
#endif
 
#if defined(__cplusplus)
extern "C" {
#endif
 static int hoc_nrnpointerindex =  -1;
 static Datum* _extcall_thread;
 static Prop* _extcall_prop;
 /* external NEURON variables */
 /* declaration of user functions */
 static void _hoc_rates(void);
 static int _mechtype;
extern void _nrn_cacheloop_reg(int, int);
extern void hoc_register_prop_size(int, int, int);
extern void hoc_register_limits(int, HocParmLimits*);
extern void hoc_register_units(int, HocParmUnits*);
extern void nrn_promote(Prop*, int, int);
extern Memb_func* memb_func;
 extern void _nrn_setdata_reg(int, void(*)(Prop*));
 static void _setdata(Prop* _prop) {
 _extcall_prop = _prop;
 }
 static void _hoc_setdata() {
 Prop *_prop, *hoc_getdata_range(int);
 _prop = hoc_getdata_range(_mechtype);
   _setdata(_prop);
 hoc_retpushx(1.);
}
 /* connect user functions to hoc names */
 static VoidFunc hoc_intfunc[] = {
 "setdata_nav17", _hoc_setdata,
 "rates_nav17", _hoc_rates,
 0, 0
};
 /* declare global and static user variables */
 /* some parameters have upper and lower limits */
 static HocParmLimits _hoc_parm_limits[] = {
 0,0,0
};
 static HocParmUnits _hoc_parm_units[] = {
 "gnabar_nav17", "mho/cm2",
 "alphaD_nav17", "/ms",
 "betaD_nav17", "/ms",
 "mam_nav17", "mV",
 "mah_nav17", "mV",
 "mas_nav17", "mV",
 "sam_nav17", "mV",
 "sah_nav17", "mV",
 "sas_nav17", "mV",
 "mbm_nav17", "mV",
 "sbm_nav17", "mV",
 "jina17_nav17", "mA/cm2",
 "gna_nav17", "mho/cm2",
 "ina_nav17", "mA/cm2",
 0,0
};
 static double C30 = 0;
 static double C20 = 0;
 static double C10 = 0;
 static double ID30 = 0;
 static double ID20 = 0;
 static double ID10 = 0;
 static double ID0 = 0;
 static double I23S0 = 0;
 static double I22S0 = 0;
 static double I21S0 = 0;
 static double I20S0 = 0;
 static double I13S0 = 0;
 static double I12S0 = 0;
 static double I11S0 = 0;
 static double I10S0 = 0;
 static double I30 = 0;
 static double I20 = 0;
 static double I10 = 0;
 static double I0 = 0;
 static double O0 = 0;
 static double delta_t = 1;
 /* connect global user variables to hoc */
 static DoubScal hoc_scdoub[] = {
 0,0
};
 static DoubVec hoc_vdoub[] = {
 0,0,0
};
 static double _sav_indep;
 static void nrn_alloc(Prop*);
static void  nrn_init(_NrnThread*, _Memb_list*, int);
static void nrn_state(_NrnThread*, _Memb_list*, int);
 static void nrn_cur(_NrnThread*, _Memb_list*, int);
static void  nrn_jacob(_NrnThread*, _Memb_list*, int);
 
static int _ode_count(int);
static void _ode_map(int, double**, double**, double*, Datum*, double*, int);
static void _ode_spec(_NrnThread*, _Memb_list*, int);
static void _ode_matsol(_NrnThread*, _Memb_list*, int);
 
#define _cvode_ieq _ppvar[3]._i
 static void _ode_matsol_instance1(_threadargsproto_);
 /* connect range variables in _p that hoc is supposed to know about */
 static const char *_mechanism[] = {
 "7.5.0",
"nav17",
 "gnabar_nav17",
 "alphaD_nav17",
 "betaD_nav17",
 "mam_nav17",
 "mah_nav17",
 "mas_nav17",
 "sam_nav17",
 "sah_nav17",
 "sas_nav17",
 "mbm_nav17",
 "sbm_nav17",
 "jina17_nav17",
 0,
 "gna_nav17",
 "ina_nav17",
 0,
 "O_nav17",
 "C1_nav17",
 "C2_nav17",
 "C3_nav17",
 "I_nav17",
 "I1_nav17",
 "I2_nav17",
 "I3_nav17",
 "I10S_nav17",
 "I11S_nav17",
 "I12S_nav17",
 "I13S_nav17",
 "I20S_nav17",
 "I21S_nav17",
 "I22S_nav17",
 "I23S_nav17",
 "ID_nav17",
 "ID1_nav17",
 "ID2_nav17",
 "ID3_nav17",
 0,
 0};
 static Symbol* _na_sym;
 
extern Prop* need_memb(Symbol*);

static void nrn_alloc(Prop* _prop) {
	Prop *prop_ion;
	double *_p; Datum *_ppvar;
 	_p = nrn_prop_data_alloc(_mechtype, 67, _prop);
 	/*initialize range parameters*/
 	gnabar = 0.1;
 	alphaD = 0.05;
 	betaD = 0.02;
 	mam = 5;
 	mah = 122.35;
 	mas = 93.9;
 	sam = -12.08;
 	sah = 15.29;
 	sas = 16.6;
 	mbm = 72.7;
 	sbm = 16.7;
 	jina17 = 0;
 	_prop->param = _p;
 	_prop->param_size = 67;
 	_ppvar = nrn_prop_datum_alloc(_mechtype, 4, _prop);
 	_prop->dparam = _ppvar;
 	/*connect ionic variables to this model*/
 prop_ion = need_memb(_na_sym);
 nrn_promote(prop_ion, 0, 1);
 	_ppvar[0]._pval = &prop_ion->param[0]; /* ena */
 	_ppvar[1]._pval = &prop_ion->param[3]; /* ina */
 	_ppvar[2]._pval = &prop_ion->param[4]; /* _ion_dinadv */
 
}
 static void _initlists();
  /* some states have an absolute tolerance */
 static Symbol** _atollist;
 static HocStateTolerance _hoc_state_tol[] = {
 0,0
};
 static void _thread_cleanup(Datum*);
 static void _update_ion_pointer(Datum*);
 extern Symbol* hoc_lookup(const char*);
extern void _nrn_thread_reg(int, int, void(*)(Datum*));
extern void _nrn_thread_table_reg(int, void(*)(double*, Datum*, Datum*, _NrnThread*, int));
extern void hoc_register_tolerance(int, HocStateTolerance*, Symbol***);
extern void _cvode_abstol( Symbol**, double*, int);

 void _nav17_reg() {
	int _vectorized = 1;
  _initlists();
 	ion_reg("na", 1.0);
 	_na_sym = hoc_lookup("na_ion");
 	register_mech(_mechanism, nrn_alloc,nrn_cur, nrn_jacob, nrn_state, nrn_init, hoc_nrnpointerindex, 3);
  _extcall_thread = (Datum*)ecalloc(2, sizeof(Datum));
 _mechtype = nrn_get_mechtype(_mechanism[1]);
     _nrn_setdata_reg(_mechtype, _setdata);
     _nrn_thread_reg(_mechtype, 0, _thread_cleanup);
     _nrn_thread_reg(_mechtype, 2, _update_ion_pointer);
  hoc_register_prop_size(_mechtype, 67, 4);
  hoc_register_dparam_semantics(_mechtype, 0, "na_ion");
  hoc_register_dparam_semantics(_mechtype, 1, "na_ion");
  hoc_register_dparam_semantics(_mechtype, 2, "na_ion");
  hoc_register_dparam_semantics(_mechtype, 3, "cvodeieq");
 	hoc_register_cvode(_mechtype, _ode_count, _ode_map, _ode_spec, _ode_matsol);
 	hoc_register_tolerance(_mechtype, _hoc_state_tol, &_atollist);
 	hoc_register_var(hoc_scdoub, hoc_vdoub, hoc_intfunc);
 	ivoc_help("help ?1 nav17 C:/Users/Bradley Barth/Box/Home Folder bbb16/Sync/School/grill_lab/projects/ex-vivo-colon-motility/gitlab-repository/mod/nav17.mod\n");
 hoc_register_limits(_mechtype, _hoc_parm_limits);
 hoc_register_units(_mechtype, _hoc_parm_units);
 }
static int _reset;
static char *modelname = "nav17.mod  ";

static int error;
static int _ninits = 0;
static int _match_recurse=1;
static void _modl_cleanup(){ _match_recurse=1;}
static int rates(_threadargsprotocomma_ double);
 extern double *_nrn_thread_getelm();
 
#define _MATELM1(_row,_col) *(_nrn_thread_getelm(_so, _row + 1, _col + 1))
 
#define _RHS1(_arg) _rhs[_arg+1]
  
#define _linmat1  1
 static int _spth1 = 1;
 static int _cvspth1 = 0;
 
static int _ode_spec1(_threadargsproto_);
/*static int _ode_matsol1(_threadargsproto_);*/
 static int _slist1[20], _dlist1[20]; static double *_temp1;
 static int scheme1();
 
static int scheme1 (void* _so, double* _rhs, double* _p, Datum* _ppvar, Datum* _thread, _NrnThread* _nt)
 {int _reset=0;
 {
   double b_flux, f_flux, _term; int _i;
 {int _i; double _dt1 = 1.0/dt;
for(_i=1;_i<20;_i++){
  	_RHS1(_i) = -_dt1*(_p[_slist1[_i]] - _p[_dlist1[_i]]);
	_MATELM1(_i, _i) = _dt1;
      
} }
 rates ( _threadargscomma_ v ) ;
   /* ~ O <-> C1 ( 3.0 * bm , am )*/
 f_flux =  3.0 * bm * O ;
 b_flux =  am * C1 ;
 _RHS1( 19) -= (f_flux - b_flux);
 _RHS1( 3) += (f_flux - b_flux);
 
 _term =  3.0 * bm ;
 _MATELM1( 19 ,19)  += _term;
 _MATELM1( 3 ,19)  -= _term;
 _term =  am ;
 _MATELM1( 19 ,3)  -= _term;
 _MATELM1( 3 ,3)  += _term;
 /*REACTION*/
  /* ~ O <-> I ( bh , ah )*/
 f_flux =  bh * O ;
 b_flux =  ah * I ;
 _RHS1( 19) -= (f_flux - b_flux);
 _RHS1( 18) += (f_flux - b_flux);
 
 _term =  bh ;
 _MATELM1( 19 ,19)  += _term;
 _MATELM1( 18 ,19)  -= _term;
 _term =  ah ;
 _MATELM1( 19 ,18)  -= _term;
 _MATELM1( 18 ,18)  += _term;
 /*REACTION*/
  /* ~ O <-> I10S ( bs , as )*/
 f_flux =  bs * O ;
 b_flux =  as * I10S ;
 _RHS1( 19) -= (f_flux - b_flux);
 _RHS1( 14) += (f_flux - b_flux);
 
 _term =  bs ;
 _MATELM1( 19 ,19)  += _term;
 _MATELM1( 14 ,19)  -= _term;
 _term =  as ;
 _MATELM1( 19 ,14)  -= _term;
 _MATELM1( 14 ,14)  += _term;
 /*REACTION*/
  /* ~ C1 <-> C2 ( 2.0 * bm , 2.0 * am )*/
 f_flux =  2.0 * bm * C1 ;
 b_flux =  2.0 * am * C2 ;
 _RHS1( 3) -= (f_flux - b_flux);
 _RHS1( 2) += (f_flux - b_flux);
 
 _term =  2.0 * bm ;
 _MATELM1( 3 ,3)  += _term;
 _MATELM1( 2 ,3)  -= _term;
 _term =  2.0 * am ;
 _MATELM1( 3 ,2)  -= _term;
 _MATELM1( 2 ,2)  += _term;
 /*REACTION*/
  /* ~ C1 <-> I1 ( bh , ah )*/
 f_flux =  bh * C1 ;
 b_flux =  ah * I1 ;
 _RHS1( 3) -= (f_flux - b_flux);
 _RHS1( 17) += (f_flux - b_flux);
 
 _term =  bh ;
 _MATELM1( 3 ,3)  += _term;
 _MATELM1( 17 ,3)  -= _term;
 _term =  ah ;
 _MATELM1( 3 ,17)  -= _term;
 _MATELM1( 17 ,17)  += _term;
 /*REACTION*/
  /* ~ C1 <-> I11S ( bs , as )*/
 f_flux =  bs * C1 ;
 b_flux =  as * I11S ;
 _RHS1( 3) -= (f_flux - b_flux);
 _RHS1( 13) += (f_flux - b_flux);
 
 _term =  bs ;
 _MATELM1( 3 ,3)  += _term;
 _MATELM1( 13 ,3)  -= _term;
 _term =  as ;
 _MATELM1( 3 ,13)  -= _term;
 _MATELM1( 13 ,13)  += _term;
 /*REACTION*/
  /* ~ C2 <-> C3 ( bm , 3.0 * am )*/
 f_flux =  bm * C2 ;
 b_flux =  3.0 * am * C3 ;
 _RHS1( 2) -= (f_flux - b_flux);
 _RHS1( 1) += (f_flux - b_flux);
 
 _term =  bm ;
 _MATELM1( 2 ,2)  += _term;
 _MATELM1( 1 ,2)  -= _term;
 _term =  3.0 * am ;
 _MATELM1( 2 ,1)  -= _term;
 _MATELM1( 1 ,1)  += _term;
 /*REACTION*/
  /* ~ C2 <-> I2 ( bh , ah )*/
 f_flux =  bh * C2 ;
 b_flux =  ah * I2 ;
 _RHS1( 2) -= (f_flux - b_flux);
 _RHS1( 16) += (f_flux - b_flux);
 
 _term =  bh ;
 _MATELM1( 2 ,2)  += _term;
 _MATELM1( 16 ,2)  -= _term;
 _term =  ah ;
 _MATELM1( 2 ,16)  -= _term;
 _MATELM1( 16 ,16)  += _term;
 /*REACTION*/
  /* ~ C2 <-> I12S ( bs , as )*/
 f_flux =  bs * C2 ;
 b_flux =  as * I12S ;
 _RHS1( 2) -= (f_flux - b_flux);
 _RHS1( 12) += (f_flux - b_flux);
 
 _term =  bs ;
 _MATELM1( 2 ,2)  += _term;
 _MATELM1( 12 ,2)  -= _term;
 _term =  as ;
 _MATELM1( 2 ,12)  -= _term;
 _MATELM1( 12 ,12)  += _term;
 /*REACTION*/
  /* ~ C3 <-> I3 ( bh , ah )*/
 f_flux =  bh * C3 ;
 b_flux =  ah * I3 ;
 _RHS1( 1) -= (f_flux - b_flux);
 _RHS1( 15) += (f_flux - b_flux);
 
 _term =  bh ;
 _MATELM1( 1 ,1)  += _term;
 _MATELM1( 15 ,1)  -= _term;
 _term =  ah ;
 _MATELM1( 1 ,15)  -= _term;
 _MATELM1( 15 ,15)  += _term;
 /*REACTION*/
  /* ~ C3 <-> I13S ( bs , as )*/
 f_flux =  bs * C3 ;
 b_flux =  as * I13S ;
 _RHS1( 1) -= (f_flux - b_flux);
 _RHS1( 11) += (f_flux - b_flux);
 
 _term =  bs ;
 _MATELM1( 1 ,1)  += _term;
 _MATELM1( 11 ,1)  -= _term;
 _term =  as ;
 _MATELM1( 1 ,11)  -= _term;
 _MATELM1( 11 ,11)  += _term;
 /*REACTION*/
  /* ~ I <-> I1 ( 3.0 * bm , am )*/
 f_flux =  3.0 * bm * I ;
 b_flux =  am * I1 ;
 _RHS1( 18) -= (f_flux - b_flux);
 _RHS1( 17) += (f_flux - b_flux);
 
 _term =  3.0 * bm ;
 _MATELM1( 18 ,18)  += _term;
 _MATELM1( 17 ,18)  -= _term;
 _term =  am ;
 _MATELM1( 18 ,17)  -= _term;
 _MATELM1( 17 ,17)  += _term;
 /*REACTION*/
  /* ~ I <-> I20S ( bs , as )*/
 f_flux =  bs * I ;
 b_flux =  as * I20S ;
 _RHS1( 18) -= (f_flux - b_flux);
 _RHS1( 10) += (f_flux - b_flux);
 
 _term =  bs ;
 _MATELM1( 18 ,18)  += _term;
 _MATELM1( 10 ,18)  -= _term;
 _term =  as ;
 _MATELM1( 18 ,10)  -= _term;
 _MATELM1( 10 ,10)  += _term;
 /*REACTION*/
  /* ~ I <-> ID ( bd , ad )*/
 f_flux =  bd * I ;
 b_flux =  ad * ID ;
 _RHS1( 18) -= (f_flux - b_flux);
 _RHS1( 6) += (f_flux - b_flux);
 
 _term =  bd ;
 _MATELM1( 18 ,18)  += _term;
 _MATELM1( 6 ,18)  -= _term;
 _term =  ad ;
 _MATELM1( 18 ,6)  -= _term;
 _MATELM1( 6 ,6)  += _term;
 /*REACTION*/
  /* ~ I1 <-> I2 ( 2.0 * bm , 2.0 * am )*/
 f_flux =  2.0 * bm * I1 ;
 b_flux =  2.0 * am * I2 ;
 _RHS1( 17) -= (f_flux - b_flux);
 _RHS1( 16) += (f_flux - b_flux);
 
 _term =  2.0 * bm ;
 _MATELM1( 17 ,17)  += _term;
 _MATELM1( 16 ,17)  -= _term;
 _term =  2.0 * am ;
 _MATELM1( 17 ,16)  -= _term;
 _MATELM1( 16 ,16)  += _term;
 /*REACTION*/
  /* ~ I1 <-> ID1 ( bd , ad )*/
 f_flux =  bd * I1 ;
 b_flux =  ad * ID1 ;
 _RHS1( 17) -= (f_flux - b_flux);
 _RHS1( 5) += (f_flux - b_flux);
 
 _term =  bd ;
 _MATELM1( 17 ,17)  += _term;
 _MATELM1( 5 ,17)  -= _term;
 _term =  ad ;
 _MATELM1( 17 ,5)  -= _term;
 _MATELM1( 5 ,5)  += _term;
 /*REACTION*/
  /* ~ I1 <-> I21S ( bs , as )*/
 f_flux =  bs * I1 ;
 b_flux =  as * I21S ;
 _RHS1( 17) -= (f_flux - b_flux);
 _RHS1( 9) += (f_flux - b_flux);
 
 _term =  bs ;
 _MATELM1( 17 ,17)  += _term;
 _MATELM1( 9 ,17)  -= _term;
 _term =  as ;
 _MATELM1( 17 ,9)  -= _term;
 _MATELM1( 9 ,9)  += _term;
 /*REACTION*/
  /* ~ I2 <-> I3 ( bm , 3.0 * am )*/
 f_flux =  bm * I2 ;
 b_flux =  3.0 * am * I3 ;
 _RHS1( 16) -= (f_flux - b_flux);
 _RHS1( 15) += (f_flux - b_flux);
 
 _term =  bm ;
 _MATELM1( 16 ,16)  += _term;
 _MATELM1( 15 ,16)  -= _term;
 _term =  3.0 * am ;
 _MATELM1( 16 ,15)  -= _term;
 _MATELM1( 15 ,15)  += _term;
 /*REACTION*/
  /* ~ I2 <-> ID2 ( bd , ad )*/
 f_flux =  bd * I2 ;
 b_flux =  ad * ID2 ;
 _RHS1( 16) -= (f_flux - b_flux);
 _RHS1( 4) += (f_flux - b_flux);
 
 _term =  bd ;
 _MATELM1( 16 ,16)  += _term;
 _MATELM1( 4 ,16)  -= _term;
 _term =  ad ;
 _MATELM1( 16 ,4)  -= _term;
 _MATELM1( 4 ,4)  += _term;
 /*REACTION*/
  /* ~ I2 <-> I22S ( bs , as )*/
 f_flux =  bs * I2 ;
 b_flux =  as * I22S ;
 _RHS1( 16) -= (f_flux - b_flux);
 _RHS1( 8) += (f_flux - b_flux);
 
 _term =  bs ;
 _MATELM1( 16 ,16)  += _term;
 _MATELM1( 8 ,16)  -= _term;
 _term =  as ;
 _MATELM1( 16 ,8)  -= _term;
 _MATELM1( 8 ,8)  += _term;
 /*REACTION*/
  /* ~ I3 <-> ID3 ( bd , ad )*/
 f_flux =  bd * I3 ;
 b_flux =  ad * ID3 ;
 _RHS1( 15) -= (f_flux - b_flux);
 
 _term =  bd ;
 _MATELM1( 15 ,15)  += _term;
 _term =  ad ;
 _MATELM1( 15 ,0)  -= _term;
 /*REACTION*/
  /* ~ ID <-> ID1 ( 3.0 * bm , am )*/
 f_flux =  3.0 * bm * ID ;
 b_flux =  am * ID1 ;
 _RHS1( 6) -= (f_flux - b_flux);
 _RHS1( 5) += (f_flux - b_flux);
 
 _term =  3.0 * bm ;
 _MATELM1( 6 ,6)  += _term;
 _MATELM1( 5 ,6)  -= _term;
 _term =  am ;
 _MATELM1( 6 ,5)  -= _term;
 _MATELM1( 5 ,5)  += _term;
 /*REACTION*/
  /* ~ ID1 <-> ID2 ( 2.0 * bm , 2.0 * am )*/
 f_flux =  2.0 * bm * ID1 ;
 b_flux =  2.0 * am * ID2 ;
 _RHS1( 5) -= (f_flux - b_flux);
 _RHS1( 4) += (f_flux - b_flux);
 
 _term =  2.0 * bm ;
 _MATELM1( 5 ,5)  += _term;
 _MATELM1( 4 ,5)  -= _term;
 _term =  2.0 * am ;
 _MATELM1( 5 ,4)  -= _term;
 _MATELM1( 4 ,4)  += _term;
 /*REACTION*/
  /* ~ ID2 <-> ID3 ( bm , 3.0 * am )*/
 f_flux =  bm * ID2 ;
 b_flux =  3.0 * am * ID3 ;
 _RHS1( 4) -= (f_flux - b_flux);
 
 _term =  bm ;
 _MATELM1( 4 ,4)  += _term;
 _term =  3.0 * am ;
 _MATELM1( 4 ,0)  -= _term;
 /*REACTION*/
  /* ~ I10S <-> I20S ( bh , ah )*/
 f_flux =  bh * I10S ;
 b_flux =  ah * I20S ;
 _RHS1( 14) -= (f_flux - b_flux);
 _RHS1( 10) += (f_flux - b_flux);
 
 _term =  bh ;
 _MATELM1( 14 ,14)  += _term;
 _MATELM1( 10 ,14)  -= _term;
 _term =  ah ;
 _MATELM1( 14 ,10)  -= _term;
 _MATELM1( 10 ,10)  += _term;
 /*REACTION*/
  /* ~ I11S <-> I21S ( bh , ah )*/
 f_flux =  bh * I11S ;
 b_flux =  ah * I21S ;
 _RHS1( 13) -= (f_flux - b_flux);
 _RHS1( 9) += (f_flux - b_flux);
 
 _term =  bh ;
 _MATELM1( 13 ,13)  += _term;
 _MATELM1( 9 ,13)  -= _term;
 _term =  ah ;
 _MATELM1( 13 ,9)  -= _term;
 _MATELM1( 9 ,9)  += _term;
 /*REACTION*/
  /* ~ I12S <-> I22S ( bh , ah )*/
 f_flux =  bh * I12S ;
 b_flux =  ah * I22S ;
 _RHS1( 12) -= (f_flux - b_flux);
 _RHS1( 8) += (f_flux - b_flux);
 
 _term =  bh ;
 _MATELM1( 12 ,12)  += _term;
 _MATELM1( 8 ,12)  -= _term;
 _term =  ah ;
 _MATELM1( 12 ,8)  -= _term;
 _MATELM1( 8 ,8)  += _term;
 /*REACTION*/
  /* ~ I13S <-> I23S ( bh , ah )*/
 f_flux =  bh * I13S ;
 b_flux =  ah * I23S ;
 _RHS1( 11) -= (f_flux - b_flux);
 _RHS1( 7) += (f_flux - b_flux);
 
 _term =  bh ;
 _MATELM1( 11 ,11)  += _term;
 _MATELM1( 7 ,11)  -= _term;
 _term =  ah ;
 _MATELM1( 11 ,7)  -= _term;
 _MATELM1( 7 ,7)  += _term;
 /*REACTION*/
   /* O + C1 + C2 + C3 + I + I1 + I2 + I3 + I10S + I11S + I12S + I13S + I20S + I21S + I22S + I23S + ID + ID1 + ID2 + ID3 = 1.0 */
 _RHS1(0) =  1.0;
 _MATELM1(0, 0) = 1;
 _RHS1(0) -= ID3 ;
 _MATELM1(0, 4) = 1;
 _RHS1(0) -= ID2 ;
 _MATELM1(0, 5) = 1;
 _RHS1(0) -= ID1 ;
 _MATELM1(0, 6) = 1;
 _RHS1(0) -= ID ;
 _MATELM1(0, 7) = 1;
 _RHS1(0) -= I23S ;
 _MATELM1(0, 8) = 1;
 _RHS1(0) -= I22S ;
 _MATELM1(0, 9) = 1;
 _RHS1(0) -= I21S ;
 _MATELM1(0, 10) = 1;
 _RHS1(0) -= I20S ;
 _MATELM1(0, 11) = 1;
 _RHS1(0) -= I13S ;
 _MATELM1(0, 12) = 1;
 _RHS1(0) -= I12S ;
 _MATELM1(0, 13) = 1;
 _RHS1(0) -= I11S ;
 _MATELM1(0, 14) = 1;
 _RHS1(0) -= I10S ;
 _MATELM1(0, 15) = 1;
 _RHS1(0) -= I3 ;
 _MATELM1(0, 16) = 1;
 _RHS1(0) -= I2 ;
 _MATELM1(0, 17) = 1;
 _RHS1(0) -= I1 ;
 _MATELM1(0, 18) = 1;
 _RHS1(0) -= I ;
 _MATELM1(0, 1) = 1;
 _RHS1(0) -= C3 ;
 _MATELM1(0, 2) = 1;
 _RHS1(0) -= C2 ;
 _MATELM1(0, 3) = 1;
 _RHS1(0) -= C1 ;
 _MATELM1(0, 19) = 1;
 _RHS1(0) -= O ;
 /*CONSERVATION*/
   } return _reset;
 }
 
static int  rates ( _threadargsprotocomma_ double _lv ) {
   am = 15.5 / ( 1.0 + exp ( ( _lv - mam ) / ( sam ) ) ) ;
   bm = 35.2 / ( 1.0 + exp ( ( _lv + mbm ) / sbm ) ) ;
   ah = 0.38685 / ( 1.0 + exp ( ( _lv + mah ) / sah ) ) ;
   bh = - .00283 + 2.00283 / ( 1.0 + exp ( ( _lv + 5.5266 ) / ( - 12.70195 ) ) ) ;
   as = .00003 + ( .00092 ) / ( 1.0 + exp ( ( _lv + mas ) / sas ) ) ;
   bs = 132.05 - ( 132.05 ) / ( 1.0 + exp ( ( _lv - 384.9 ) / 28.5 ) ) ;
   ad = alphaD ;
   bd = betaD ;
    return 0; }
 
static void _hoc_rates(void) {
  double _r;
   double* _p; Datum* _ppvar; Datum* _thread; _NrnThread* _nt;
   if (_extcall_prop) {_p = _extcall_prop->param; _ppvar = _extcall_prop->dparam;}else{ _p = (double*)0; _ppvar = (Datum*)0; }
  _thread = _extcall_thread;
  _nt = nrn_threads;
 _r = 1.;
 rates ( _p, _ppvar, _thread, _nt, *getarg(1) );
 hoc_retpushx(_r);
}
 
/*CVODE ode begin*/
 static int _ode_spec1(double* _p, Datum* _ppvar, Datum* _thread, _NrnThread* _nt) {int _reset=0;{
 double b_flux, f_flux, _term; int _i;
 {int _i; for(_i=0;_i<20;_i++) _p[_dlist1[_i]] = 0.0;}
 rates ( _threadargscomma_ v ) ;
 /* ~ O <-> C1 ( 3.0 * bm , am )*/
 f_flux =  3.0 * bm * O ;
 b_flux =  am * C1 ;
 DO -= (f_flux - b_flux);
 DC1 += (f_flux - b_flux);
 
 /*REACTION*/
  /* ~ O <-> I ( bh , ah )*/
 f_flux =  bh * O ;
 b_flux =  ah * I ;
 DO -= (f_flux - b_flux);
 DI += (f_flux - b_flux);
 
 /*REACTION*/
  /* ~ O <-> I10S ( bs , as )*/
 f_flux =  bs * O ;
 b_flux =  as * I10S ;
 DO -= (f_flux - b_flux);
 DI10S += (f_flux - b_flux);
 
 /*REACTION*/
  /* ~ C1 <-> C2 ( 2.0 * bm , 2.0 * am )*/
 f_flux =  2.0 * bm * C1 ;
 b_flux =  2.0 * am * C2 ;
 DC1 -= (f_flux - b_flux);
 DC2 += (f_flux - b_flux);
 
 /*REACTION*/
  /* ~ C1 <-> I1 ( bh , ah )*/
 f_flux =  bh * C1 ;
 b_flux =  ah * I1 ;
 DC1 -= (f_flux - b_flux);
 DI1 += (f_flux - b_flux);
 
 /*REACTION*/
  /* ~ C1 <-> I11S ( bs , as )*/
 f_flux =  bs * C1 ;
 b_flux =  as * I11S ;
 DC1 -= (f_flux - b_flux);
 DI11S += (f_flux - b_flux);
 
 /*REACTION*/
  /* ~ C2 <-> C3 ( bm , 3.0 * am )*/
 f_flux =  bm * C2 ;
 b_flux =  3.0 * am * C3 ;
 DC2 -= (f_flux - b_flux);
 DC3 += (f_flux - b_flux);
 
 /*REACTION*/
  /* ~ C2 <-> I2 ( bh , ah )*/
 f_flux =  bh * C2 ;
 b_flux =  ah * I2 ;
 DC2 -= (f_flux - b_flux);
 DI2 += (f_flux - b_flux);
 
 /*REACTION*/
  /* ~ C2 <-> I12S ( bs , as )*/
 f_flux =  bs * C2 ;
 b_flux =  as * I12S ;
 DC2 -= (f_flux - b_flux);
 DI12S += (f_flux - b_flux);
 
 /*REACTION*/
  /* ~ C3 <-> I3 ( bh , ah )*/
 f_flux =  bh * C3 ;
 b_flux =  ah * I3 ;
 DC3 -= (f_flux - b_flux);
 DI3 += (f_flux - b_flux);
 
 /*REACTION*/
  /* ~ C3 <-> I13S ( bs , as )*/
 f_flux =  bs * C3 ;
 b_flux =  as * I13S ;
 DC3 -= (f_flux - b_flux);
 DI13S += (f_flux - b_flux);
 
 /*REACTION*/
  /* ~ I <-> I1 ( 3.0 * bm , am )*/
 f_flux =  3.0 * bm * I ;
 b_flux =  am * I1 ;
 DI -= (f_flux - b_flux);
 DI1 += (f_flux - b_flux);
 
 /*REACTION*/
  /* ~ I <-> I20S ( bs , as )*/
 f_flux =  bs * I ;
 b_flux =  as * I20S ;
 DI -= (f_flux - b_flux);
 DI20S += (f_flux - b_flux);
 
 /*REACTION*/
  /* ~ I <-> ID ( bd , ad )*/
 f_flux =  bd * I ;
 b_flux =  ad * ID ;
 DI -= (f_flux - b_flux);
 DID += (f_flux - b_flux);
 
 /*REACTION*/
  /* ~ I1 <-> I2 ( 2.0 * bm , 2.0 * am )*/
 f_flux =  2.0 * bm * I1 ;
 b_flux =  2.0 * am * I2 ;
 DI1 -= (f_flux - b_flux);
 DI2 += (f_flux - b_flux);
 
 /*REACTION*/
  /* ~ I1 <-> ID1 ( bd , ad )*/
 f_flux =  bd * I1 ;
 b_flux =  ad * ID1 ;
 DI1 -= (f_flux - b_flux);
 DID1 += (f_flux - b_flux);
 
 /*REACTION*/
  /* ~ I1 <-> I21S ( bs , as )*/
 f_flux =  bs * I1 ;
 b_flux =  as * I21S ;
 DI1 -= (f_flux - b_flux);
 DI21S += (f_flux - b_flux);
 
 /*REACTION*/
  /* ~ I2 <-> I3 ( bm , 3.0 * am )*/
 f_flux =  bm * I2 ;
 b_flux =  3.0 * am * I3 ;
 DI2 -= (f_flux - b_flux);
 DI3 += (f_flux - b_flux);
 
 /*REACTION*/
  /* ~ I2 <-> ID2 ( bd , ad )*/
 f_flux =  bd * I2 ;
 b_flux =  ad * ID2 ;
 DI2 -= (f_flux - b_flux);
 DID2 += (f_flux - b_flux);
 
 /*REACTION*/
  /* ~ I2 <-> I22S ( bs , as )*/
 f_flux =  bs * I2 ;
 b_flux =  as * I22S ;
 DI2 -= (f_flux - b_flux);
 DI22S += (f_flux - b_flux);
 
 /*REACTION*/
  /* ~ I3 <-> ID3 ( bd , ad )*/
 f_flux =  bd * I3 ;
 b_flux =  ad * ID3 ;
 DI3 -= (f_flux - b_flux);
 DID3 += (f_flux - b_flux);
 
 /*REACTION*/
  /* ~ ID <-> ID1 ( 3.0 * bm , am )*/
 f_flux =  3.0 * bm * ID ;
 b_flux =  am * ID1 ;
 DID -= (f_flux - b_flux);
 DID1 += (f_flux - b_flux);
 
 /*REACTION*/
  /* ~ ID1 <-> ID2 ( 2.0 * bm , 2.0 * am )*/
 f_flux =  2.0 * bm * ID1 ;
 b_flux =  2.0 * am * ID2 ;
 DID1 -= (f_flux - b_flux);
 DID2 += (f_flux - b_flux);
 
 /*REACTION*/
  /* ~ ID2 <-> ID3 ( bm , 3.0 * am )*/
 f_flux =  bm * ID2 ;
 b_flux =  3.0 * am * ID3 ;
 DID2 -= (f_flux - b_flux);
 DID3 += (f_flux - b_flux);
 
 /*REACTION*/
  /* ~ I10S <-> I20S ( bh , ah )*/
 f_flux =  bh * I10S ;
 b_flux =  ah * I20S ;
 DI10S -= (f_flux - b_flux);
 DI20S += (f_flux - b_flux);
 
 /*REACTION*/
  /* ~ I11S <-> I21S ( bh , ah )*/
 f_flux =  bh * I11S ;
 b_flux =  ah * I21S ;
 DI11S -= (f_flux - b_flux);
 DI21S += (f_flux - b_flux);
 
 /*REACTION*/
  /* ~ I12S <-> I22S ( bh , ah )*/
 f_flux =  bh * I12S ;
 b_flux =  ah * I22S ;
 DI12S -= (f_flux - b_flux);
 DI22S += (f_flux - b_flux);
 
 /*REACTION*/
  /* ~ I13S <-> I23S ( bh , ah )*/
 f_flux =  bh * I13S ;
 b_flux =  ah * I23S ;
 DI13S -= (f_flux - b_flux);
 DI23S += (f_flux - b_flux);
 
 /*REACTION*/
   /* O + C1 + C2 + C3 + I + I1 + I2 + I3 + I10S + I11S + I12S + I13S + I20S + I21S + I22S + I23S + ID + ID1 + ID2 + ID3 = 1.0 */
 /*CONSERVATION*/
   } return _reset;
 }
 
/*CVODE matsol*/
 static int _ode_matsol1(void* _so, double* _rhs, double* _p, Datum* _ppvar, Datum* _thread, _NrnThread* _nt) {int _reset=0;{
 double b_flux, f_flux, _term; int _i;
   b_flux = f_flux = 0.;
 {int _i; double _dt1 = 1.0/dt;
for(_i=0;_i<20;_i++){
  	_RHS1(_i) = _dt1*(_p[_dlist1[_i]]);
	_MATELM1(_i, _i) = _dt1;
      
} }
 rates ( _threadargscomma_ v ) ;
 /* ~ O <-> C1 ( 3.0 * bm , am )*/
 _term =  3.0 * bm ;
 _MATELM1( 19 ,19)  += _term;
 _MATELM1( 3 ,19)  -= _term;
 _term =  am ;
 _MATELM1( 19 ,3)  -= _term;
 _MATELM1( 3 ,3)  += _term;
 /*REACTION*/
  /* ~ O <-> I ( bh , ah )*/
 _term =  bh ;
 _MATELM1( 19 ,19)  += _term;
 _MATELM1( 18 ,19)  -= _term;
 _term =  ah ;
 _MATELM1( 19 ,18)  -= _term;
 _MATELM1( 18 ,18)  += _term;
 /*REACTION*/
  /* ~ O <-> I10S ( bs , as )*/
 _term =  bs ;
 _MATELM1( 19 ,19)  += _term;
 _MATELM1( 14 ,19)  -= _term;
 _term =  as ;
 _MATELM1( 19 ,14)  -= _term;
 _MATELM1( 14 ,14)  += _term;
 /*REACTION*/
  /* ~ C1 <-> C2 ( 2.0 * bm , 2.0 * am )*/
 _term =  2.0 * bm ;
 _MATELM1( 3 ,3)  += _term;
 _MATELM1( 2 ,3)  -= _term;
 _term =  2.0 * am ;
 _MATELM1( 3 ,2)  -= _term;
 _MATELM1( 2 ,2)  += _term;
 /*REACTION*/
  /* ~ C1 <-> I1 ( bh , ah )*/
 _term =  bh ;
 _MATELM1( 3 ,3)  += _term;
 _MATELM1( 17 ,3)  -= _term;
 _term =  ah ;
 _MATELM1( 3 ,17)  -= _term;
 _MATELM1( 17 ,17)  += _term;
 /*REACTION*/
  /* ~ C1 <-> I11S ( bs , as )*/
 _term =  bs ;
 _MATELM1( 3 ,3)  += _term;
 _MATELM1( 13 ,3)  -= _term;
 _term =  as ;
 _MATELM1( 3 ,13)  -= _term;
 _MATELM1( 13 ,13)  += _term;
 /*REACTION*/
  /* ~ C2 <-> C3 ( bm , 3.0 * am )*/
 _term =  bm ;
 _MATELM1( 2 ,2)  += _term;
 _MATELM1( 1 ,2)  -= _term;
 _term =  3.0 * am ;
 _MATELM1( 2 ,1)  -= _term;
 _MATELM1( 1 ,1)  += _term;
 /*REACTION*/
  /* ~ C2 <-> I2 ( bh , ah )*/
 _term =  bh ;
 _MATELM1( 2 ,2)  += _term;
 _MATELM1( 16 ,2)  -= _term;
 _term =  ah ;
 _MATELM1( 2 ,16)  -= _term;
 _MATELM1( 16 ,16)  += _term;
 /*REACTION*/
  /* ~ C2 <-> I12S ( bs , as )*/
 _term =  bs ;
 _MATELM1( 2 ,2)  += _term;
 _MATELM1( 12 ,2)  -= _term;
 _term =  as ;
 _MATELM1( 2 ,12)  -= _term;
 _MATELM1( 12 ,12)  += _term;
 /*REACTION*/
  /* ~ C3 <-> I3 ( bh , ah )*/
 _term =  bh ;
 _MATELM1( 1 ,1)  += _term;
 _MATELM1( 15 ,1)  -= _term;
 _term =  ah ;
 _MATELM1( 1 ,15)  -= _term;
 _MATELM1( 15 ,15)  += _term;
 /*REACTION*/
  /* ~ C3 <-> I13S ( bs , as )*/
 _term =  bs ;
 _MATELM1( 1 ,1)  += _term;
 _MATELM1( 11 ,1)  -= _term;
 _term =  as ;
 _MATELM1( 1 ,11)  -= _term;
 _MATELM1( 11 ,11)  += _term;
 /*REACTION*/
  /* ~ I <-> I1 ( 3.0 * bm , am )*/
 _term =  3.0 * bm ;
 _MATELM1( 18 ,18)  += _term;
 _MATELM1( 17 ,18)  -= _term;
 _term =  am ;
 _MATELM1( 18 ,17)  -= _term;
 _MATELM1( 17 ,17)  += _term;
 /*REACTION*/
  /* ~ I <-> I20S ( bs , as )*/
 _term =  bs ;
 _MATELM1( 18 ,18)  += _term;
 _MATELM1( 10 ,18)  -= _term;
 _term =  as ;
 _MATELM1( 18 ,10)  -= _term;
 _MATELM1( 10 ,10)  += _term;
 /*REACTION*/
  /* ~ I <-> ID ( bd , ad )*/
 _term =  bd ;
 _MATELM1( 18 ,18)  += _term;
 _MATELM1( 6 ,18)  -= _term;
 _term =  ad ;
 _MATELM1( 18 ,6)  -= _term;
 _MATELM1( 6 ,6)  += _term;
 /*REACTION*/
  /* ~ I1 <-> I2 ( 2.0 * bm , 2.0 * am )*/
 _term =  2.0 * bm ;
 _MATELM1( 17 ,17)  += _term;
 _MATELM1( 16 ,17)  -= _term;
 _term =  2.0 * am ;
 _MATELM1( 17 ,16)  -= _term;
 _MATELM1( 16 ,16)  += _term;
 /*REACTION*/
  /* ~ I1 <-> ID1 ( bd , ad )*/
 _term =  bd ;
 _MATELM1( 17 ,17)  += _term;
 _MATELM1( 5 ,17)  -= _term;
 _term =  ad ;
 _MATELM1( 17 ,5)  -= _term;
 _MATELM1( 5 ,5)  += _term;
 /*REACTION*/
  /* ~ I1 <-> I21S ( bs , as )*/
 _term =  bs ;
 _MATELM1( 17 ,17)  += _term;
 _MATELM1( 9 ,17)  -= _term;
 _term =  as ;
 _MATELM1( 17 ,9)  -= _term;
 _MATELM1( 9 ,9)  += _term;
 /*REACTION*/
  /* ~ I2 <-> I3 ( bm , 3.0 * am )*/
 _term =  bm ;
 _MATELM1( 16 ,16)  += _term;
 _MATELM1( 15 ,16)  -= _term;
 _term =  3.0 * am ;
 _MATELM1( 16 ,15)  -= _term;
 _MATELM1( 15 ,15)  += _term;
 /*REACTION*/
  /* ~ I2 <-> ID2 ( bd , ad )*/
 _term =  bd ;
 _MATELM1( 16 ,16)  += _term;
 _MATELM1( 4 ,16)  -= _term;
 _term =  ad ;
 _MATELM1( 16 ,4)  -= _term;
 _MATELM1( 4 ,4)  += _term;
 /*REACTION*/
  /* ~ I2 <-> I22S ( bs , as )*/
 _term =  bs ;
 _MATELM1( 16 ,16)  += _term;
 _MATELM1( 8 ,16)  -= _term;
 _term =  as ;
 _MATELM1( 16 ,8)  -= _term;
 _MATELM1( 8 ,8)  += _term;
 /*REACTION*/
  /* ~ I3 <-> ID3 ( bd , ad )*/
 _term =  bd ;
 _MATELM1( 15 ,15)  += _term;
 _MATELM1( 0 ,15)  -= _term;
 _term =  ad ;
 _MATELM1( 15 ,0)  -= _term;
 _MATELM1( 0 ,0)  += _term;
 /*REACTION*/
  /* ~ ID <-> ID1 ( 3.0 * bm , am )*/
 _term =  3.0 * bm ;
 _MATELM1( 6 ,6)  += _term;
 _MATELM1( 5 ,6)  -= _term;
 _term =  am ;
 _MATELM1( 6 ,5)  -= _term;
 _MATELM1( 5 ,5)  += _term;
 /*REACTION*/
  /* ~ ID1 <-> ID2 ( 2.0 * bm , 2.0 * am )*/
 _term =  2.0 * bm ;
 _MATELM1( 5 ,5)  += _term;
 _MATELM1( 4 ,5)  -= _term;
 _term =  2.0 * am ;
 _MATELM1( 5 ,4)  -= _term;
 _MATELM1( 4 ,4)  += _term;
 /*REACTION*/
  /* ~ ID2 <-> ID3 ( bm , 3.0 * am )*/
 _term =  bm ;
 _MATELM1( 4 ,4)  += _term;
 _MATELM1( 0 ,4)  -= _term;
 _term =  3.0 * am ;
 _MATELM1( 4 ,0)  -= _term;
 _MATELM1( 0 ,0)  += _term;
 /*REACTION*/
  /* ~ I10S <-> I20S ( bh , ah )*/
 _term =  bh ;
 _MATELM1( 14 ,14)  += _term;
 _MATELM1( 10 ,14)  -= _term;
 _term =  ah ;
 _MATELM1( 14 ,10)  -= _term;
 _MATELM1( 10 ,10)  += _term;
 /*REACTION*/
  /* ~ I11S <-> I21S ( bh , ah )*/
 _term =  bh ;
 _MATELM1( 13 ,13)  += _term;
 _MATELM1( 9 ,13)  -= _term;
 _term =  ah ;
 _MATELM1( 13 ,9)  -= _term;
 _MATELM1( 9 ,9)  += _term;
 /*REACTION*/
  /* ~ I12S <-> I22S ( bh , ah )*/
 _term =  bh ;
 _MATELM1( 12 ,12)  += _term;
 _MATELM1( 8 ,12)  -= _term;
 _term =  ah ;
 _MATELM1( 12 ,8)  -= _term;
 _MATELM1( 8 ,8)  += _term;
 /*REACTION*/
  /* ~ I13S <-> I23S ( bh , ah )*/
 _term =  bh ;
 _MATELM1( 11 ,11)  += _term;
 _MATELM1( 7 ,11)  -= _term;
 _term =  ah ;
 _MATELM1( 11 ,7)  -= _term;
 _MATELM1( 7 ,7)  += _term;
 /*REACTION*/
   /* O + C1 + C2 + C3 + I + I1 + I2 + I3 + I10S + I11S + I12S + I13S + I20S + I21S + I22S + I23S + ID + ID1 + ID2 + ID3 = 1.0 */
 /*CONSERVATION*/
   } return _reset;
 }
 
/*CVODE end*/
 
static int _ode_count(int _type){ return 20;}
 
static void _ode_spec(_NrnThread* _nt, _Memb_list* _ml, int _type) {
   double* _p; Datum* _ppvar; Datum* _thread;
   Node* _nd; double _v; int _iml, _cntml;
  _cntml = _ml->_nodecount;
  _thread = _ml->_thread;
  for (_iml = 0; _iml < _cntml; ++_iml) {
    _p = _ml->_data[_iml]; _ppvar = _ml->_pdata[_iml];
    _nd = _ml->_nodelist[_iml];
    v = NODEV(_nd);
  ena = _ion_ena;
     _ode_spec1 (_p, _ppvar, _thread, _nt);
  }}
 
static void _ode_map(int _ieq, double** _pv, double** _pvdot, double* _pp, Datum* _ppd, double* _atol, int _type) { 
	double* _p; Datum* _ppvar;
 	int _i; _p = _pp; _ppvar = _ppd;
	_cvode_ieq = _ieq;
	for (_i=0; _i < 20; ++_i) {
		_pv[_i] = _pp + _slist1[_i];  _pvdot[_i] = _pp + _dlist1[_i];
		_cvode_abstol(_atollist, _atol, _i);
	}
 }
 
static void _ode_matsol_instance1(_threadargsproto_) {
 _cvode_sparse_thread(&_thread[_cvspth1]._pvoid, 20, _dlist1, _p, _ode_matsol1, _ppvar, _thread, _nt);
 }
 
static void _ode_matsol(_NrnThread* _nt, _Memb_list* _ml, int _type) {
   double* _p; Datum* _ppvar; Datum* _thread;
   Node* _nd; double _v; int _iml, _cntml;
  _cntml = _ml->_nodecount;
  _thread = _ml->_thread;
  for (_iml = 0; _iml < _cntml; ++_iml) {
    _p = _ml->_data[_iml]; _ppvar = _ml->_pdata[_iml];
    _nd = _ml->_nodelist[_iml];
    v = NODEV(_nd);
  ena = _ion_ena;
 _ode_matsol_instance1(_threadargs_);
 }}
 
static void _thread_cleanup(Datum* _thread) {
   _nrn_destroy_sparseobj_thread(_thread[_cvspth1]._pvoid);
   _nrn_destroy_sparseobj_thread(_thread[_spth1]._pvoid);
 }
 extern void nrn_update_ion_pointer(Symbol*, Datum*, int, int);
 static void _update_ion_pointer(Datum* _ppvar) {
   nrn_update_ion_pointer(_na_sym, _ppvar, 0, 0);
   nrn_update_ion_pointer(_na_sym, _ppvar, 1, 3);
   nrn_update_ion_pointer(_na_sym, _ppvar, 2, 4);
 }

static void initmodel(double* _p, Datum* _ppvar, Datum* _thread, _NrnThread* _nt) {
  int _i; double _save;{
  C3 = C30;
  C2 = C20;
  C1 = C10;
  ID3 = ID30;
  ID2 = ID20;
  ID1 = ID10;
  ID = ID0;
  I23S = I23S0;
  I22S = I22S0;
  I21S = I21S0;
  I20S = I20S0;
  I13S = I13S0;
  I12S = I12S0;
  I11S = I11S0;
  I10S = I10S0;
  I3 = I30;
  I2 = I20;
  I1 = I10;
  I = I0;
  O = O0;
 {
   rates ( _threadargscomma_ v ) ;
    _ss_sparse_thread(&_thread[_spth1]._pvoid, 20, _slist1, _dlist1, _p, &t, dt, scheme1, _linmat1, _ppvar, _thread, _nt);
     if (secondorder) {
    int _i;
    for (_i = 0; _i < 20; ++_i) {
      _p[_slist1[_i]] += dt*_p[_dlist1[_i]];
    }}
 }
 
}
}

static void nrn_init(_NrnThread* _nt, _Memb_list* _ml, int _type){
double* _p; Datum* _ppvar; Datum* _thread;
Node *_nd; double _v; int* _ni; int _iml, _cntml;
#if CACHEVEC
    _ni = _ml->_nodeindices;
#endif
_cntml = _ml->_nodecount;
_thread = _ml->_thread;
for (_iml = 0; _iml < _cntml; ++_iml) {
 _p = _ml->_data[_iml]; _ppvar = _ml->_pdata[_iml];
#if CACHEVEC
  if (use_cachevec) {
    _v = VEC_V(_ni[_iml]);
  }else
#endif
  {
    _nd = _ml->_nodelist[_iml];
    _v = NODEV(_nd);
  }
 v = _v;
  ena = _ion_ena;
 initmodel(_p, _ppvar, _thread, _nt);
 }
}

static double _nrn_current(double* _p, Datum* _ppvar, Datum* _thread, _NrnThread* _nt, double _v){double _current=0.;v=_v;{ {
   gna = gnabar * O ;
   ina = gna * ( v - ena ) ;
   jina17 = ina ;
   }
 _current += ina;

} return _current;
}

static void nrn_cur(_NrnThread* _nt, _Memb_list* _ml, int _type) {
double* _p; Datum* _ppvar; Datum* _thread;
Node *_nd; int* _ni; double _rhs, _v; int _iml, _cntml;
#if CACHEVEC
    _ni = _ml->_nodeindices;
#endif
_cntml = _ml->_nodecount;
_thread = _ml->_thread;
for (_iml = 0; _iml < _cntml; ++_iml) {
 _p = _ml->_data[_iml]; _ppvar = _ml->_pdata[_iml];
#if CACHEVEC
  if (use_cachevec) {
    _v = VEC_V(_ni[_iml]);
  }else
#endif
  {
    _nd = _ml->_nodelist[_iml];
    _v = NODEV(_nd);
  }
  ena = _ion_ena;
 _g = _nrn_current(_p, _ppvar, _thread, _nt, _v + .001);
 	{ double _dina;
  _dina = ina;
 _rhs = _nrn_current(_p, _ppvar, _thread, _nt, _v);
  _ion_dinadv += (_dina - ina)/.001 ;
 	}
 _g = (_g - _rhs)/.001;
  _ion_ina += ina ;
#if CACHEVEC
  if (use_cachevec) {
	VEC_RHS(_ni[_iml]) -= _rhs;
  }else
#endif
  {
	NODERHS(_nd) -= _rhs;
  }
 
}
 
}

static void nrn_jacob(_NrnThread* _nt, _Memb_list* _ml, int _type) {
double* _p; Datum* _ppvar; Datum* _thread;
Node *_nd; int* _ni; int _iml, _cntml;
#if CACHEVEC
    _ni = _ml->_nodeindices;
#endif
_cntml = _ml->_nodecount;
_thread = _ml->_thread;
for (_iml = 0; _iml < _cntml; ++_iml) {
 _p = _ml->_data[_iml];
#if CACHEVEC
  if (use_cachevec) {
	VEC_D(_ni[_iml]) += _g;
  }else
#endif
  {
     _nd = _ml->_nodelist[_iml];
	NODED(_nd) += _g;
  }
 
}
 
}

static void nrn_state(_NrnThread* _nt, _Memb_list* _ml, int _type) {
double* _p; Datum* _ppvar; Datum* _thread;
Node *_nd; double _v = 0.0; int* _ni; int _iml, _cntml;
double _dtsav = dt;
if (secondorder) { dt *= 0.5; }
#if CACHEVEC
    _ni = _ml->_nodeindices;
#endif
_cntml = _ml->_nodecount;
_thread = _ml->_thread;
for (_iml = 0; _iml < _cntml; ++_iml) {
 _p = _ml->_data[_iml]; _ppvar = _ml->_pdata[_iml];
 _nd = _ml->_nodelist[_iml];
#if CACHEVEC
  if (use_cachevec) {
    _v = VEC_V(_ni[_iml]);
  }else
#endif
  {
    _nd = _ml->_nodelist[_iml];
    _v = NODEV(_nd);
  }
 v=_v;
{
  ena = _ion_ena;
 {  sparse_thread(&_thread[_spth1]._pvoid, 20, _slist1, _dlist1, _p, &t, dt, scheme1, _linmat1, _ppvar, _thread, _nt);
     if (secondorder) {
    int _i;
    for (_i = 0; _i < 20; ++_i) {
      _p[_slist1[_i]] += dt*_p[_dlist1[_i]];
    }}
 } }}
 dt = _dtsav;
}

static void terminal(){}

static void _initlists(){
 double _x; double* _p = &_x;
 int _i; static int _first = 1;
  if (!_first) return;
 _slist1[0] = &(ID3) - _p;  _dlist1[0] = &(DID3) - _p;
 _slist1[1] = &(C3) - _p;  _dlist1[1] = &(DC3) - _p;
 _slist1[2] = &(C2) - _p;  _dlist1[2] = &(DC2) - _p;
 _slist1[3] = &(C1) - _p;  _dlist1[3] = &(DC1) - _p;
 _slist1[4] = &(ID2) - _p;  _dlist1[4] = &(DID2) - _p;
 _slist1[5] = &(ID1) - _p;  _dlist1[5] = &(DID1) - _p;
 _slist1[6] = &(ID) - _p;  _dlist1[6] = &(DID) - _p;
 _slist1[7] = &(I23S) - _p;  _dlist1[7] = &(DI23S) - _p;
 _slist1[8] = &(I22S) - _p;  _dlist1[8] = &(DI22S) - _p;
 _slist1[9] = &(I21S) - _p;  _dlist1[9] = &(DI21S) - _p;
 _slist1[10] = &(I20S) - _p;  _dlist1[10] = &(DI20S) - _p;
 _slist1[11] = &(I13S) - _p;  _dlist1[11] = &(DI13S) - _p;
 _slist1[12] = &(I12S) - _p;  _dlist1[12] = &(DI12S) - _p;
 _slist1[13] = &(I11S) - _p;  _dlist1[13] = &(DI11S) - _p;
 _slist1[14] = &(I10S) - _p;  _dlist1[14] = &(DI10S) - _p;
 _slist1[15] = &(I3) - _p;  _dlist1[15] = &(DI3) - _p;
 _slist1[16] = &(I2) - _p;  _dlist1[16] = &(DI2) - _p;
 _slist1[17] = &(I1) - _p;  _dlist1[17] = &(DI1) - _p;
 _slist1[18] = &(I) - _p;  _dlist1[18] = &(DI) - _p;
 _slist1[19] = &(O) - _p;  _dlist1[19] = &(DO) - _p;
_first = 0;
}

#if defined(__cplusplus)
} /* extern "C" */
#endif
