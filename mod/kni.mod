TITLE Non-inactivating potassium current
: from Yeoh JW, Corrias A, Buist ML. Modelling Human Colonic Smooth Muscle Cell Electrophysiology. Cell Mol Bioeng. 2017

NEURON {
  SUFFIX kni
  USEION k READ ek WRITE ik
  USEION ca READ cai
  RANGE gbar, g, i, jika, Vdkni, kdkni, Kdcakni, scakni
}

UNITS {
  (S) = (siemens)
  (mV) = (millivolt)
  (mA) = (milliamp)	
}

PARAMETER {
  gbar = 0.0032727 (S/cm2)  : length 83um and radius 6um; 40 nS translates to 40 nS / 1222.2 um2 = 4e-8 S/ 1.2222e-5 cm2 = 0.0032727 S/cm2
  Vdkni = 12 (mV)           : Half-maximal activation potential 
  kdkni = 27 (mV)           : Slope potential for activation
  Kdcakni = 0.000095 (mM)   : Half-activation [Ca2+]
  scakni = 0.000048 (mM)    : Slope factor
  
  cai (mM)
  jika 	(mA/cm2)
}

ASSIGNED {
  v	(mV)
  ek	(mV)
  ik 	(mA/cm2)
  i 	(mA/cm2)
  g	(S/cm2)
  
 
}

STATE {dkni dcakni}


BREAKPOINT {
  SOLVE states METHOD cnexp
  g = gbar*dkni*dcakni
  i = g*(v-ek)
  ik = i
  jika = i
}
  
INITIAL {
  dkni = dkniinf(v)
  dcakni = dcakniinf(cai)
}

DERIVATIVE states {
 dkni'= (dkniinf(v)-dkni)/dknitau(v)
 dcakni' = (dcakniinf(cai)-dcakni)/dcaknitau(cai)
}

FUNCTION dkniinf (Vm (mV)) () {

  UNITSOFF
    dkniinf = 1/(1+exp(-(Vm-Vdkni)/kdkni))
  UNITSON

}


FUNCTION dcakniinf (CaiFree (mM)) () {

  UNITSOFF
    dcakniinf = 1/(1+exp(-(CaiFree+Kdcakni)/scakni))
  UNITSON

}


FUNCTION dknitau (Vm (mV)) () {

  UNITSOFF
    dknitau = 0.0006 + 0.004*exp(-Vm/20)
  UNITSON

}    


FUNCTION dcaknitau (CaiFree (mM)) () {

  UNITSOFF
    dcaknitau = 0.0007 + 0.010*exp(-CaiFree/43)
  UNITSON

}