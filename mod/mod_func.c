#include <stdio.h>
#include "hocdec.h"
#define IMPORT extern __declspec(dllimport)
IMPORT int nrnmpi_myid, nrn_nobanner_;

extern void _ElectSyn_reg();
extern void _VecStim_reg();
extern void _cMSC_reg();
extern void _cal_reg();
extern void _can_reg();
extern void _cansc_reg();
extern void _cat_reg();
extern void _ih_reg();
extern void _jcal_reg();
extern void _jsvclmp_reg();
extern void _ka_reg();
extern void _katp_reg();
extern void _kca_fast_reg();
extern void _kca_slow_reg();
extern void _kdr_reg();
extern void _kdrBS_reg();
extern void _kfi_reg();
extern void _kmtwt_reg();
extern void _kni_reg();
extern void _msc_reg();
extern void _myexpsyn_reg();
extern void _nak_reg();
extern void _nav13_reg();
extern void _nav15_reg();
extern void _nav17_reg();
extern void _nav19_reg();
extern void _ncx_reg();
extern void _tMSC_reg();

void modl_reg(){
	//nrn_mswindll_stdio(stdin, stdout, stderr);
    if (!nrn_nobanner_) if (nrnmpi_myid < 1) {
	fprintf(stderr, "Additional mechanisms from files\n");

fprintf(stderr," ElectSyn.mod");
fprintf(stderr," VecStim.mod");
fprintf(stderr," cMSC.mod");
fprintf(stderr," cal.mod");
fprintf(stderr," can.mod");
fprintf(stderr," cansc.mod");
fprintf(stderr," cat.mod");
fprintf(stderr," ih.mod");
fprintf(stderr," jcal.mod");
fprintf(stderr," jsvclmp.mod");
fprintf(stderr," ka.mod");
fprintf(stderr," katp.mod");
fprintf(stderr," kca_fast.mod");
fprintf(stderr," kca_slow.mod");
fprintf(stderr," kdr.mod");
fprintf(stderr," kdrBS.mod");
fprintf(stderr," kfi.mod");
fprintf(stderr," kmtwt.mod");
fprintf(stderr," kni.mod");
fprintf(stderr," msc.mod");
fprintf(stderr," myexpsyn.mod");
fprintf(stderr," nak.mod");
fprintf(stderr," nav13.mod");
fprintf(stderr," nav15.mod");
fprintf(stderr," nav17.mod");
fprintf(stderr," nav19.mod");
fprintf(stderr," ncx.mod");
fprintf(stderr," tMSC.mod");
fprintf(stderr, "\n");
    }
_ElectSyn_reg();
_VecStim_reg();
_cMSC_reg();
_cal_reg();
_can_reg();
_cansc_reg();
_cat_reg();
_ih_reg();
_jcal_reg();
_jsvclmp_reg();
_ka_reg();
_katp_reg();
_kca_fast_reg();
_kca_slow_reg();
_kdr_reg();
_kdrBS_reg();
_kfi_reg();
_kmtwt_reg();
_kni_reg();
_msc_reg();
_myexpsyn_reg();
_nak_reg();
_nav13_reg();
_nav15_reg();
_nav17_reg();
_nav19_reg();
_ncx_reg();
_tMSC_reg();
}
