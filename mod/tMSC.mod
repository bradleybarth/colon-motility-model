TITLE tensile mechanosensitive channel
: Written by Bradley Barth on December 20th, 2019

UNITS {
	(mA)     = (milliamp)
	(mV)     = (millivolt) 
	: (ms)     = (millisecond)
  : (Pa)   = (pascal)
}

PARAMETER {
	v 		(mV)
  tension
  dtension
  sigma
  gbar = 0.0001 (S/cm2)
  e = 10 (mV)
  onHalf = 0.1
  onSlope = 0.015
  offHalf = 0.1
  offSlope = 0.01
  onTauMax = 1000
  onTauOffset = 0.2
  onTauSlope = 0.05
  onTauHalf = 0.05  
  offTauMax = 5000
  offTauOffset = 0.05
  offTauSlope = 0.01
  offTauHalf = 0.15
    
    
  k1       = 10 : (Pa) spring constant for linear spring
  B        = 1 : (ms) : 12 kPa * s = 12e6 Pa * ms Dashpot constant
  normFactor = 1
	jiimsc (mA/cm2)
}


NEURON {
	SUFFIX tMSC
	NONSPECIFIC_CURRENT i
	RANGE tension, dtension, sigma, gbar, jiimsc, onHalf, onSlope, offHalf, offSlope, onTauSlope, onTauHalf, offTauSlope, offTauHalf, onTauMax, onTauOffset, offTauMax, offTauOffset, k1, B                 
}

ASSIGNED {
	i (mA/cm2)
}

STATE {onGate offGate}

INITIAL {
  onGate = onInf(tension)
  offGate = offInf(tension)  
}


BREAKPOINT {     
  SOLVE states METHOD cnexp                    
  sigma    = (k1*tension + B*dtension)/normFactor
	i        = onGate * offGate * gbar * (v-e)
	jiimsc   = i
}

DERIVATIVE states {
  onGate' = (onInf(sigma) - onGate)/onTau(sigma)
  offGate' = (offInf(sigma) - offGate)/offTau(sigma)
}

FUNCTION onInf (Sigma) () {
  UNITSOFF
    onInf = 1/(1+exp(-(Sigma-onHalf)/onSlope))
  UNITSON
}

FUNCTION offInf (Sigma) () {
  UNITSOFF
    offInf = 1/(1+exp((Sigma-offHalf)/offSlope))
  UNITSON
}   

FUNCTION onTau (Sigma) () {
  UNITSOFF
    onTau = onTauMax * (onTauOffset + (1-onTauOffset)/(1+exp(-(Sigma-onTauHalf)/onTauSlope)))
  UNITSON
}

FUNCTION offTau (Sigma) () {
  UNITSOFF
    offTau = offTauMax * (offTauOffset + (1-offTauOffset)/(1+exp((Sigma-offTauHalf)/offTauSlope)))
  UNITSON
}