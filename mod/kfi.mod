TITLE Fast inactivating potassium current
: from Yeoh JW, Corrias A, Buist ML. Modelling Human Colonic Smooth Muscle Cell Electrophysiology. Cell Mol Bioeng. 2017

NEURON {
  SUFFIX kfi
  USEION k READ ek WRITE ik
  USEION ca READ cai
  RANGE gbar, g, i, jika, Vdkfi, kdkfi, Kdcakfi, sdcakfi, kkfi, hkfi, caset
}

UNITS {
  (S) = (siemens)
  (mV) = (millivolt)
  (mA) = (milliamp)	
}

PARAMETER {
  gbar = 0.001472715 (S/cm2): length 83um and radius 6um; 18 nS translates to 18 nS / 1222.2 um2 = 1.8e-8 S/ 1.2222e-5 cm2 = 0.001472715 S/cm2
  Vdkfi = 1.2 (mV)          : Half-maximal activation potential 
  kdkfi = 25 (mV)           : Slope potential for activation
  Kdcakfi = 4.76e-6 (mM)    : Half-activation [Ca2+]
  sdcakfi = 8.078e-6 (mM)   : Slope factor
  kkfi = 13.5 (mV)          : Slope factor for votlage activation
  hkfi = 0.484              : Hill coefficient for calcium-dependent activation
  caset = 0.00143e-6 (mM)   : Half-activation [Ca] at V = 0mV
  dcakfitau = 0.002
  dkfitau = 0.006
  
  jika 	(mA/cm2)
  cai (mM)
}

ASSIGNED {
  v	  (mV)
  ek	(mV)
  ik 	(mA/cm2)
  i 	(mA/cm2)
  g	  (S/cm2)
}

STATE {dkfi dcakfi fpo}


BREAKPOINT {
  SOLVE states METHOD cnexp
  g = gbar*dkfi*dcakfi*fpo
  i = g*(v-ek)
  ik = i
  jika = i
}
  
INITIAL {
  dkfi = dkfiinf(v)
  dcakfi = dcakfiinf(cai)
  fpo = fpoinf(v,cai)
}

DERIVATIVE states {
 dkfi'= (dkfiinf(v)-dkfi)/dkfitau
 dcakfi' = (dcakfiinf(cai)-dcakfi)/dcakfitau
 fpo' = (fpoinf(v,cai)-fpo)/fpotau(v)
}

FUNCTION dkfiinf (Vm (mV)) () {

  UNITSOFF
    dkfiinf = 1/(1+exp(-(Vm-Vdkfi)/kdkfi))
  UNITSON

}


FUNCTION dcakfiinf (CaiFree (mM)) () {

  UNITSOFF
    dcakfiinf = 1/(1+exp(-(CaiFree-Kdcakfi)/sdcakfi))
  UNITSON

}  


FUNCTION fpoinf (Vm (mv), CaiFree (mM)) () {

  UNITSOFF
    fpoinf = 1 - 1/(1+exp(-Vm/kkfi - hkfi*log(CaiFree/caset)))
  UNITSON

}


FUNCTION fpotau (Vm (mV)) () {

  UNITSOFF
    fpotau = 0.388 - 0.310/(1+exp(-(Vm+2.0)/5.3))
  UNITSON

}    