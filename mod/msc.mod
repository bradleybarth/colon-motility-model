TITLE mechano-sensitive channel
: Modelled from Swerup and Rydqvist 1996 doi.org/10.1152/jn.1996.76.4.2211
: Written by Bradley Barth on October 25th, 2019

UNITS {
	(mA)     = (milliamp)
	(mV)     = (millivolt) 
	: (ms)     = (millisecond)
  : (Pa)   = (pascal)
}

PARAMETER {
	v 		(mV)
  strain
  dstrain
  emsc     = 10	(mV)  
	gsmax    = 0.0637 	(mho/cm2)    
  kb       = 106 : unitless Boltzman Constant 106 or 18.9 for linear or square model
  s        = 0.00277 : (1/Pa) : sensitivity constant 0.002277 Pa^(-1) or 0.995e-6 Pa^(-2) for linear or square model
  m        = 25 : unitless Tension conversion factor 20-30
  q        = 1 : unitless Power constant - linear (1) or square (2) model
  k0       = 12000 : (Pa) constant for MSC adaptation
  T0       = 50 : (ms) : time-constant for MSC adaptation
  k1       = 4e5 : (Pa) spring constant for linear spring
  B        = 4e4 : (ms) : 12 kPa * s = 12e6 Pa * ms Dashpot constant
    
	jiimsc (mA/cm2)
}


NEURON {
	SUFFIX msc
	NONSPECIFIC_CURRENT i
	GLOBAL kb, s, q, m, emsc
	RANGE strain, dstrain, jiimsc, p0, sigma0, gsmax, B, k1
}

ASSIGNED {
	i (mA/cm2)
  sigma    
  p0
}

STATE {sigma0}

INITIAL {   
}


BREAKPOINT {     
  SOLVE states METHOD cnexp
  sigma    = (k1*strain + B * dstrain)/20
  p0       = 1/(1 + exp(-((sigma)-2000)/200)) :1/(1 + kb * exp(-s * (sigma/m)^q)) hill plot didn't match scale in paper
	i        = p0 * gsmax * (v-emsc)
	jiimsc   = i
}

DERIVATIVE states {
 	sigma0'  = (k0 - sigma0)/T0
}