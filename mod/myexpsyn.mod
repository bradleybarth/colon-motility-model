NEURON {
	POINT_PROCESS myexpsyn
	RANGE tau, e, i, strength
	NONSPECIFIC_CURRENT i
}

UNITS {
	(nA) = (nanoamp)
	(mV) = (millivolt)
	(uS) = (microsiemens)
}

PARAMETER {
	tau = 0.5 (ms) <1e-9,1e9>
	e = 0	(mV)
	strength
}

ASSIGNED {
	v (mV)
	i (nA)
}

STATE {
	g (uS) 
}

INITIAL {
	g=0
}

BREAKPOINT {
	SOLVE state METHOD cnexp
	i = strength*g*(v - e)
}

DERIVATIVE state {
	:g' = 0.5*(1-g)/(1+exp((-v-5)/12)) - 2*g
	g' = -g/tau
}
NET_RECEIVE(weight (uS)) {
	g = weight
}